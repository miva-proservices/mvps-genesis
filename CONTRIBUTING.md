# Pull Request Template

For more details about reporting issues, requesting features, or working on tasks, [please see the contributing guide in the docs](CONTRIBUTING.md?useDefaultHandler=true)

## Summary

Summarize your changes

## Motivation and Context

- Why is this change required?
- What problem does it solve?

## How Has This Been Tested?

Please describe in detail how you tested your changes.

Include details of your testing environment, and the tests you ran to
see how your change affects other areas of the code, etc.

## Tested in Browsers

Check the boxes on the browsers that you tested

- [ ] Mobile
	- [ ] Chrome for Android
	- [ ] iOs Safari
	- [ ] Samsung Internet
- [ ] Desktop
	- [ ] Mac
		- [ ] Safari
		- [ ] Chrome
	- [ ] Windows
		- [ ] Chrome
		- [ ] FireFox
		- [ ] Edge
		- [ ] IE 11

## Checklist

- [ ] It has been cross-browser tested according to our [standards](docs/standards.md#browser-support)
- [ ] All code conforms to the [linters](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-configs/browse).
- [ ] You have revised or add to the documentation where it makes sense
