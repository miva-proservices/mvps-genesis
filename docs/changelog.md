# Change Log

## Unreleased

### [Genesis Roadmap](https://design.mivamerchant.net/secure/Dashboard.jspa?selectPageId=11213)

### [Version 1.0.0](https://design.mivamerchant.net/projects/PS/versions/10204)

- **Timeline:** Q2 2021
- Notable Features
	- In-sync with Design System

### Version 0.8.0

- **Timeline:** February 2021
- Notable Features:
	- Mega Menu
	- mmt

---

## Released

### Version 0.7.0

- **Latest Release:** `0.7.6`
- **Main Goals:** Optimize framework with more components from the Design System and improve the site-build development workflow.
- **Timeline:** May 11th, 2020 to February 5th, 2021
- **Jira Issues:** [filter = "Genesis - All Items" AND status  = Done AND fixVersion = "Genesis 0.7"](https://design.mivamerchant.net/issues/?jql=filter%20%3D%20%22Genesis%20-%20All%20Items%22%20AND%20status%20%20%3D%20Done%20AND%20fixVersion%20%3D%20%22Genesis%200.7%22%20)
- **Upgrade Path:** 0.7 has several [security-fixes](#security) & [new-features](#added) that may be worth adding to old stores. See the [Security](#security) and [Added > Notable](#added) items for more details. Upgrading from 0.6 to 0.7 is a manual process where a developer would need to pick the features & changes they like in 0.7 that they want to migrate to their store. The best way to use 0.7 is as the foundation for new/future site-builds.
- **Downloads:**
	- [Framework .pkg File](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/generator-quickstart/app/templates/miva-server/%25WEB_ROOT%25/%25MIVA_ROOT%25/frameworks/%25STORE_ID%25/genesis--v0.7.6.pkg?at=refs%2Ftags%2F%40mvps-genesis%2Fgenerator-quickstart%400.7.6)
	- Components & Layouts:
		- [Components](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/generator-quickstart/app/templates/miva-server/private/mivadata/component_layouts--components.xml?at=refs%2Ftags%2F%40mvps-genesis%2Fgenerator-quickstart%400.7.6)
		- [Layouts](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/generator-quickstart/app/templates/miva-server/private/mivadata/component_layouts--layouts.xml?at=@mvps-genesis/generator-quickstart@0.7.6)

#### Security

- [PS-21280 - Resolve Potential PII Display](https://design.mivamerchant.net/browse/PS-21280) | [Code Changes](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/192/overview)
- [PS-21341 - Clickjacking / Redressing Prevention](https://design.mivamerchant.net/browse/PS-21341) | [Code Changes](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/189/overview)
- Commented out the `variable-list` Content Section | [Code Changes](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/commits/4fa526eaf477e1f0d029fc1515c3bf665084a0c5)

#### Added

- Notable
	- [PS-15411 - Added new components & layouts from the Design System](https://projects.invisionapp.com/share/JTXUITL8EWD#/screens) | [PS-15411](https://design.mivamerchant.net/browse/PS-15411)
	- [PS-15298 - Added Cross Device Baskets](https://design.mivamerchant.net/browse/PS-15298)
		- [PS-24129 - Updated Cross-Device Baskets for Miva 10](https://design.mivamerchant.net/browse/PS-24129)
	- [PS-25173 - Added additional root-htaccess best-practice configurations](https://design.mivamerchant.net/browse/PS-25173)
	- [PS-26954 - Improved the BASK & WISH product layout on mobile devices](https://design.mivamerchant.net/browse/PS-26954)
	- Added all public/webpack build files to pkg framework | [PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/218/overview)
- Other
	- [PS-15371 - Setup genesis.mivamerchant.net](https://design.mivamerchant.net/browse/PS-15371)
	- [PS-21143 - Added missing documentation for JavaScript utility packages](https://design.mivamerchant.net/browse/PS-21143)
	- Added new field options for _Arrow Navigation Direction_ in the _carousel_ component

#### Changed

- Notable
	- [PS-18707 - Killed jQuery](https://design.mivamerchant.net/browse/PS-18707)
	- [PS-18899 - Update Mobile Menu Design](https://design.mivamerchant.net/browse/PS-18899)
	- [PS-18351 - Ensure Accessibility WCAG AA - SiteSpeed.io Audit](https://design.mivamerchant.net/browse/PS-18351)
	- [PS-16772 - Extensions: Product Card Listings Rendering a Global RT CS for Product Card](https://design.mivamerchant.net/browse/PS-16772)
	- [PS-20474 - Developers: Modify Theme Update Strategy to "Patch Only"](https://design.mivamerchant.net/browse/PS-20474)
	- Moved `themes/genesis` location to easier development directory
	- Moved many packages into theme for improved site-build customization
	- pnpm script commands have been reduced and are easier to customize (ex. `$ pnpm run build:[env]` is now `$ pnpm run build [env]`)
	- Changed how `ConfirmAction` works: it's now globally initialized and can work on forms too
	- The `subcategory_tree` Content Section has been disabled by default in accordance with the preference for the default for future site-build designs; instead only the facets will display
- Other
	- [PS-16323 - Extensions: Optimize Shipping Estimator](https://design.mivamerchant.net/browse/PS-16323)
	- [PS-16724 - Misc. Features: Improve Customer Fields Templates: Documentation](https://design.mivamerchant.net/browse/PS-16724)
	- [PS-17978 - Release 0.6 QA Fixes - PROD Zoom Image Cycle/Browse](https://design.mivamerchant.net/browse/PS-17978)
	- [PS-17983 - Release 0.6 QA Enhancements- BASK Delete Entire Basket Missing Confirmation](https://design.mivamerchant.net/browse/PS-17983)
	- [PS-17989 - Release 0.6 QA Enhancements - Checkout Privacy / Ship Policy Link Changes](https://design.mivamerchant.net/browse/PS-17989)
	- [PS-18373 - Add rows to C&L Form Features](https://design.mivamerchant.net/browse/PS-18373)
	- [PS-18427 - Add `prepatch` pnpm command and rename prerelease one](https://design.mivamerchant.net/browse/PS-18427)
	- [PS-18445 - Mobile Footer Tabs Update](https://design.mivamerchant.net/browse/PS-18445)
	- [PS-19537 - Update Theme/Style to Match Design System - UI Library](https://design.mivamerchant.net/browse/PS-19537)
	- [PS-19878 - Update Theme/Style to Match Design System - UI Library QA](https://design.mivamerchant.net/browse/PS-19878)
	- [PS-19910 - Extensions: Optimize Shipping Estimator: Fixing Merge Conflicts](https://design.mivamerchant.net/browse/PS-19910)
	- [PS-20611 - ParameterValidator Enhancements](https://design.mivamerchant.net/browse/PS-20611)
	- [PS-21035 - Clean-Up: Remove Category Tree Item Assignments](https://design.mivamerchant.net/browse/PS-21035)
	- [PS-21212 - Remove Sub-Category Tree listing from Default CTGY page](https://design.mivamerchant.net/browse/PS-21212)
	- [PS-9285 - Template Cleanup: Global variables for layout: Single vs Two column](https://design.mivamerchant.net/browse/PS-9285)
	- [PS-9288 - Template Cleanup: Global Variables for simplified checkout layout](https://design.mivamerchant.net/browse/PS-9288)
	- [PS-9315 - Misc. Features: Tess & Joes Optimizations](https://design.mivamerchant.net/browse/PS-9315)
	- [PS-9395 - Extensions: Optimize AJAX Add to Cart](https://design.mivamerchant.net/browse/PS-9395)

#### [Fixed](https://design.mivamerchant.net/issues/?jql=filter%20%3D%20%22Genesis%20-%20All%20Items%22%20AND%20status%20%20%3D%20Done%20AND%20fixVersion%20%3D%20%22Genesis%200.7%22%20and%20type%20%3D%20Bug)

- Moderate
	- [PS-15253 - Resolved some ShipEstimate edge-case scenarios](https://design.mivamerchant.net/browse/PS-15253)
	- [PS-18424 - Fixed HTMLFormValidator for Safari](https://design.mivamerchant.net/browse/PS-18424)
	- [PS-19812 - Fixed Delete Entire Cart on BASK](https://design.mivamerchant.net/browse/PS-19812)
	- [PS-20476 - Add Missing Attribute Values that configured C&L Display](https://design.mivamerchant.net/browse/PS-20476)
	- [PS-20697 - Fix Missing Product-Image-Data On Transient Load of C&L Carousel](https://design.mivamerchant.net/browse/PS-20697)
	- [PS-20699 - Changed C&L Form Math Problem Input-Type to Number for proper validation](https://design.mivamerchant.net/browse/PS-20699)
	- [PS-21297 - Bugfixes from Farmer Bros](https://design.mivamerchant.net/browse/PS-21297)
	- [PS-21883 - Fixed Checkout Basket Summary "data_long" attribute-value display](https://design.mivamerchant.net/browse/PS-21883)
	- [PS-22203 - Cleared Stale Basket Charges from Checkout](https://design.mivamerchant.net/browse/PS-22203)
	- [PS-22467 - Fixed PROD AjaxAddToCart Error When product was out of stock](https://design.mivamerchant.net/browse/PS-22467)
	- [PS-22651 - Minor/Moderate ADA Updates to Mini Basket Extension](https://design.mivamerchant.net/browse/PS-22651)
	- [PS-23146 - Fixed PROD Missing Attributes message display on PROD](https://design.mivamerchant.net/browse/PS-23146)
	- [PS-23163 - Changed CSS Chunking strategy to improve cache-invalidation](https://design.mivamerchant.net/browse/PS-23163)
	- [PS-23473 - Fixed "Organization" & "Breadcrumb" Schema Data Errors](https://design.mivamerchant.net/browse/PS-23473)
	- [PS-23823 - Resolved Minor/Moderate Axe accessibility issues found on EnWild](https://design.mivamerchant.net/browse/PS-23823)
	- [PS-24042 - Instagram Feed: Image Loading Issue](https://design.mivamerchant.net/browse/PS-24042) | [Code Changes](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/178)
	- [PS-25347 - Developers: Updated `$ pnpm run init` to Set Execute Permissions and ensure Jira-ID automatically is added to commits](https://design.mivamerchant.net/browse/PS-25347)
	- [PS-27515 - Fixed "Delete Entire Cart" functionality to properly replace inventory for items deleted from cart](https://design.mivamerchant.net/browse/PS-27515)
- Other
	- [PS-18091 - Fixed pnpm install not working with framework-theme package outside of monorepo](https://design.mivamerchant.net/browse/PS-18091)
	- [PS-18904 - Improved OPAY's Non-MivaPay Credit Card Field Display](https://design.mivamerchant.net/browse/PS-18904)
	- [PS-18938 - Fixed Schema Data Bug on Also Bought](https://design.mivamerchant.net/browse/PS-18938)
	- [PS-19255 - Fix IE11 BASK Basket Item Quantity Layout](https://design.mivamerchant.net/browse/PS-19255)
	- [PS-19921 - Ensured Fastened Header Appears When Scrolling](https://design.mivamerchant.net/browse/PS-19921)
	- [PS-20081 - Added Print CSS To Key Pages](https://design.mivamerchant.net/browse/PS-20081)
	- [PS-20660 - Improved Button Hover State](https://design.mivamerchant.net/browse/PS-20660)
	- [PS-20698 - Fixed overflow from o-wrapper Wide Class](https://design.mivamerchant.net/browse/PS-20698)
	- [PS-21657 - Fixed Carousel Mode (center) mobile scrolling](https://design.mivamerchant.net/browse/PS-21657)
	- [PS-21795 - Improved Breadcrumb Display](https://design.mivamerchant.net/browse/PS-21795)
	- [PS-23961 - Added Inventory Messages To AJAX Add To Cart](https://design.mivamerchant.net/browse/PS-23961)
	- [PS-25586 - Fixed Browser Sync Error introduced by PS-21341](https://design.mivamerchant.net/browse/PS-25586)
	- [PS-26613 - Fix Broken Webpack Configurations & Run eslint](https://design.mivamerchant.net/browse/PS-26613)
	- [PS-26744 - Updated OSEL Radio Buttons to be aligned](https://design.mivamerchant.net/browse/PS-26744)
	- [PS-26853 - Made Coupon message easier to read in Checkout](https://design.mivamerchant.net/browse/PS-26853)
	- [PS-26855 - Removed duplicate Quantity Display on ORDS using small tablet devices](https://design.mivamerchant.net/browse/PS-26855)
	- [PS-9251 - Verified/Fixed Site to Support Redis](https://design.mivamerchant.net/browse/PS-9251)

#### Removed

- [PS-25553 - Remove Legacy CTFM Page](https://design.mivamerchant.net/browse/PS-25553)

### Version 0.6.0

- **Description:** Adding remaining essential optimizations & improving existing extensions
- **Timeline:** 2020-03-17 to 2020-05-11
- [Release Summary](https://design.mivamerchant.net/projects/PS/versions/10400)
- [Repo](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse?at=refs%2Ftags%2Fv0.6.0)
- [Changes](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/compare/diff?targetBranch=refs%2Ftags%2Fv0.5.0&sourceBranch=refs%2Ftags%2Fv0.6.0&targetRepoId=24)
- [Commits](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/commits?until=refs%2Ftags%2Fv0.6.0)
- **Downloads:**
	- [Framework .pkg File](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/packages/framework/web-server/htdocs/mm5/frameworks/00000001/genesis.pkg?at=refs%2Ftags%2Fv0.6.0)
	- Components & Layouts:
		- [Components](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/packages/framework/web-server/mivadata/component_layouts--components.xml?at=refs%2Ftags%2Fv0.6.0)
		- [Layouts](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/packages/framework/web-server/mivadata/component_layouts--layouts.xml?at=refs%2Ftags%2Fv0.6.0)

#### Added

- New Packages:
	- `share-product` package for Product Page social-sharing links -
		[View Docs](features/optional-extensions/share-product.md) |
		[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/8/overview)
	- `form-validation` package to enable inline-form validation throughout site -
		[View Docs](features/theme-extensions/form-validation.md) |
		[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/21/overview)
	- `instagram-feed` package so clients can show their recent instagram posts on the site -
		[View Docs](features/optional-extensions/instagram.md) |
		[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/18/overview)
	- `breadcrumbs` package with several different mobile displays ("<- More Parent Category", Dropdown, & Horizontal scroll) -
		[View Docs](features/theme-extensions/breadcrumbs.md)
		[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/24/overview)
	- `event-bus` package so various packages can communicate and send each other events/messages -
		[View Docs](features/packages/event-bus.md)
	- `environment-helpers` package that adds js/no-js classes, touch/hover classes, and session variables so you can style or build JS logic around certain user capabilities -
		[View Docs](features/theme-extensions/environment-helpers.md)
	- `ajax-add-to-cart` package as a JS-class to handle the form submission of a Miva ADPR forms with AJAX -
		[View Docs](features/theme-extensions/ajax-add-to-cart.md)
	- `mini-basket` package as a JS-class to handle the display of the global-minibasket -
		[View Docs](features/theme-extensions/mini-basket.md)
	- `also-bought` package so that a "Customers Also Bought" section can be added to product pages -
		[View Docs](features/optional-extensions/also-bought.md)
		[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/32/overview)
	- `confirm-action` package for use during the checkout process to prompt customers to stay in the checkout if they left by accident -
		[View Docs](features/theme-extensions/confirm-action.md) |
		[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/44/overview)
	- `icons` package so that developers & other packages can reference the `content` with SCSS variables
		[View Docs](features/icons.md) |
		[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/53/overview)
	- `dynamic-form-element-attributes` package to Add `id` and `class` attributes to `input` and `select` elements dynamically created by Miva -
		[View Docs](features/theme-extensions/dynamic-form-element-attributes.md) |
		[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/59/overview)
- [pnpm package manager](https://pnpm.js.org/) to save disk-space & installation time -
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/58/overview)
- Password-requirement validation-messages that match the store's settings for new & updated passwords -
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/57/overview)
- `.githooks/prepare-commit-msg` for contributors to use to automatically append Jira Id to commits
- `aria-label` and other accessibility improvements
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/17/overview)
- A global `product_card_iterator` RT CS that can be used throughout the sites product listings (CTGY, PLST, SRCH, SFNT, PROD-related-products, etc) -
	[View Docs](features/miva-store/product-card.md) |
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/39/overview)
- Documented how open-source-fonts (like [fonts.google.com](https://fonts.google.com)) can easily be added to store
	[View Docs](features/fonts.md)
- Custom-field-group (`order_info_customer`) that will load all of the order-custom-fields belonging to that group and automatically display them in emails, INVC, & ORDS pages
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/48/overview)

#### Changed

- Updated order-history lists to be sorted from newest-to-oldest by default
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/52/overview)
- Moved core JS files & functionality into their own packages
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/34/overview)
- `_variables.scss` organization so that variables are on top, then maps, then `@mixins`
	[PR1](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/38/overview)
	[PR2](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/28/overview)
- Made CTGY, PLST, & SRCH product-listing templates share the same template code [PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/6/overview)
- Leveraged SCSS features & nesting on pre-existing CSS base [PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/11/overview)
- Customer-fields on ACAD, ACED, & OCST display dynamically based on how the Store's Customer-Field settings are configured and their templates are shared using a RT CS -
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/23/overview)
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/14/overview)
- Improved contributor documentation to clarify where & how to organize their code for new packages
- Moved webpack main entry files (main.css & main.js) into JS & CSS Resource Groups so that it adds cache-busting hashes
- Development on features should occur within your own development store and then be moved to [the staging store](https://genesis.mivamerchantdev.com/) after your PR has been approved
- Made the `mkdocs.yml` file use the `strict: true` setting to avoid broken-links and missing files when building docs.

#### Removed

- [npm package manager](https://docs.npmjs.com/about-npm/) dependancy and replaced it with pnpm
- ul/li tags on several forms; in-favor of div tags

#### Fixed

- Mini-modal functionality -
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/62/overview)
- Subscription-edit page UX issues -
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/60/overview)
- Upsell page image-id loading to use image-code instead
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/49/overview)
- PROD page tab display
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/35/overview)
- Product `<title>` content so it display's product information instead of generic content
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/41/overview)
- Continue Shopping Links on BASK & BSKE
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/36/overview)
- Global-header Search Preview display
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/25/overview)
- Product listing sort-by & items-per-page display issue
- Some Axe accessibility changes in the global header/footer [PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/20/overview)
- Navigating directly to PROD & CTGY pages without product/category codes will redirect to NTFD page
- `MMDynamic_Form_Submit` error that occurs when `searchfield` item is unassigned from Account pages (CABK, CPCD) [PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/22/overview)
- Misc. UX Errors
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/55/overview)

### Version 0.5.0

- **Description:** Documenting extensions, changelog, contributing, standards, and adding tests
- **Timeline:** 2020-02-15 to 2020-03-17
- [Release Summary](https://design.mivamerchant.net/projects/PS/versions/10301)
- [Repo](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse?at=refs%2Ftags%2Fv0.5.0)
- [Changes](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/compare/diff?targetBranch=refs%2Ftags%2Fv0.4.0&sourceBranch=refs%2Ftags%2Fv0.5.0&targetRepoId=24)
- [Commits](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/commits?until=refs%2Ftags%2Fv0.5.0)

#### Added

- Added high-level docs
	- Vision, Changelog, Contributing, Developer Guide, Code Standards, License

### Version 0.4.0

- **Description:** More Optimizations & first-site-build QA fixes
- **Timeline:** 2019-12-14 to 2019-02-13
- [Release Summary](https://design.mivamerchant.net/projects/PS/versions/10300)
- [Repo](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/browse?at=refs%2Ftags%2Fv0.4.0)
- [Changes](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/compare/diff?targetBranch=refs%2Ftags%2Fv0.3.0&sourceBranch=refs%2Ftags%2Fv0.4.0&targetRepoId=24)
- [Commits](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/commits?until=refs%2Ftags%2Fv0.4.0)
- **Downloads:**
	- [Framework .pkg File](https://genesis.mivamerchantdev.com/mm5/frameworks/00000001/genesis--0.4-beta.pkg)
	- Components & Layouts:
		- [Components](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/packages/framework/web-server/mivadata/component_layouts--components.xml?at=refs%2Ftags%2Fv0.4.0)
		- [Layouts](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/packages/framework/web-server/mivadata/component_layouts--layouts.xml?at=refs%2Ftags%2Fv0.4.0)

#### Added

- Added a product-card extension

#### Changed

- Consolidated INVC & ORDS order_details templates
- Improved the messages so they can auto-close and removed `$.hook` dependency
- Refactored the directory structure
- Simplified print.css overrides

#### Removed

- Removed printer_friendly INVC & ORDS content blocks

#### Fixed

- Resolved IE11 node_modules/@mv-ps transpilation issues
- Resolved SASS mixin/variable import-order issues

---

### Version 0.3.0

- **Description:** Essentials for first-site-build to use new framework
- **Timeline:** 2019-11-29 to 2019-12-19
- [Release Summary](https://design.mivamerchant.net/projects/PS/versions/10203)
- [Repo](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/browse?at=refs%2Ftags%2Fv0.3.0)
- [Changes](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/compare/diff?targetBranch=refs%2Ftags%2Fv0.2.0&sourceBranch=refs%2Ftags%2Fv0.3.0&targetRepoId=24)
- [Commits](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/commits?until=refs%2Ftags%2Fv0.3.0)

#### Added

- Added web-performance .htaccess file
- Added [PostCSS](https://github.com/postcss/postcss) [autoprefixer plugin](https://github.com/postcss/autoprefixer) for SASS/CSS and removed vendor prefixes in CSS
- [Improved icon loading so that webpack can build svg files into webfont](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/pull-requests/29/overview)
- Added `x-mm-page-code` HTTP Header for JS AJAX calls to identify loaded page
- Added a page-layout that uses Components & Layouts (great for ABUS, CTUS, PRPO, FAQS, etc.)

#### Changed

- Consolidated template code & re-used it across multiple pages wherever possible.
- [ShipEstimate stats & countries are now dynamic based on Store settings](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/pull-requests/42/diff)
- [Normalized CSS naming conventions for scales/ranges/series](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/pull-requests/52/overview)
- Replaced reliance on Miva's `category_tree` item with mvt:do version that only loads sub-categories of the current category.
- [Consolidated image dimension settings to only use: 100px, 360px, 640px, & 960px](https://design.mivamerchant.net/browse/PS-12043)
- Updated PATR to render/rese PROD
- [Split core CSS & JS files](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/commits/061859e31f1720ed957d62381ef3230c6c515237) [into monorepo that](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-monorepo/browse) is published to NPM and imported as a dependency
	- https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-monorepo/browse
	- https://www.npmjs.com/org/mv-ps
	- https://www.npmjs.com/package/@mv-ps/genesis

#### Fixed

- Resolved misc. CSS & HTML validation errors & warnings
- Refactored improved semantics/accessibility throughout site `<forms>`

#### Removed

- Removed page-code-specific id & classes from body tag (ex. `<body id="js-&mvte:page:code;" class="t-page-&mvte:page:code;">`)
	- For CSS, avoid page-specific styles/classes & just focus on the extension/component
	- For JS, use `mivaJS.Page_Code`

#### Security

- Removed XSS vulnerabilities

---

### Version 0.2.0

- **Description:** Starting core foundational optimizations
- **Timeline:** 2019-10-26 to 2019-12-11
- [Release Summary](https://design.mivamerchant.net/projects/PS/versions/10208)
- [Repo](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/browse?at=refs%2Ftags%2Fv0.2.0)
- [Changes](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/compare/diff?targetBranch=refs%2Ftags%2Fv0.1.0&sourceBranch=refs%2Ftags%2Fv0.2.0&targetRepoId=24)
- [Commits](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/commits?until=refs%2Ftags%2Fv0.2.0)

#### Added

- Added [Babel](https://babeljs.io/) to webpack setup
- Created initial kitchen-sink/[pattern-library page](https://genesis.mivamerchantdev.com/pattern-library.html)

#### Changed

- [Improved SASS conversion/implementation](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/pull-requests/27/overview)
	- Media Queries leverage an [include-media sass library](https://github.com/eduardoboucas/include-media) that lets you define and reference media queries with opperators (>,>=,<,<=) and named breakpoints (small, medium, large, wide)
	- Grids & widths are generated based on `$column_count` and `$breakpoints` variables.
	- Standardized units to use variables for commonly used values
- Improved Webpack Setup
- Simplify the default page-template structure
	- [Simplified some of the global-header's logic for showing page-titles/headers](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/31/diff#miva-templates/cssui-global-header.mvt)
	- [Consolidated & moved page-specific `<head>` logic into the global-head item](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/pull-requests/31/diff#miva-templates/cssui-global-head.mvt)
	- [Create four ReadyTheme Content Sections to drive the layout of each page & converted the pages to use the layouts](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/31/diff#miva-templates/abus.mvt)
	- Removed Miva category-tree dependency
	- [Moved page specific logic/content to the page's content item](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/31/diff#miva-templates/abus-content.mvt)
	- Removed page-code logic from global-header/footer and based them on global-variables instead


### Version 0.1.0

- **Description:** Essentials for collaboration
	- The goal of this release was to assemble the code-base from other existing projects into the foundations of what Genesis would be.
- **Timeline:** 2019-10-09 to 2019-10-30
- [Release Summary](https://design.mivamerchant.net/projects/PS/versions/10202)
- [Repo](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/browse?at=refs%2Ftags%2Fv0.1.0)
- [Changes](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/compare/diff?targetBranch=362234c0b7c1abc2db5922ee1f4a4005b969d3c6&sourceBranch=refs%2Ftags%2Fv0.1.0&targetRepoId=24)
- [Commits](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/commits?until=refs%2Ftags%2Fv0.1.0)

#### Added

- Started with [Shadows ReadyTheme 1.0.6](https://github.com/mivaecommerce/readytheme-shadows/tree/v1.0.6)
- Added [webpack](https://webpack.js.org/)
- Added [browsersync](https://browsersync.io/)

#### Changed

- CSS/SASS
	- Converted CSS files to [SASS (SCSS)](https://sass-lang.com/)
	- Updated to adhere to linters
	- Refactored/removed over-use of `calc()`
	- Added in some basic SASS variables

---

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).
