# Contributors

## Leads

- <jaguilar@miva.com>
- <jburton@miva.com>
- <mhegler@miva.com>
- <ssoule@miva.com>

## Developers

<!--
Run this in your terminal to see the contributors of the repo
$ git shortlog -s | cut -c8-
-->

- <aserrano@miva.com>
- <awilliams@miva.com>
- <jaugustavo@miva.com>
- <jtascon@miva.com>
- <kfontana@miva.com>
- <mlucks@miva.com>
- <pstearns@miva.com>
- <rmarin@miva.com>
- <sterry@miva.com>
- <tguefen@miva.com>

## Special Thanks

- <sterry@miva.com>
- <mstillerman@miva.com>
- <rwilson@miva.com>
- [mivaecommerce/readytheme-shadows](https://github.com/mivaecommerce/readytheme-shadows/)
	- <mzimmerman@miva.com>
