# Contributor Guide

This guide is for developers that are looking to help with and develop new features for Genesis.

## Submitting Feature Requests or Bugfixes

1. [Clone the "Genesis: [Issue Template]" in Jira](https://design.mivamerchant.net/browse/PS-15374)
	1. Check the box to "Clone sprint value".
	2. Click the "Create" button
	3. Modify the "Issue Links" section to remove the "clones" link/reference.
2. Fill out the "Description" template with all of the relevant information.
3. Notify <ssoule@miva.com> or any of the [contributors](contributors.md)

## Working on Tasks

!!! attention
	Before helping out, please be sure to review our:

	1. [Prerequisites](../prerequisites.md)
	1. [Setup](setup.md)
	1. [Stores & Access](stores.md)
	1. [Standards](../standards.md)
	1. [Changelog](../changelog.md)

[Overall you should be able to follow our GitFlow process](http://blog.merchant.local/?p=5321)

1. Find a task in the [Genesis - Current Version: Todo list](https://design.mivamerchant.net/issues/?filter=11424)
	- If there aren't any tasks available for you to work on, then you can check the [Genesis - Roadmap Items](https://design.mivamerchant.net/issues/?filter=11606) and look for items in the next version.
2. Notify the Reporter that you're interested in getting started
3. Assign it to yourself, refine, and estimate the task
4. Pull it into your sprint when you don't have more pressing client work
5. Mark it as in-progress.
6. Determine where you should commit your changes:
	- If you're working on fixing or modifying existing Miva store templates or CSS/JS plugins that are used by the Genesis framework, you can find & update the corresponding package in [mvps-genesis/generator-quickstart/app/templates/themes/genesis/src/extensions](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/generator-quickstart/app/templates/themes/genesis/src/extensions)
	- Otherwise, if you're adding a new feature, package, and component that should ship at the theme level of Genesis (Meaning you want the source of your package to be easily editable by developers using Genesis) then develop it as it's own package under [mvps-genesis/browse/optional-extensions](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/optional-extensions/) by creating a new directory to encapsulate your package (ex. `mvps-genesis/browse/optional-extensions/your-new-feature`).
		- Within your feature's package, there is a common directory structure that they should follow. Here is a full example showing most/all possible places where you might add new code. When making your package, only add the files/folders that apply to you.
			- `optional-extensions/your-new-feature/`
				- `lib/`
					- `js/`
						- `yourNewJsFile.js`
					- `scss/`
						- `yourNewScssFile.js`
				- `miva-templates/`
					- `readytheme/`
						- `contentsection/`
							- `your-new-content-section.mvt`
					- `your-new-page.mvt`
				- `miva-server/`
					- `your-new-app/`
						- `your-new-script.php`
				- `index.js`
				- `index.scss`
		- **Please note:** _if you're adding package that will be `pnpm` installed, the create the `index.js` and/or `index.scss` with very minimal content that imports the bulk of the scripts in `lib/js` and/or `lib/scss`._
		- You will also need to write some documentation about how to install/use your package under the `docs/` folder. For example:
			- `docs/features/packages/`
				- `your-new-feature.md`
	- If your new feature is more of a utility based package meaning that you want to control the source of the code and have it available through the monorepo and controled with PNPM, you would elect to create a new package in `/packages`. Packages in this folder are included in the monorepo. Good examples of packages that work well with this are the Event Bus and PageManager classes that are used by Genesis, but should not be modified by develoers on a per project basis. These packages we want to control the updates of and keep a consistant code base across genesis projects.
		- Within your feature's package, there is a common directory structure that they should follow. Here is a full example showing most/all possible places where you might add new code. When making your package, only add the files/folders that apply to you.
			- `packages/your-new-feature/`
				- `lib/`
					- `js/`
						- `yourNewJsFile.js`
					- `scss/`
						- `yourNewScssFile.scss`
				- `miva-templates/`
					- `readytheme/`
						- `contentsection/`
							- `your-new-content-section.mvt`
					- `your-new-page.mvt`
				- `miva-server/`
					- `your-new-app/`
						- `your-new-script.php`
				- `index.js`
				- `index.scss`


7. Create a branch on the from Jira using our branch-naming conventions.
	* For example:
		* `feature/PS-1234-summary-of-feature`
		* `bugfix/PS-1234-summary-of-bug`
7. Apply the changes to your branch and _your own personal dev store_.
	* **Please note:** *We do not want to continue to develop features on [the staging store](https://genesis.mivamerchantdev.com/); please be sure to use your own store for development.*
	* Follow the [local development](../develop/workflow.md#local-development) and [deploying a build to FTP](../develop/workflow.md#deploying-a-build-to-ftp) instructions.
8. Commit your changes & create a pull request when you're done.
	* Copy and [fill-out the pull-request template](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/CONTRIBUTING.md) by clicking on the link below the text-area of the pull-request creation form.
	* Be sure that at-least two of the [lead contributors](contributors.md#leads) are added as Default Reviewers (you will need two of their approvals to complete the merge).
9. Your pull request will be reviewed by the lead contributors.
10. Once your pull request has been merged into `develop`, you will need to migrate your changes from the repo/your-dev store into the [the staging store](https://genesis.mivamerchantdev.com/) store.

## Installing Dependencies for All Packages

- To recursively install dependancies for packages within the monorepo run the following command:
	- `pnpm -r i` which stands for `pnpm recursive install`
	- Alternatively, in the root of the repo, you can run `pnpm run bootstrap`

- Make sure you run this script every time you merge branches or create a new branch from `develop`.

!!! attention
	Do not run `pnpm install` in any of the managed sub-packages! This will cause issues with testing due to improper `node_modules` folder format.

## Adding a New Dependancy

See [pnpm add](https://pnpm.js.org/en/cli/add) documentation for more information about the add command. Here are common use cases for our project:

- Add a dependancy to a *SPECIFIC* package in the monorepo. Use the filter flag to specify an exact package.
	- `pnpm -r add <package>[@version] --filter <package-name>`
	- Add the `-D` or `--save-dev` flags to add it to `devDependancies`

!!! note
	You can also `cd` into a specific package folder and run the standard `pnpm add <package>[@version]` command.

- Add a dependancy to *ALL* packages in the monorepo.
	- `pnpm -r add <package>[@version]`
	- Add the `-D` or `--save-dev` flags to add it to `devDependancies`

!!! note
	Learn more about [pnpm filtering here](https://pnpm.js.org/en/filtering).

## Publishing a Release

1. Bump the package(s) you updated.
	* `cd package/YOUR_PACKAGE && pnpm version [major|minor|patch|[premajor --preid alpha|preminor --preid alpha|prepatch --preid alpha|prelease --preid alpha]`
1. Review which packages were updated thru git after the version bump.
	* If your package is a dependency for other packages it will automatically their reference to the latest version. This maintains the workspace linking.
	* If this happens you will want to also bump the affected packages as well. Follow through this pattern until all packages are updated to the next version.
1. Commit all changes.
1. Run the correct publish command for the type of release you are doing (`pre-release` or `release`).

### Updating the `framework_export.mvt`

1. `cd mvps-genesis`
1. `pnpm run build --prefix generator-quickstart/app/templates/themes/genesis`
1. `pnpm run framework_export`
1. `git commit -m "Updated framework_export with latest build files"`
1. Update the Miva Store's `framework_export` page with updated contents of `generator-quickstart/app/templates/miva-templates/framework_export.mvt`
1. Create a new framework-pkg file from the Miva Store
1. Commit the new framework pkg file in the mvps-genesis repo.

## Tips & Tricks

### pnpm link

You can use [pnpm link](https://pnpm.js.org/en/cli/link) to locally test changes that you want to make to the `@mv-ps` packages.

> Recommended Reading
>
> * [pnpm link - Symlink a package folder](https://pnpm.js.org/en/cli/link)
> * [The magic behind npm link](https://medium.com/@alexishevia/the-magic-behind-npm-link-d94dcb3a81af)
> * [pnpm](https://pnpm.js.org/en/motivation)


### Production Sync Incremental Commits

```
git add \*.json && git commit -m "production sync of json files"
git add -A && git diff --cached -w | git apply --cached -R && git commit -m "production sync of whitespace changes"
git add -A && git commit -m "production sync of template changes"
```
