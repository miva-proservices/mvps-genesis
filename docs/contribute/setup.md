# Setup for Working on Genesis Features

Use the following instructions to prepare a Miva Store and your local development environment so you can work on Genesis tasks, features, and bugfixes

You should only need to follow this process one time, then move onto [working on tasks](index.md#working-on-tasks).

## Main Steps

1. [Install the dev-dependencies on your computer](../prerequisites.md#development-environment)
2. [Install Genesis on your store](#install-genesis-on-your-store)
3. [Clone & Configure the `mvps-genesis` Repo](#clone-configure-the-mvps-genesis-repo)
4. That's it. Continue on to [working on tasks](index.md#working-on-tasks)

## [Ensure your pre-requisites are met](../prerequisites.md)

Do not proceed until you have [prepared your computer with all of the dev-dependencies: node, pnpm, yoeoman, linters, etc.](../prerequisites.md)

## Install Genesis on Your Store

[Follow the Miva store installation process](../install.md)

!!! note
	You will most-likely want to [install the framework from the `develop` branch](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/generator-quickstart/app/templates/miva-server/%25WEB_ROOT%25/%25MIVA_ROOT%25/frameworks/%25STORE_ID%25) or you can log into the [genesis.mivamerchantdev.com](https://genesis.mivamerchantdev.com/mm5/admin.mvc) store and create a fresh framework-pkg file. Just create it with the following framework-code: `genesis-YYYY-MM-DD`

## Clone & Configure the `mvps-genesis` Repo

The following commands show how setup your repo by: cloning the repo, initialize & configure the repo's settings, install dev-dependencies, run a build, and upload the build to your development store.

=== "HTTPS"

	```sh
	git clone https://bitbucket.mivamerchant.net/scm/ps/mvps-genesis.git
	cd mvps-genesis
	pnpm run init
	pnpm run build dev
	# FTP upload the `public` folder to your dev-store: themes/genesis/public
	```

=== "SSH"

	```sh
	git clone ssh://git@bitbucket.mivamerchant.net:7999/ps/mvps-genesis.git
	cd mvps-genesis
	pnpm run init
	pnpm run build dev
	# FTP upload the `public` folder to your dev-store: themes/genesis/public
	```

## Start Working on Tasks

After following the above processes you should be able to run `$ pnpm run start` within the `generator-quickstart/app/templates/themes/genesis/` folder and proceed to start [working on tasks](index.md#working-on-tasks)
