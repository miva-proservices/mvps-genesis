# Stores & Access

The following describes the purpose of the various Genesis development stores and how to access them.

## TL;DR

- [`genesis.mivareadythemes.com`](#main-genesismivareadythemescom)
	- Is setup as the production store which matches the `main` branch and is useful for sharing with clients
	- `ssoule` typically updates this monthly/quarterly as `release` branches are merged into `main`
- [`genesis.mivamerchantdev.com`](#develop-genesismivamerchantdevcom)
	- Is designed to match the `develop` branch and is the main place to do integration testing after features are merged into `develop`
	- `ssoule` typically will update this weekly after PRs/branches are approved and merged into `main`
- [Your Personal Development Store](#feature-bugfix-your-dev-store)
	- Is where `feature` and `bugfix` tasks should be worked on.

## `main` / genesis.mivareadythemes.com

### Purpose

Production version of Genesis used to demonstrate its features for POs, clients, and the public.

This store will primarily only be updated by the [lead-contributors](contributors.md#leads)

### Use

The store should replicate the `main` branch as much as possible.

Update it after `release` & `hotfix` branch pull-requests have been approved & merged into `main`.

### FTP Access Setup

Unlike the dts-store and our client stores, this server requires the use a of an SSH key to connect to the server through SFTP.

The following steps describe how to generate an SSH key, add it to Plesk, and connect to SFNT through FileZilla with your SSH key so you can upload/download files & pre-provide into the store.

#### 1. Generate an SSH Key

Windows

1. [Download and install PuttyGen](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
1. Launch PuttyGen
1. Click the "Generate" button
1. Move your mouse around over the dedicated area to generate randomness until the progress bar is full
1. Update the "Key Comment" with your first-name-initial + last-name (ex. Change the "Key Comment" from `rsa-key-20200602` to `YOURNAME`)
1. Enter the "Key Passphrase" & "Confirm passphrase"
1. Click the "Save Private Key" button and save it to a safe location on your computer (ex. `C:\Users\YOURNAME\Documents\SSH Keys\genesis_rsa.ppk`)
1. In the Putty Key Generator window, select-all of the contents of the "Public key for pasting into OpenSSH authorized_keys file:" text & copy it.

Mac

1. Open Terminal
1. cd to `~/.ssh`
1. Generate a key with the following command: `ssh-keygen -t rsa`
1. Save the file as `genesis_rsa`
1. Enter your passphrase
1. This will create two files with your ssh folder. `genesis_rsa` and `genesis_rsa.pub`. The `.pub` file is your public key and the extension-less file is your private key.
2. Copy the contents of your pub file: `pbcopy < genesis_rsa.pub`

#### 2. Add the Public Key to Plesk

1. [Open the store in Plesk](https://saasmgmt.mivamerchant.net/?domain=infra-pub2)
1. In Plesk, navigate to "Domains > genesis.mivareadythemes.com"
1. Click on "SSH Keys" in the right column.
1. Click the "Add Key" button.
1. Paste your public-key
1. Click the "Add" button.

#### 3. Connect to Server w/ SSH

FileZilla

1. Open FileZilla
1. Create a new site/connection (ex. "File > Site Manager > New Site")
1. Set the "Protocol" to "SFTP"
1. Set the "Host" to `216.188.132.238`
1. Set the "Logon Type" to "Key file"
1. Set the "User Name" to `genesis`
1. Set the "Key file" by browsing to the private-key file you created in Step 1 (ex. `C:\Users\YOURNAME\Documents\SSH Keys\genesis_rsa.ppk` or `~/.ssh/genesis_rsa`)
1. Click "Connect"
1. You will be prompted to enter the "Passphrase" you entered in Step 1. Enter it and click "Ok"
1. That's it. You should be connected.

Other

1. Consult documentation of your preferred FTP/SSH client.

## `develop` / genesis.mivamerchantdev.com

### Purpose

A staging environment for Genesis used to run integration tests and exemplify the next release of Genesis.

### Use

The store should replicate the `develop` branch as much as possible.

Update it after `bugfix` & `feature` branch pull-requests have been approved & merged into `develop`.

### FTP Access

[View PDF](https://design.mivamerchant.net/secure/attachment/13406/13406_mvps-framework-access+%282%29.pdf)

## `feature` & `bugfix` / Your Dev Store

Please do all development work on your own dev store. After the change has been approved & merged into `develop` or `main` you can then apply the changes to the corresponding store.
