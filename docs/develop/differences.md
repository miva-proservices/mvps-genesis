# Developer Differences

The following outlines the specific changes that have been made from the Elements, Shadows, Colossus frameworks that you may be familiar with.

For a high-level summary, you can view [Changelog](../changelog.md)

- [Miva Store > Page Layouts](../features/miva-store/page-layouts.md)
	- Each page in the Miva Store has altered it's over-all template layout. Most of what you already know & expect have been moved or consolidated.
- Customer-fields on ACAD, ACED, & OCST display dynamically based on how the Store's Customer-Field settings are configured and their templates are shared using a RT CS - 
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/23/overview) |
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/14/overview)
- Made CTGY, PLST, & SRCH product-listing templates share the same template code [PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/6/overview)
- A global `product_card_iterator` RT CS that can be used throughout the sites product listings (CTGY, PLST, SRCH, SFNT, PROD-related-products, etc) - 
	[View Docs](../features/miva-store/product-card.md) | 
	[PR](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/pull-requests/39/overview)
- PATR renders PROd
- Consolidated INVC & ORDS templates into an `order_details` RT CS

> // TODO: Describe more notable changes from elements, shadows, previous workflows, etc.
