# Developer Guide

This guide is for developers that are looking to theme & customize a store that is using Genesis.

If you're interested in working on Genesis or adding features to it, please see the [contributing guide](../contribute/index.md)

---

!!! attention
	Before getting started, review the [prerequisites](../prerequisites.md) for using Genesis to ensure your Miva store, development-environment, & developer-experience are ready.

---

## Table of Contents

 * [Initial Setup](setup.md)
 * [Developer Workflow](workflow.md)
 * [Best Practices](best-practices.md)
 * [Differences from Elements/Shadows](differences.md)
