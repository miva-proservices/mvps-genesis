# Optional Modifications

The following are a couple commonly requested or made modifications that you may be interested in making to your Genesis setup.

## Including jQuery Globally in Webpack

Only use this feature if you need include jQuery globally without requiring the developer to explicitly import it each time. If needed, update your `webpack.common.js` file's plugins array to include the following code:

```js
new webpack.ProvidePlugin(
    {
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
    }
)
```

## Adding Vue.js to your project
The Genesis webpack file has already been configured to allow for the compilation of Vue [Single Page Components](https://vuejs.org/v2/guide/single-file-components.html)

To start using Vue.js you will need to include it as a dependency for your project. You can do this by running the following pnpm command in your Genesis framework directory
`pnpm install vue --save`

Once you have installed Vue as a dependency, you can include Vue in either your theme's Global.js file, or any of the page specific JS files with the following import code
```js
import Vue from 'vue/dist/vue.js';
```
This imports the Vue.js Full build as opposed to the default Runtime-only build. The full build is required for the .vue files to compile correctly. More information about the different Vue.js builds [can be found here](https://vuejs.org/v2/guide/installation.html#Explanation-of-Different-Builds)

With those two steps complete you are now ready to start using Vue in your build. From this point forward you should be able to follow the standard [Vue.js documentation](https://vuejs.org/v2/guide/)

Per the Vue.js documentation, components can be regestered globally or local to your specific Vue component. Here is an example of a global component being registered. In my case I put this in my theme's Global.js file:
```js
Vue.component('vue-drawer', require('../components/vue-drawer.vue').default);
```

Below your code that registers all of your global components, you would place the following to create a new Vue.js instance and assign it to an element in your DOM. I also did this in my Global.js file
```js
var app = new Vue({
	el: '#app',
});
```

This attaches Vue to an element on the page that has an ID of `app` (This can be named anything you want). 

## Independent Theming per Store

This modification allows you to develop different themes for each store within a "mall".

!!! warning
    If you have not followed the instructions within [Setup](setup.md#setup-repo) first than go back.

Once you have configured your repository with Genesis you can proceed with modifying your folder structure to accommodate a multi-store environment.


### Step 1: Copy your theme folder

1. Open your terminal and cd to your project folder
2. Run the following commands:
```
cd themes
mkdir 00000001
cp genesis 00000001
```

### Step 2: Update your theme-settings.json

1. Open `theme-settings.example.json`
2. Modify the `themePath` and `launchThemePath` values by adding your store id as a folder between `themes` and `genesis`. For example:
```
"themePath": "themes/00000001/genesis/"
```
3. If this is an existing store notify other developers that you have made changes to the `theme-settings.json` file and that they should reference the new example file.

### Step 3: Repeat

Repeat steps 1 & 2 for each store. Make sure you use the correct store id.

Once you are finished you can safely remove the genesis folder contained directly within the `themes` folder.

```
rm -rf genesis
```

### Step 4: Update your theme path (MVT)

1. Open `cssui-html-profile.mvt` file
2. Modify the `g.theme_path` variable values to dynamically include the theme path. Example:
```
<mvt:comment>Set your theme file path here.</mvt:comment>
<mvt:if expr="g.launch EQ 1">
	<mvt:assign name="g.theme_path" value="'themes/' $ padl( g.store:id, 8, '0' ) $ '/genesis-launch'" />
<mvt:else>
	<mvt:assign name="g.theme_path" value="'themes/' $ padl( g.store:id, 8, '0' ) $ '/genesis'" />
</mvt:if>
```