# Setup for Genesis Site-Build

Use the following instructions to prepare a Miva Store and your local development environment for developing a site-build that will redesign a site with the Genesis framework.

You should only need to follow this process one time, then move onto the [Development Workflow](workflow.md) process and get started customizing your site.

## Main Steps

1. [Install the dev-dependencies on your computer](../prerequisites.md#development-environment)
2. [Install Genesis on your store](#install-genesis-on-your-store)
3. [Setup your Git & BitBucket repo](#setup-repo)
4. [Configure your development environments](#local-computer-setup)
5. That's it. Continue on to the [developer workflow](workflow.md)

## [Ensure your pre-requisites are met](../prerequisites.md)

Do not proceed until you have [prepared your computer with all of the dev-dependencies: node, pnpm, yoeoman, linters, etc.](../prerequisites.md)

## Install Genesis on Your Store

[Follow the Miva store installation process](../install.md)

## Setup Repo

=== "New Repo/Client"

	This is the setup process that works well when the client/store does not already have a repo

	1. Create your local working area:
		- Clone a repo and create a directory for it
		- Or create a new directory and `$ git init`
	2. Run `$ yo @mvps-genesis/quickstart` and answer the questions to match the site's existing folder structure.
	3. Run the `pnpm run init` command from the root.
	4. Use FTP to upload your newly-created `public/` directory to the store's `themes/genesis/public/` directory
	5. Use the [`mvps-template-export` module](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-template-export/browse) to export the store's templates after Genesis has been installed.
	6. Download the `templates.tar`, unzip templates, and put their files in the `miva-templates` folder
	10. Commit & push your completed repo

	**Example Repo Setup Commands**

	```sh
	mkdir some-store
	cd some-store
	git init
	mkdir -p themes/genesis
	cd ../
	# Use yeoman to download the latest version of the genesis framework
	yo @mvps-genesis/quickstart
	# initialize dependencies, git-lfs and git hooks
	pnpm run init
	# Update theme-settings.json with the urls of your store
	# Add "theme-settings.json" to the store-repo's .gitignore file
	pnpm install
	pnpm run start
	pnpm run build
	# Upload public/ to your store through FTP to: /httpdocs/mm5/themes/genesis/public
	# Import the genesis pkg file on the store
	# Export the template with mvps-template-export and add them to the store's repo
	# Test everything to make sure it works
	git commit -m "Inital commit with Genesis"
	git push
	```

=== "Existing Repo/Client"

	The following steps show how you can setup a repo for an existing Miva client, where you'd like to keep the main/develop branches separate from the work that is being done for the site-build

	1. Find the store's repo that you're going to be building the redesign with (ex. [www.marketviewliquor.com](https://bitbucket.mivamerchant.net/projects/PS/repos/www.marketviewliquor.com/browse))
	2. Clone it to your local machine
	3. Create some redesign branches, (`redesign/main` & `redesign/develop`), from the current `main` branch. These new `redesign` branches will be used to maintain a separate GitFlow that is independent of the current live site.
	4. Checkout the `redesign/develop` branch.
	5. Run `$ yo @mvps-genesis/quickstart` and answer the questions to match the site's existing folder structure.
	6. Run the `pnpm run init` command from the root.
	7. Use FTP to upload your newly-created `public/` directory to the store's `themes/genesis/public/` directory
	8. Use the [`mvps-template-export` module](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-template-export/browse) to export the store's templates after Genesis has been installed.
	9. Download the `templates.tar`, unzip templates, and copy & replace them over the store's current templates.
	10. Commit your changes, tag them with the `v.2.0.0` to commemorate the start of the redesign
	11. Push up your changes to the BitBucket repo and make sure `redesign/main` & `redesign/develop` are in sync with all of these changes.
	12. That's it! Now start working on features by branching from `redesign/develop`.

	**Example Repo Setup Commands**

	Here's a command line depiction that shows the previously described steps. *Note: You may need to change the directories and paths depending on your store's setup (ex. htdocs vs httpdocs, mm5 vs Merchant2, etc.)*

	```sh
	git clone https://bitbucket.mivamerchant.net/scm/ps/www.marketviewliquor.com.git
	cd www.marketviewliquor.com
	git checkout main
	git checkout -b redesign/main
	git checkout -b redesign/develop
	mkdir -p miva-server/httpdocs/mm5/themes/genesis
	cd ../
	# Use yeoman to download the latest version of the genesis framework
	yo @mvps-genesis/quickstart
	# initialize dependencies, git-lfs and git hooks
	pnpm run init
	# Update theme-settings.json with the urls of your store
	# Add "theme-settings.json" to the store-repo's .gitignore file
	pnpm install
	pnpm run start
	pnpm run build
	# Upload public/ to your store through FTP to: /httpdocs/mm5/themes/genesis/public
	# Import the genesis pkg file on the store
	# Export the template with mvps-template-export and add them to the store's repo
	# Test everything to make sure it works
	git commit -m "added Genesis to start site redesign"
	git tag -a v2.0.0
	git push
	# Create a pull request in BitBucket to merge the changes from `redesign/develop` to `redesign/main`
	```

## Setting up a "Mall" Store

See [Independent Theming per Store](modifications.md#independent-theming-per-store).

## Local Computer Setup

[After the repo has been setup](#setup-repo), other developers working on the project may need to go through this local-computer setup.

1. [Clone the repo to your machine](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse)
2. Open your terminal and cd into the Genesis theme directory of the store's repo (ex. `themes/genesis/`)
3. Run `pnpm install`.
4. Create a copy of `theme-settings.example.json` and rename it to `theme-settings.json`.
5. Open `theme-settings.json` and configure the `moduleRoot`, `themePath`, `launchThemePath` and `proxyUrl` variables for each environment.

!!! note
	After following the above processes you should be able to run `$ pnpm run start` from the geneis directory and proceed to follow the [Developer Workflow](workflow.md)
