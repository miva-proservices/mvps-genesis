# Development Workflow

## Local Development

For our pseudo-local development process, we utilize a node package called [browser-sync](https://www.npmjs.com/package/browser-sync). Specifically, the ["proxy" feature of browser-sync](https://browsersync.io/docs/options#option-proxy).

!!! note
	Make sure you have configured your `theme-settings.json` file from the [Setup > Local Computer Setup](setup.md#setup-repo) section.

Once your have configured your `theme-settings.json` file you can run the following pnpm commands to proxy your desired environment:

```
pnpm run start				# defaults to "dev" environment
pnpm run start [env]		# replace [env] with any environment defined within `theme-settings.json`
```

If you want to proxy to an environment's "-launch" path use:

```
pnpm run start-launch [env]
```

For help run:

```
pnpm run start -- -h
```

After the command as finished it will provide you a localhost address where the proxy is running. Open that localhost address in your browser to begin development. Any change made to files within the `src` directory will force a re-build of webpack and will trigger a refresh in your browser window.

## Local Development and Miva Templates

Currently, we do not have an automatic deployment system for Miva templates. As of right now, simply logging into the proxied environments Admin (not via localhost) and make updates will work. You will have to manually refresh your localhost browser window after updating templates.

!!! warning
	Make sure you ALWAYS update your local, repo version of the template before pasting into the Admin screen to ensure you keep your repository up to date

## Deploying a build to FTP

When deploying a build to your site via FTP, please follow the steps below.

1. Ensure your `main` branch is up to date with all of changes you want to take live.
2. Checkout `main` branch.
3. Run `pnpm run build-launch`.
4. Upload the `public` folder into the `genesis-launch` folder on your FTP server.
5. Run QA tests on the front-end of the store via the `?launch=1` query parameter.
6. Once you pass QA run log into the Miva Admin and update the User Interface > Settings > HTML Profile template and uncomment out the code that assigns `g.launch` to `1`. This will force all live traffic be served the new build.
7. Run `pnpm run build`.
8. Upload the `public` folder into the `genesis` folder on your FTP server.
9. Run QA tests on the front-end of the store via the `?launch=0` (or anything but `1`) query parameter.
10. Once you pass QA you can re-comment the code within the HTML Profile template to finalize the launch.
