# FAQs

## How do I add this to my store?

- To build a new site with Genesis, [see the developer setup documentation](develop/index.md)
- Otherwise, to contribute a feature/bugifx to Genesis, [see the contributor setup documentation](contribute/index.md)

## I have a great idea for the framework, what should I do?

See the [Contributing > Submitting Feature Requests or Bugfixes](contribute/index.md#submitting-feature-requests-or-bugfixes) and [Contributing > Working on Tasks](contribute/index.md#working-on-tasks) sections.

## Do we have to support IE 11?

Unfortunately, Yes. [See why...](standards.md#a-note-about-internet-explorer)

## How do I resolve this error...

### `$ pnpm run start` results in `Node Sass could not find a binding for your current environment`

#### Problem:

Here's an excerpt from the build log after trying to run `$ pnpm run start`:

```
Node Sass could not find a binding for your current environment: Windows 64-bit with Node.js 13.x

    Found bindings for the following environments:
      - Windows 64-bit with Node.js 12.x

    This usually happens because your environment has changed since running `pnpm install`.
```

#### Cause:

I had recently updated my computer's Node from 12.x to 13.x.

#### Solution:

```sh
$ pnpm rebuild node-sass
# Or just...
$ pnpm rebuild
```

### Why do my icons look incorrect?

My icons look like this:

![Broken Icon Images](images/charset--icons-incorrect.png)

But they should look like this:

![Correct Icon Images](images/charset--icons-correct.png)

### Solution:

Ensure the character-set is set to `utf-8`. Within the Miva Admin, set the `Store Settings > Character Set` is set to `utf-8`.

!!! note
    If you are adding this for the first time, or changing it from a different value, then you may need to QA the site to ensure that text-characters look correct.
