# Buttons

CSS classes for styling HTML button elements.

## Usage

Add a `c-button` class to any html button tag.

```html
<button class="c-button">Default Button</button>
```

## Customizable Variables

- `$btn_radius`
- `$font_size`

## Variations

Add the following classes to a `c-button` element to further customize it

### Button Sizes

- `c-button--xsmall`
- `c-button--small`
- _`c-button` is medium by default_
- `c-button--large`
- `c-button--xlarge`

```html
<button class="c-button c-button--xsmall">Tiny Button</button>
<button class="c-button c-button--small">Small Button</button>
<button class="c-button">Medium Button</button>
<button class="c-button c-button--large">Large Button</button>
<button class="c-button c-button--xlarge">Huge Button</button>
```
### Disabled State

Styles will automatically apply when the `disabled` attribute is added to a `c-button`, or you can add `is-disabled` class to it.

```html
<button class="c-button" disabled>Can't click me</button>
<button class="c-button is-disabled">You shouldn't click me</button>
```


### Error State


```html
<button class="c-button is-error">Button</button>
<label class="is-error">
	<button class="c-button">Button</button>
</label>
```


### Modifiers

- `c-button--flush`
	- Removes left/right padding
- `c-button--full`
	- Sets width to 100%
- `c-button--clear`
	- Makes the background transparent
- `c-button--hollow`
	- Makes the background transparent and inherits the `border-color` & `color`
- `c-floating-cta`
	- Turns a button into an absolutely positioned element that will respond to o-layout justify and align classes.
- `u-cta-style--pattern-bg`
	- Turns a button into a style element with a background image.

```html
<button class="c-button c-button--full">Full Width Button</button>
<button class="c-button c-button--flush">Full Width Button</button>
<button class="c-button c-button--clear">Clear Button</button>
<button class="c-button c-button--hollow">Hollow Button</button>
<div class="c-floating-cta">Floating Button</div>
<button class="c-button u-cta-style--pattern-bg">Button with Pattern BG</button>
```
