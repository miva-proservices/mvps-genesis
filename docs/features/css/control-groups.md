# Control Groups

To group related buttons and/or inputs together, you can use the `c-control-group` wrapping element. This will bunch the related elements together and remove the spacing between them.

## Usage

```html
<div class="c-control-group">
	<button class="c-button c-control-group__button">Button 1</button>
	<button class="c-button c-control-group__button">Button 2</button>
	<button class="c-button c-control-group__button">Button 3</button>
</div>
```
## Variations

Add the following classes to the `c-control-group`

### `c-control-group--rounded`

Round both sides of the control-group.

```html
<div class="c-control-group c-control-group--rounded">
	<button class="c-button c-control-group__button">Button 1</button>
	<button class="c-button c-control-group__button">Button 2</button>
	<button class="c-button c-control-group__button">Button 3</button>
</div>
```

### `c-control-group--rounded-right`

Round the right side of the control-group.

```html
<div class="c-control-group c-control-group--rounded-right">
	<button class="c-button c-control-group__button">Button 1</button>
	<button class="c-button c-control-group__button">Button 2</button>
	<button class="c-button c-control-group__button">Button 3</button>
</div>
```

### `c-control-group--rounded-left`

Round the left side of the control-group.

```html
<div class="c-control-group c-control-group--rounded-left">
	<button class="c-button c-control-group__button">Button 1</button>
	<button class="c-button c-control-group__button">Button 2</button>
	<button class="c-button c-control-group__button">Button 3</button>
</div>
```

### `c-control-group--stacked`

Display the buttons top-to-bottom instead of left-to-right

```html
<div class="c-control-group c-control-group--stacked">
	<button class="c-button c-control-group__button">Button 1</button>
	<button class="c-button c-control-group__button">Button 2</button>
	<button class="c-button c-control-group__button">Button 3</button>
</div>
```
