# Grids and Layouts

## Common HTML Page Structures

Page layouts typically follow this combinations of `o-site-wrapper`, `o-wrapper`, `o-layout` & `o-layout-item` elements.

```html
<body class="o-site-wrapper">
	<header class="o-wrapper o-wrapper--flush o-wrapper--full">
		<div class="o-layout">
			<div class="o-layout__item u-width-10 u-offset-1">
				Top Content
			</div>
		</div>
	</header>
	<main class="o-wrapper">
		<div class="o-layout o-layout--row-reverse">
			<div class="o-layout__item u-width-8">
				Main Right Content
			</div>
			<div class="o-layout__item u-width-4">
				Extra Left Content
			</div>
		</div>
	</main>
	<footer class="o-wrapper o-wrapper--flush o-wrapper--full">
		<div class="o-layout u-grids-4">
			<div class="o-layout__item">
				Left Content
			</div>
			<div class="o-layout__item">
				Middle Content
			</div>
			<div class="o-layout__item">
				Right Content
			</div>
		</div>
	</footer>
</body>
```

## Wrapper

Wrappers are used as the top-most page-layout classes to set max-widths.

- `.o-wrapper` can be used to constrain the container to the maximum site width variable while including the right and left padding.
- Add `.o-wrapper--full` to `.o-wrapper` to set it to `100vw`.


## Layouts

A set of flex-grid layout classes that can be combined with grid & width classes to create fluid and responsive page layouts.

- `.o-layout` is the outer block class which adds offsets to counter the outer gutters. (`.o-layout` is used similarly to the `.row` classes found in other frameworks.)
- `.o-layout__item` is the element used for layout items which sets gutter spacing and essential styling. (`.o-layout__item` is used similarly to `.col` classes in other frameworks.)

!!! note
	All direct child elements of `.o-layout` must be `.o-layout__item` elements.

```html
<div class="o-layout">
	<div class="o-layout__item">
		<div>Some Content</div>
	</div>
	<div class="o-layout__item">
		<div>Some More Content</div>
	</div>
	<div class="o-layout__item">
		<div>Still More Content</div>
	</div>
</div>
```

### Layouts Gutters

#### `o-layout--flush`

Remove the gutters between items.

```html
<div class="o-layout o-layout--narrow">
	<div class="o-layout__item">
		<div>Some Content</div>
	</div>
	<div class="o-layout__item">
		<div>Some More Content</div>
	</div>
	<div class="o-layout__item">
		<div>Still More Content</div>
	</div>
</div>
```

#### `o-layout--narrow`

Have smaller gutters between items.

```html
<div class="o-layout o-layout--narrow">
	<div class="o-layout__item">
		<div>Some Content</div>
	</div>
	<div class="o-layout__item">
		<div>Some More Content</div>
	</div>
	<div class="o-layout__item">
		<div>Still More Content</div>
	</div>
</div>
```

#### `o-layout--wide`

Have larger gutters between items.

```html
<div class="o-layout o-layout--">
	<div class="o-layout__item">
		<div>Some Content</div>
	</div>
	<div class="o-layout__item">
		<div>Some More Content</div>
	</div>
	<div class="o-layout__item">
		<div>Still More Content</div>
	</div>
</div>
```

### Other Modifiers

#### `o-layout--spaced`

Include vertical gutters on layout items

```html
<div class="o-layout o-layout--spaced">
	<div class="o-layout__item">
		<div>Some Content</div>
	</div>
	<div class="o-layout__item">
		<div>Some More Content</div>
	</div>
	<div class="o-layout__item">
		<div>Still More Content</div>
	</div>
</div>
```

#### `o-layout--row-reverse`

Will reverse the display order of the child `.o-layout__item` elements

```html
<div class="o-layout o-layout--row-reverse">
	<div class="o-layout__item">
		<div>Some Content</div>
	</div>
	<div class="o-layout__item">
		<div>Some More Content</div>
	</div>
	<div class="o-layout__item">
		<div>Still More Content</div>
	</div>
</div>
```

#### `o-layout--column`

Will display the child `.o-layout__item` elements in a stacked list.

```html
<div class="o-layout o-layout--column">
	<div class="o-layout__item">
		<div>Some Content</div>
	</div>
	<div class="o-layout__item">
		<div>Some More Content</div>
	</div>
	<div class="o-layout__item">
		<div>Still More Content</div>
	</div>
</div>
```

#### `o-layout--column-reverse`

Will display the child `.o-layout__item` elements in a stacked list and display them in reverse order.

```html
<div class="o-layout o-layout--column-reverse">
	<div class="o-layout__item">
		<div>Some Content</div>
	</div>
	<div class="o-layout__item">
		<div>Some More Content</div>
	</div>
	<div class="o-layout__item">
		<div>Still More Content</div>
	</div>
</div>
```

### Grids, Widths, & Offsets

A set of utility classes allow you to specify the widths & offsets of `.o-layout__item` elements.

The classes will be created based on the `$column_count` & `$breakpoint_shortcodes` variables in `_varaiibles.scss`.

## Widths

Specify the number of columns that an `.o-layout__item` should occupy.

**Class Pattern:** - `u-width-[Column Count]`

The "Column Count" numbers will range from 1 to the value of the `$column_count` variable of the `_variables.scss` file. (`$column_count` defaults to 12)

```html
<div class="o-layout">
	<div class="o-layout__item u-width-2">
		<div>Some Content</div>
	</div>
	<div class="o-layout__item u-width-3">
		<div>Some More Content</div>
	</div>
	<div class="o-layout__item u-width-7">
		<div>Still More Content</div>
	</div>
</div>
```

### Responsive Widths

Widths can also be restricted to be active at different breakpoints.

- **Class Pattern:** `u-width-[Column Count]--[Breakpoint Shortcode]`

The "Breakpoint Shortcode" will correspond to the map-values of the `$breakpoint_shortcodes` variable in the `_variables.css` file. The default values will be: s, m, l, xl, xxl.


For example, you can specify that an `.o-layout__item` should occupy 12 columns on the xs-breakpoint and six columns on the large breakpoint:

```html
<div class="o-layout">
	<div class="o-layout__item u-width-12 u-width-6--l">
		<div>Some Content</div>
	</div>
	<div class="o-layout__item u-width-12 u-width-6--l">
		<div>Some More Content</div>
	</div>
</div>
```

## Grids

The `.u-grids-` utility classes give you the ability to create an automatic number of columns based on the class used; media breakpoints are included.

- **Class Pattern:** `u-grids-[Column Count]`

For example, the following will create a three-column structure in which each column will fluidly fill one-third of the width of the parent with the remainder elements wrapping as needed.

```html
<div class="o-layout u-grids-3">
	<div class="o-layout__item">
		Content 1
	</div>
	<div class="o-layout__item">
		Content 2
	</div>
	<div class="o-layout__item">
		Content 3
	</div>
	<div class="o-layout__item">
		Content 4
	</div>
	<div class="o-layout__item">
		Content 5
	</div>
</div>
```

### Grid Breakpoints

Grids can also be restricted to be active at different breakpoints.

- **Class Pattern:** `u-grids-[Column Count]-[Breakpoint Shortcode]`
- **Class Examples:**
	- `u-grid-6--s`
	- `u-grid-4--m`
	- `u-grid-3--l`

For example, we can create a structure in which each column will be 100% width until we enter our medium breakpoint, then they will be one-third the width of the parent container with the remainder elements wrapping as needed.

```html
<div class="o-layout u-grids-1 u-grids-3--m"">
	<div class="o-layout__item">
		Content 1
	</div>
	<div class="o-layout__item">
		Content 2
	</div>
	<div class="o-layout__item">
		Content 3
	</div>
	<div class="o-layout__item">
		Content 4
	</div>
	<div class="o-layout__item">
		Content 5
	</div>
</div>
```


