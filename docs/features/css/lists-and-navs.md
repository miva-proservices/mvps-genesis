# Lists & Navs

## Navs

### Navigation

Simple navigation component that could be used for a global-header.

<nav class="c-navigation">
	<input id="mainNavButton" class="c-navigation__trigger" type="checkbox">
	<label class="c-navigation__label" for="mainNavButton">Menu</label>
	<ul class="c-navigation__row">
		<li class="c-navigation__list"><a class="c-navigation__link" href="#">All Products</a></li>
		<li class="c-navigation__list"><a class="c-navigation__link" href="#">Category 1</a></li>
		<li class="c-navigation__list"><a class="c-navigation__link" href="#">Category 2</a></li>
		<li class="c-navigation__list"><a class="c-navigation__link" href="#">Category 3</a></li>
		<li class="c-navigation__list"><a class="c-navigation__link" href="#">More</a></li>
	</ul>
</nav>

### Menu

Here is a component that is commonly used in our global-footer

```html
<nav class="c-menu">
	<h5 class="c-menu__title">Quick Links</h5>
	<ul class="c-menu__list">
		<li><a class="c-menu__link" href="#">About Us</a></li>
		<li><a class="c-menu__link" href="#">FAQs</a></li>
		<li><a class="c-menu__link" href="#">Contact Us</a></li>
		<li><a class="c-menu__link" href="#">Become an Affiliate</a></li>
	</ul>
</nav>
```

## Lists

`<ul>` & `<ul>` helpers

### Default

```html
<ul>
	<li>List Item 1</li>
	<li>List Item 2</li>
	<li>List Item 3</li>
</ul>
```

### `o-list-bare`

Removing bullets and indentation.

```html
<ul class="o-list-bare">
	<li>List Item 1</li>
	<li>List Item 2</li>
	<li>List Item 3</li>
</ul>
```

### `o-list-block`

Displays any list of items into stacked blocks.

```html
<ul class="o-list-block">
	<li class="o-list-block__item">List Item 1</li>
	<li class="o-list-block__item">List Item 2</li>
	<li class="o-list-block__item">List Item 3</li>
</ul>
```

### `o-list-inline`

Displays a list of items in one line.

```html
<ul class="o-list-inline">
	<li class="o-list-inline__item">List Item 1</li>
	<li class="o-list-inline__item">List Item 2</li>
	<li class="o-list-inline__item">List Item 3</li>
</ul>
```

### `o-list-inline--narrow`

Inline lists with smaller margins.

```html
<ul class="o-list-inline o-list-inline--narrow">
	<li class="o-list-inline__item">List Item 1</li>
	<li class="o-list-inline__item">List Item 2</li>
	<li class="o-list-inline__item">List Item 3</li>
</ul>
```

### `o-list-inline--narrow`

Inline lists with larger margins.

```html
<ul class="o-list-inline o-list-inline--wide">
	<li class="o-list-inline__item">List Item 1</li>
	<li class="o-list-inline__item">List Item 2</li>
	<li class="o-list-inline__item">List Item 3</li>
</ul>
```
