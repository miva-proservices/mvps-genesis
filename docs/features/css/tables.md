# Tables

## Basic Table

```html
<table class="o-table">
	<tr>
		<th>First name</th>
		<th>Last name</th>
	<tr>
		<td>John</td>
		<td>Doe</td>
	</tr>
	<tr>
		<td>Jane</td>
		<td>Smith</td>
	</tr>
</table>
```

### Padding Sizes

#### `o-table--narrow`

Sets the `td` & `th` padding to `$unit_small`

```html
<table class="o-table o-table--narrow">
	<tr>
		<th>First name</th>
		<th>Last name</th>
	<tr>
		<td>John</td>
		<td>Doe</td>
	</tr>
	<tr>
		<td>Jane</td>
		<td>Smith</td>
	</tr>
</table>
```

#### `o-table--wide`

Sets the `td` & `th` padding to `$unit_large`

```html
<table class="o-table o-table--wide">
	<tr>
		<th>First name</th>
		<th>Last name</th>
	<tr>
		<td>John</td>
		<td>Doe</td>
	</tr>
	<tr>
		<td>Jane</td>
		<td>Smith</td>
	</tr>
</table>
```

### Modifiers

#### `o-table-borderless`

Removes default border-bottom on table rows

```html
<table class="o-table o-table--borderless">
	<tr>
		<th>First name</th>
		<th>Last name</th>
	<tr>
		<td>John</td>
		<td>Doe</td>
	</tr>
	<tr>
		<td>Jane</td>
		<td>Smith</td>
	</tr>
</table>
```

#### `o-table--fixed`

Sets the `table-layout` property to `fixed`

```html
<table class="o-table o-table--fixed">
	<tr>
		<th>First name</th>
		<th>Last name</th>
	<tr>
		<td>John</td>
		<td>Doe</td>
	</tr>
	<tr>
		<td>Jane</td>
		<td>Smith</td>
	</tr>
</table>
```

## Styled Tables

### Simple

```html
<table class="c-table-simple">
	<thead>
		<tr class="c-table-simple__row">
			<th class="c-table-simple__cell">First Name</th>
			<th class="c-table-simple__cell">Last Name</th>
			<th class="c-table-simple__cell">Age</th>
		</tr>
	</thead>
	<tbody>
		<tr class="c-table-simple__row">
			<td class="c-table-simple__cell">Roger</td>
			<td class="c-table-simple__cell">Waters</td>
			<td class="c-table-simple__cell">73</td>
		</tr>
		<tr class="c-table-simple__row">
			<td class="c-table-simple__cell">Nick</td>
			<td class="c-table-simple__cell">Mason</td>
			<td class="c-table-simple__cell">73</td>
		</tr>
		<tr class="c-table-simple__row">
			<td class="c-table-simple__cell">David</td>
			<td class="c-table-simple__cell">Gilmour</td>
			<td class="c-table-simple__cell">71</td>
		</tr>
	</tbody>
</table>
```

### Striped

```html
<table class="c-table-stripped">
	<thead>
		<tr class="u-bg-gray-30">
			<th>First Name</th>
			<th>Last Name</th>
			<th>Age</th>
		</tr>
	</thead>
	<tbody>
		<tr class="c-table-stripped__row">
			<td>Roger</td>
			<td>Waters</td>
			<td>73</td>
		</tr>
		<tr class="c-table-stripped__row">
			<td>Nick</td>
			<td>Mason</td>
			<td>73</td>
		</tr>
		<tr class="c-table-stripped__row">
			<td>David</td>
			<td>Gilmour</td>
			<td>71</td>
		</tr>
	</tbody>
</table>
```
