# Typography

## Heading & Font Styles

Separate your css-concerns between the look & feel, [Font Styles](#font-styles) & the vertical-rhythm/box-model/dimensions of the type; [Heading Styles](#heading-styles).

### Font Styles

Font styles are focus on the look-and-feel of the text (regardless of it's size or dimensions).

Focus on things like: `font-family`, `font-style`, `font-weight`, `text-transform`, `color`, etc.

That way you can easily combine font-style-classes with headings, buttons, links, or body copy and you won't be have conflicting/over-writing css-properties.

!!! warning
	For Font Styles, avoid using scale/dimensional css-properties that are reserved for the [Heading Styles](#heading-styles).

```html
<div>
	<h4 class="c-heading-style-1 u-font-style-1">This is font-style-1</h4>
	<p class="u-font-style-1">This is still font-style-1. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
	<button class="c-button u-font-style-1">font-style-1</button>
</div>
<div>
	<h4 class="c-heading-style-1 u-font-style-2">This is font-style-2</h4>
	<p class="u-font-style-2">This is still font-style-2. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
	<button class="c-button u-font-style-2">font-style-2</button>
</div>
<div>
	<h4 class="c-heading-style-1 u-font-style-3">This is font-style-3</h4>
	<p class="u-font-style-3">This is still font-style-3. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
	<button class="c-button u-font-style-3">font-style-3</button>
</div>
```

### Heading Styles

Heading-styles are focused primarily on the font-size, line-height, margin, padding, etc. dimensional properties.

!!! warning
	For Heading Styles, avoid using stylistic css-properties that are reserved for the [Font Styles](#font-styles).

```html
<h1 class="c-heading-style-1">Heading One <span class="c-heading--subheading">Subheading</span></h1>
<h2 class="c-heading-style-2">Heading Two <span class="c-heading--subheading">Subheading</span></h2>
<h3 class="c-heading-style-3">Heading Three <span class="c-heading--subheading">Subheading</span></h3>
<h4 class="c-heading-style-4">Heading Four <span class="c-heading--subheading">Subheading</span></h4>
<h5 class="c-heading-style-5">Heading Five</h5>
<h6 class="c-heading-style-6">Heading Siz</h6>
```

## Utilities

### Body Copy

```html
<p class="c-text-body">Officia occaecat nisi sit ut cupidatat officia consectetur consequat ipsum ea aliquip consequat. Consectetur sint officia veniam aliquip sit enim qui dolor qui. Irure enim aliqua nulla cillum exercitation commodo labore velit non est officia labore veniam laborum. Dolore labore dolore adipisicing sint reprehenderit culpa ex non Lorem reprehenderit id cillum ex ea. Dolore enim minim proident eu non. Eu deserunt amet esse non in fugiat esse elit consectetur cillum aute pariatur. Et velit cupidatat exercitation elit aliquip voluptate eiusmod cupidatat eiusmod.</p>
```

### Font Families

A primary & secondary fonts are defined in the `_variables.scss` file in the `$fonts` map, and a set of utility classes will output classes to change the `font-family` to their values.

```html
<p class="u-font-primary">I will be styled with the primary font-family.</p>
<p class="u-font-secondary">I will be styled with the secondary font-family.</p>
```

### Text-Alignment

A primary & secondary fonts is defined in the `_variables.scss` file and a set of utility classes will output classes to change the `font-family`.

```html
<p class="u-text-left">I will be aligned left</p>
<p class="u-text-center">I will be aligned center</p>
<p class="u-text-right">I will be aligned right</p>
<p class="u-text-justify">I will be aligned justify</p>
```

#### Responsive Text-Alignment

Additionally, there are break-point specific alignment utilities you can use to change the alignment direction depending on the active breakpoint.

Classes will be made with the `.u-text-[alignment]--[breakpoint]` pattern for the following combinations of

- `alignment`: left, center, right, justify
- `breakpoint`: s, m, l, xl, xxl
	- _(from the `$breakpoint_shortcodes` in `_variables.scss`)_

**For Example:**

```html
<p class="u-text-left--s u-text-center--m u-text-right--l u-text-justify--xl">I will change my alignment depending on the width of the screen</p>
```
