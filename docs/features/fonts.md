# Fonts

## Summary
A project called typefaces is a project that has packaged up Open Source typefaces which allows developers to download and import fonts such as Google Fonts. For more information please visit the project's homepage https://github.com/KyleAMathews/typefaces

More information on font loading strategies https://www.zachleat.com/web/comprehensive-webfonts/

## Installation and Usage
The author of this package is prefixing the packages with typeface-\<FontFamily\>. For example, If I wanted to install Google's "Open Sans" locally. I would visit https://www.npmjs.com/ and search for "typeface-open-sans". Find the package and use npm to install the package as you normally would. Follow the steps below to use the package.

### Steps
1. Install your desired font. In this example we are downloading Google's Open Sans
    ```bash
    # One Font
    $ npm install --save typeface-open-sans 

    # Two Fonts
    $ npm install --save typeface-roboto typeface-source-sans-pro
    ```
1. Each package can either be imported in a JS or SCSS file. Depending on where you would like to import them would be up to the developer. Within Genesis visit the `/src/pages` directory. You can either import them globally or import them per page.

    Javascript:
    ```js
    // Globally
    // /src/pages/Global.js
    import "typeface-open-sans";
    ```

    SCSS/CSS
    ```css
    /* Globally */
    /* src/pages/Global.scss */
    @import "~typeface-open-sans/index";
    ```

    Javascript:
    ```js
    // Per Page
    // /src/pages/PROD/PROD.js
    import "typeface-source-sans-pro";
    ```

    SCSS/CSS
    ```css
    /* Per Page */
    /* /src/pages/PROD/PROD.scss */
    @import "~typeface-source-sans-pro/index";
    ```
    __NOTE:__ The `~` here means that rather than trying to locate a file relative to our current file's location, locate a file relative to our project's `node_modules` directory instead. For more information https://chriscourses.com/blog/loading-fonts-webpack

    __NOTE:__ Importing the packages in Javascript will output the `@font-face` rules in the `/public/dist/main.css` file. However, importing the package in SCSS/CSS will create a new stylehsheet in `public/dist/` directory and will be initiated by webpack.
1. Within your SCSS/CSS stylesheet you can declare the font family as you would normally.
    ```css
    body {
      font-family: "Open Sans"
    }
    ```
