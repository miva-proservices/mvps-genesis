# htaccess

There are a couple different htaccess files used throughout the framework:

1. `generator-quickstart\app\templates\miva-server\%WEB_ROOT%\.htaccess`
2. `generator-quickstart\app\templates\miva-server\%WEB_ROOT%\%MIVA_ROOT%\graphics\%STORE_ID%\.htaccess`
3. `generator-quickstart\app\templates\miva-server\%WEB_ROOT%\%MIVA_ROOT%\themes\%STORE_ID%\genesis\public\.htaccess`

## `%WEB_ROOT%\.htaccess`

Here is a breakdown of the contents of the `generator-quickstart\app\templates\miva-server\%WEB_ROOT%\.htaccess` file.

### Dev Store Access Protection

The following sections of the `.htaccess` file are helpful during the site-build phase of a project. It allows you to restrict access to a store based off of username:password proection with Basic-Auth *or* allow access to specific IP-addresses.

You will need to:

- [Create a `.htpasswd`](https://hostingcanada.org/htpasswd-generator/) and upload it to the server outside of the web-root (ex. `/access/`)
- Update the `AuthUserFile` path to point to where you uploaded it
- Update the list of "Miva IPs"
- Update the "Server Requests To Self" to match your store's server so it can work with `mvt:call`s
- "Client IPs" & "3rd Party IPs" can be added/removed as necessary

To disable this functionality, you would just comment/remove all of the blocks listed below.

```apache
#-------------------------------------------------------------------------------
# Force Normal Development Store Password Protection If Not On IP Allow List
#-------------------------------------------------------------------------------

AuthType Basic
AuthName "Restricted Access"
AuthUserFile /var/www/vhosts/example.com/access/.htpasswd
require valid-user
order deny,allow
deny from all

#-------------------------------------------------------------------------------
# Miva IPs
#-------------------------------------------------------------------------------

allow from 209.132.3.11
allow from 207.114.171.26
allow from 100.3.92.237
allow from 10.7.12.0/22
allow from 10.7.16.0/22
allow from 10.7.20.0/22
allow from 2607:4d00::/32

# Server Requests To Self
allow from XX.XX.XXX.XXX

#-------------------------------------------------------------------------------
# 3rd Party IPs
#-------------------------------------------------------------------------------

# 3rd Party Developer
allow from XX.XX.XXX.XXX

#-------------------------------------------------------------------------------
# Client IPs
#-------------------------------------------------------------------------------

allow from XX.XXX.XX.XXX

#-------------------------------------------------------------------------------
# Satisfy Any Request (IP acceptance list or basic authentication)
#-------------------------------------------------------------------------------

satisfy any

#-------------------------------------------------------------------------------
# Deny Access to Hidden Files
#-------------------------------------------------------------------------------

# RedirectMatch 403 /\.(.*)
```

### DirectoryIndex

This is set to allow index.html & index.php files to load when uploaded to their own directory (ex. wordpress, mkdocs, etc.)

```apache
#-------------------------------------------------------------------------------
# Directory Index
#-------------------------------------------------------------------------------

DirectoryIndex index.html index.php %MIVA_ROOT%/merchant.mvc
```

### Redirect to Consistent/Default Domain & Protocol

The following rules are used to ensure that visitors are always redirected to the client's chosen link structure. Typically, this will be forcing-HTTPS and forcing-www, but that may change if the store is on a subdomain too, so you might need to remove/update the `RewriteCond %{HTTP_HOST} !^www\. [NC]` line and it's subsequent `RewriteRule`.

```apache
#-------------------------------------------------------------------------------
# Enable Rewrite Engine
#-------------------------------------------------------------------------------

RewriteEngine On

#-------------------------------------------------------------------------------
# Force HTTPS
#-------------------------------------------------------------------------------

RewriteCond %{SERVER_PORT} 80
RewriteRule ^(.*)$ https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]

#-------------------------------------------------------------------------------
# Force WWW
#-------------------------------------------------------------------------------

RewriteCond %{HTTP_HOST} !^$
RewriteCond %{HTTP_HOST} !^www\. [NC]
RewriteCond %{HTTPS}s ^on(s)|
RewriteRule ^ https://www.%{HTTP_HOST}%{REQUEST_URI} [R=301,L]
```

### Miva's URI Management

This is a modified version of Miva's default URI management. Typically, it just includes the `RewriteCond %{REQUEST_FILENAME} !-s`, but that has been commented out and the `!-f` and `!-d` lines are added so that the `DirectoryIndex` can kick-in for directories that contain a `index.html` & `index.php`.

```apache
#-------------------------------------------------------------------------------
# Miva URI Management
#-------------------------------------------------------------------------------

### Begin - Miva Merchant URI Management: Direct all non-file URIs to Miva Merchant
RewriteEngine on
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
# RewriteCond %{REQUEST_FILENAME} !-s
RewriteCond %{REQUEST_URI} !^/%MIVA_ROOT%/.*$
RewriteRule ^(.*)$ /%MIVA_ROOT%/uri.mvc? [QSA,L]
### End - Miva Merchant URI Management
```

### Cache Control

Some basic image & font caching is added to the root directory. CSS & JS are intentionally left out of the root htaccess because it can create issues with the Miva admin. Therefore, we have added seperate htaccess files within the graphics & theme directories.

```apache
#-------------------------------------------------------------------------------
# Cache Control (CloudFlare)
#-------------------------------------------------------------------------------

# Fonts: 1 YEAR
<FilesMatch "(?i)^.*\.(ico|pdf|flv|eot|otf|svg|ttf|woff)$">
	Header set Cache-Control "max-age=29030400, public, no-transform"
</FilesMatch>

# Images: 1 MONTH
<FilesMatch "(?i)^.*\.(jpg|jpeg|png|gif|swf)$">
	Header set Cache-Control "max-age=15552000, public, no-transform"
</FilesMatch>
```

### Old Platform Links - 301 Redirects

This is just placeholder showing an ideal place to put the store's 301-redirects.

## graphics & theme `.htacess`

The rules within the graphics & theme htaccess files are pretty much identical and are just a subset of the most relevant performance & best-practices found within the [HTML5 Boilerplate .htaccess](https://github.com/h5bp/html5-boilerplate/blob/master/dist/.htaccess); which is pretty well commented with descriptions and links as it's own set of documentation.

- `generator-quickstart\app\templates\miva-server\%WEB_ROOT%\%MIVA_ROOT%\graphics\%STORE_ID%\.htaccess`
- `generator-quickstart\app\templates\miva-server\%WEB_ROOT%\%MIVA_ROOT%\themes\%STORE_ID%\genesis\public\.htaccess`
