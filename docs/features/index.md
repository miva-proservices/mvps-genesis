# Features

This section goes over the various features of a Miva Store built with the Genesis framework.

## Store Owner Features

### Overall/Essential Items

* Essential ecommerce features & best-practices built-in:
	* Responsive mobile-first website
	* We output key schema, microdata, & open-graph mark-up to help Search Engines crawl your site and display rich-SERPs
	* Basic GA & GTM configuration to track page-views & some enhanced-ecommerce data
	* Cloudflare CDN can be configured for improved page-load time
	* Optimized for Google's PageSpeed guidelines with optimized asset delivery (caching, expires-headers, minification, concatenation, code-splitting, and more.)
* Training & consultation on how to:
	* Best leverage Miva's built-in functionality
	* Architect and setup your product-catalog
	* Create & manage your content for About Us / Contact Us / FAQs / Privacy Policy  pages
	* Setup your tax, payment, shipping, and other essential-ecommerce functionality
* Fonts, colors, & style of the site are easily configured to reflect & promote your brand

### Global Header/Footer Features

* Promotional text-banners & links that are easily editable
* Horizontal navigation with drop-down links
* Search bar with product-result autocomplete
* Sticky-header on desktop-devices that keeps the search & cart visible after scrolling down the page
* Cart CTA & cart-count displays, and when it is clicked it reveals the mini-basket
* Mini-basket shows a summary of the customer's basket-items, charges, & totals with CTAs to drive the customer into the checkout process
* Social-media icons & links to the client's accounts
* Form to capture email-newsletter subscriber signups

### Home Page

Storefront & static pages can use Miva's Components & Layout module to customize & display the following components:

* Hero slide & slider
* Promotional text & background images
* Show a carousel of featured products from a category
* Display promotional images, text, & links in a grid, 3-up grid, & 2-up grid
* Highlight key selling-points, trust-builders, and store information

### Category/Search & Product Listings

* Display Subcategories with their images, names, & links
* Display products-listing with options to for shoppers to filter & find products by:
	* Facets based on prices, product-attributes, and product-custom fields
	* Sorting parameters & direction
	* Pagination & items-per-page options

### Product Page

* Display detailed information about each product:
	* Main image with thumbnails of alternative images
	* Customers click to view larger/detailed images
	* Product attributes, inventory levels, discounts, pricing, & description
	* Display a list of product-details/custom fields; like: a short-description, spec fields, FAQs html-block, etc.
	* and more!
* AJAX add a product to your basket reveals a mini-basket and prompts customers to checkout
* Display related products (products the client has indicated are related to the current product)
* Add a products to your wishlist
* Allow customers to share products to various social-media platforms (Facebook, Twitter, Pinterest, etc.)

### Checkout 

* Logged in customers can build & choose their address-book to complete their checkout quicker
* Leverage any of Miva's built-in payment modules (MivaPay, Braintree, Authorize.net, PayPal, etc.)
* Leverage any of Miva's built-in shipping modules (USPS, UPS, Fedex, etc.)
* Basket page provides a continue-shopping link to direct customer back to the last categry or product page they viewed
* Customers can estimate shipping from the Basket page to preview their costs before entering all their information
* Checkout process will prompts for customer to create or log into their account

### Account Pages

* Account dashboard page displays a key information about your account
	* Based on the [Colossus ReadyTheme](https://apps.miva.com/colossus-readytheme.html)
	* Recent orders
	* Account Credit Balance
	* Primary Payment Method
	* Primary Shipping & Billing Address from Address-Book
	* Active Subscriptions
	* Your wishlists
	* Helpful store contact-details
* Order history page lists the your most recent orders first.

## Developer

This includes the extensions, npm-packages, Miva-pages, Miva-page-layouts, ReadyTheme Content Sections, CSS utilities, SASS setup, etc.

- CSS Components & Utilities
	- [Buttons](css/buttons.md)
	- [Control Groups](css/control-groups.md)
	- [Grids & Layouts](css/grids-and-layouts.md)
	- [Lists & Navs](css/lists-and-navs.md)
	- [Tables](css/tables.md)
	- [Typography](css/typography.md)
	- [Misc. Items](css/misc.md)
- Miva Store
    - [Customer Fields](miva-store/customer-fields.md)
	- [Page Layouts](miva-store/page-layouts.md)
	- [Theme Overrides](miva-store/theme-overrides.md)
	- [UI Library](miva-store/ui-library/index.md)
- Theme Extensions
	- [AJAX Add to Cart](theme-extensions/ajax-add-to-cart.md)
	- [Breadcrumbs](theme-extensions/breadcrumbs.md)
	- [Carousel](theme-extensions/carousel.md)
	- [Dynamic Form Element Attributes](theme-extensions/dynamic-form-element-attributes.md)
	- [Environment Helpers](theme-extensions/environment-helpers.md)
	- [Form Validation](theme-extensions/form-validation.md)
	- [Messages](theme-extensions/messages.md)
	- [Mini Basket](theme-extensions/mini-basket.md)
	- [Shipping Estimator](theme-extensions/shipping-estimator.md)
	- [Tabs](theme-extensions/tabs.md)
- Optional Extensions
	- [Also Bought](optional-extensions/also-bought.md)
	- [Instagram](optional-extensions/instagram.md)
	- [Share Product](optional-extensions/share-product.md)
	- [Show Related](optional-extensions/show-related.md)
- NPM packages
	- [Bootstrap](packages/bootstrap.md)
	- [Debounced](packages/debounced.md)
	- [Decode Entities](packages/decode-entities.md)
	- [Event Bus](packages/event-bus.md)
	- [Miva Array Deserialize](packages/miva-array-deserialize.md)
	- [Parameter Validator](packages/parameter-validator.md)
	- [string2regex](packages/string2regex.md)
	- [UID](packages/uid.md)
	- [Wait For Expression](packages/wait-for-expression.md)
- [PNPM Scripts](pnpm-scripts.md)
<!-- TODO: Document & add more features -->
