# Components & Layouts > Forms

## Summary

Several C&L components have been built that allow store-owners to create & modify forms on their site. They are geared towards collecting information provided by customers, like for: contact forms, catalog request forms, dealer-applications, feedback surveys, RMA request forms, etc.

The forms can be added to any of the pages that use the Components & Layouts `layout_load_components` feature which is typically setup on the SFNT, CTUS, ABUS, PRPO, & SARP pages. See the [Sample Setup](#sample-setup) below for more technical details about how to add this to a store.

Customers can create forms that take optional or required inputs: text, textarea, radio-buttons, checkboxes, and dropdowns. They also include an option simple-math-problem for filtering out bot/spam submissions. It does not include file/image uploads.

## Example

[Click here to try out a demo form](https://genesis.mivamerchantdev.com/page/contact-us.html)

[![Example Contact Form](../../../images/pattern-library/organisms/contact-form.png)](https://genesis.mivamerchantdev.com/page/contact-us.html)

Once the form is filled out, it will send an email to the whomever you choose that outputs the information the customer provided:

![Example Contact Form Email](../../../images/pattern-library/organisms/contact-form-email.png)

## Components

## Form Component (`cl_form`)

When creating a custom form, start by creating the required/wrapping `cl_form` component. It needs to be the direct-parent of the `cl_form_input` & `cl_form_choice` components.

### Form Component Attributes

#### Name

A name is required for all C&L components and it will be used as the title/name/heading used to identify and summarize the purpose of your form. In the [example form above](#example) it is set to "Contact Form".

#### Description

This is where you can write additional details about the form to help the customer fill out the form. It will display below the title. In the [example form above](#example) it is set to "This is the form description. Lorem ipsum...".

* **Required:** No
* **Type:** Text Input
* **Code:** title

#### Submit Button Text

This is going to be the text of the form's final submit button. In the [example form above](#example) it is set to "Send Message"

* **Required:** Yes
* **Type:** Text Input
* **Code:** submit_button_text

#### Email To

Enter the email address(es) that you would like to send the details of the filled-out form to (ex. john.doe@company.com)

* **Required:** Yes
* **Type:** Text Input
* **Code:** email_to

#### Email CC

Optionally, enter the email address(es) that you would like to send the details of the form to as a CC recipient (ex. jane.doe@company.com)

* **Required:** No
* **Type:** Text Input
* **Code:** email_cc

#### Email Subject

Enter the subject line that you would like the email to have when the form is submitted.

* **Required:** Yes
* **Type:** Text Input
* **Code:** email_subject

#### Success Message

This is the message that will display to the customer after they have successfully fill-out and submitted the form.

Typically this is a great place to thank them for their information & clarify when you intend to follow up with them.

* **Required:** Yes
* **Type:** Text Input
* **Code:** success_message

#### Validate Math Problem?

Check this box if you would like to display a simple math problem to the customer to prove that they are human. This is helpful for filtering out spam/bot submissions.

The math problem will generate two random numbers less-than 10, and ask customers to enter the sum of the two numbers (ex. "What is 3 + 4?" Expected answer: 7)

* **Required:** No
* **Type:** Checkbox
* **Code:** validate_math_problem

## Form Input Component (`cl_form_input`)

This component is used to gather custom textual information from a customer. It could be their name, email, a message, etc.

In order for this to work, it must be placed inside of a cl_form component as a direct-child-component of the cl_form component.

### Form Input Component Attributes

#### Name

A name is required for all C&L components and it will be used as the label/prompt of the input

#### Type

Here is where you can select the input-type for the information you would like people to enter.

* **Required:** Yes
* **Type:** Select
* **Code:** type
* **Options:**
	* Text
	* Textarea (i.e. message)
	* Number
	* Email
	* Password
	* Tel (i.e. Phone number)
	* Datetime-Local (i.e. Date)

#### Description

Optionally, you can add additional text information right below the Name/Label (and above the input). This can be used to describe more details about the input's expectations & provide instructions to customers too.

* **Required:** No
* **Type:** Textarea
* **Code:** description

#### Placeholder

Optionally, you can add provide some placeholder text to the inputs. These are helpful for showing some example/lead-in text to help guide the customer in entering correct information.

For example, in a name input, you might enter "John Smith" to subtly show the customer you would like their full-name.

* **Required:** No
* **Type:** Text
* **Code:** placeholder

#### Is Required?

Check this box if you would like to require that the customer fills out this field in order to submit/send the form.

This will enable browser validation on the front-end to ensure people enter proper information according ot the "type" you selected for the input (e. you need to enter a valid email address). Also, on the backend, it will ensure that any value has been entered.

* **Required:** No
* **Type:** Text
* **Code:** placeholder

## Form Choice (`cl_form_choice`)

This component is used to gather the choices that customers make from a pre-determined list of options. It can display a select-dropdown list, radio buttons, or checkboxes depending on how the `display` and `allow_multiple` options are configured.

In order for this to work, it must be placed inside of a cl_form component as a direct-child-component of the cl_form component.

### Form Choice Component Attributes

#### Name

A name is required for all C&L components and it will be used as the label/prompt of the input

#### Options

This is where you list out the options that you would like the customer to choose from. Each line represents one option/selection that people can make.

For example, to create three options to gather people's interests you would list each option on their own line like this:

```
Technology
News & Politics
Entertainment
```

* **Required:** Yes
* **Type:** Textarea
* **Code:** options

#### Display

This changes the display of the Options that you present to the customer. You can pick between:

* `dropdown` - Displays the options in a `<select>` dropdown menu.
* `inline` - Displays radio or checkbox options in an horizonal/inline list.
* `block` - Displays radio or checkbox options in a vertical/block list.

Combine this Display attribute with the [Allow Multiple?](#allow-multiple) option to create the display & responses that you want.

* **Required:** Yes
* **Type:** Textarea
* **Code:** options

#### Allow Multiple?

Optionally, you can allow the customer to select more-than-one of the options you have provided.

* When used in combination with the "Display" of `dropdown`, this will render a `<select multiple>` input.
* When used in combination with the "Display" of `block` or `inline`, this will render several radio inputs (i.e. `<input type="radio">`)

| | Allow Multiple?: Yes | Allow Multiple?: No |
| --- | --- | --- |
| **Display: dropdown** | `<select multiple>` | `<select>` |
| **Display: inline** | `<input type="checkbox">` | `<input type="radio">` |
| **Display: block** | `<input type="checkbox">` | `<input type="radio">` |

* **Required:** No
* **Type:** Checkbox
* **Code:** allow_multiple

#### Description

Optionally, you can add additional text information right below the Name/Label (and above the input). This can be used to describe more details about the input's expectations & provide instructions to customers too.

* **Required:** No
* **Type:** Textarea
* **Code:** description

#### Is Required?

Check this box if you would like to require that the customer fills out this field in order to submit/send the form.

This will enable browser validation on the front-end to ensure people enter proper information according ot the "type" you selected for the input (e. you need to enter a valid email address). Also, on the backend, it will ensure that any value has been entered.

* **Required:** No
* **Type:** Text
* **Code:** placeholder

## Sample Setup

### Import the Components

1. Download the [`cl-form__components.xml`](imports/cl-form__components.xml) import file
2. Open the Miva Admin & navigate to Data Management
3. Choose the "Import Data and Settings in XML Provisioning Format" option
4. Upload and trigger the import for the [`cl-form__components.xml`](imports/cl-form__components.xml) import file.

### Import/Create the Layout

1. You can use an existing layout & manually add the form components to it, or you can import this sample CTUS form:
2. Download the [`cl-form__example-layout.xml`](imports/cl-form__example-layout.xml) import file
3. Open the Miva Admin & navigate to Data Management
4. Choose the "Import Data and Settings in XML Provisioning Format" option
5. Upload and trigger the import for the [`cl-form__example-layout.xml`](imports/cl-form__example-layout.xml) import file.

### Sample Page Content

!!! note
	This step may be optional if your CTUS page is already configured to use the C&L page-layout that recursively renders components.

Now that the store has the `cl_form` components & your have a layout that is confiugred to use those components, you can render the form wherever you want it to display using the `layout_code` you added the form to and the `layout_load_components` RT Content Section.

```xml
<mvt:assign name="l.settings:layout_code" value="'ctus'" />
<mvt:item name="readytheme" param="contentsection( 'layout_load_components' )" />
```
