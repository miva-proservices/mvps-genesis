# Generic Currency Formatting

The Genesis store makes use of the "Generic Currency Formatting" module to display negative values in a more user-friendly & recognized way: "-$9.99" (as opposed to the "US Currency Formatting" modules negative price display: "($9.99)").

By making this change, it allowed us to remove some code-complexity on certain pages and make a more consistent & easy price display throughout the store.

The Store's currency module can be changed under "Store > Settings > Currency Formatting", and the settings for the "Generic Currency Formatting" are configured as follows:

<img src="../../../images/generic-currency-settings.png" alt="Generic Currency Settings" width="350" />
