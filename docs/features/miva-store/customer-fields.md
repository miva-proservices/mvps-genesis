# Customer Fields

The characteristics of the customer fields displayed through out the framework's front end are controlled by the **Customer Fields** settings in the Miva admin.

```
Home > Edit Store: Genesis > Customer Fields
```

## Customer Fields Settings

Below is a breakdown of the customer field settings and how they regulate the customer fields on the front end of the site.

### General Settings

**Shipping Information**

| Option | Description |
| --- | --- |
| Hidden | Shipping information fields will not be displayed. |
| Optional | All shipping information fields will be displayed as optional fields. |
| Use Settings Below | Individual shipping information fields will be displayed according to its corresponding **Field** setting. |

**Billing Information**

| Option | Description |
| --- | --- |
| Hidden | Billing information fields will not be displayed. |
| Optional | All billing information fields will be displayed as optional fields. |
| Use Settings Below | Individual billing information fields will be displayed according to its corresponding **Field** setting. |

**Primary Address**

| Option | Description |
| --- | --- |
| Shipping | Set the shipping information fields as the primary address fields. Shipping information fields will be displayed above the billing information fields if the billing information fields are not set as Hidden. Billing information fields will have the checkbox option to set the billing information fields to be the same as shipping the shipping fields. |
| Billing | Set the billing information fields as the primary address fields. Billing information fields will be displayed above the shipping information fields if the shipping information fields are not set as Hidden. Shipping information fields will have the checkbox option to set the shipping information fields to be the same as shipping the billing fields. |

**Residential Delivery**

This option is not currently utilized in the customer field templates for front end display.

### Field Settings

The individual field settings will control the display for each field under Shipping Information and/or Billing Information sections when the Use Settings Below option is set, as well as when adding or updating a customer address book entry (CADA / CADE). 

| Option | Description |
| --- | --- |
| Required | Field is displayed and the input element's label has "required" input CSS styling. A `is-required` class is added the input label and `required` attribute is added to the input element. |
| Optional | Field is display and the input element's label has "optional" input CSS styling. |
| Hidden | The field is not displayed. |

**Note:** The Fax Number field is not displayed by default on any of the customer fields. To enable the display, you will need to uncomment the code to display the Fax Number in the corresponding template.

## Customer Fields Templates

This section is a quick reference guide for the templates that utilize the customer field settings across the framework.

**Checkout: Customer Information (OCST)**

* Customer Fields template

**ReadyTheme > Content Sections: customer_account_fields**

* This is a common template for the customer fields on customer account pages.
    - ACAD: Customer Fields template
    - ACED: Customer Fields template

**ReadyTheme > Content Sections: customer_address_fields**

* This is a common template for the customer fields on customer address book pages.
    - CADA: Customer Address Fields template
    - CADE: Customer Address Fields template