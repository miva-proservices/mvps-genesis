# [LogRocket](https://logrocket.com/)

## Summary

> [LogRocket](https://logrocket.com/) lets you replay what users do on your site, helping you reproduce bugs and fix issues faster.

Tracking script that allows you to track 1,000 sessions per month. Session tracking includes key information for identifying and resolving bugs:

- Session replay
- Network activity
- Console and errors

### Motivation for adding:

- Quickly understand and fix JavaScript errors
- Support users without back-and-forth
- Free for up to 1,000
- Can be disabled by default and enabled for client testing or tricky customer bugs
- Learn where users are getting stuck (ex. Identifies "Rage Clicks" from users)

## Installation

1. Create a `logrocket-library` JavaScript Resource
	- Type: `External File`
	- File Path: `https://cdn.lr-ingest.io/LogRocket.min.js`
	- Add Attribute: `crossorigin="anonymous"`
2. Create a `logrocket-init` JavaScript Resource
	- Type: `Inline Script`
	- Source: `logrocket-init.mvt`
3. Optionally, change the `init()` parameter from `miva/genesis` to the customer's LogRocket account.
4. Optionally, change the `identify()` parameter to work better for the customer
5. Optionally, add `data-private` to additional non-default DOM elements that should not be recorded.
	- *Note: This has already been done for OPAY, CPCA, & CPCE*
6. Assign both JS Resources to the `head_tag` Resource Group
