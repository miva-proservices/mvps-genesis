# Page Layouts

We noticed many of the page had duplicate logic or layouts, so we moved the existing logic & layouts to global parts of the store so they can be re-used across many pages.

## Layouts

There are about five different layouts used throughout the store:

### Layout: Single Column

- **Content Section Code:** `layout_single_column`
- **Description:** This is a single-column layout that is primarily empty for feature-rich page-specific content.
- **Used On:** ABUS, ACAD, ACRT, AFAD, AFCL, AFED, BASK, BSKE, CSTR, FPWD, GFTL, INVC, LOGN, MNTN, NTFD, OMIN, OPRC, ORDL, ORDS, ORHL, OUS1, OUSM, PATTERN_LIBRARY, PLMT, POUT, PROD, SFNT, SMAP, UATM, UATR, WLGN

### Layout: Product Listing

- **Content Section Code:** `layout_product_listing`
- **Description:** This is a two-column layout that is used on most product-listing pages. The main-right column is used for page-specific product-listing grids, and the secondary left column is used to render the facets or the `subcategory_tree` RT CS.
- **Used On:** CTLG, CTGY, PLST, SRCH

### Layout: Static Links

- **Content Section Code:** `layout_static_navigation`
- **Description:** This is a two-column layout that is used on the main static-content pages that come with Miva. The main right column is used for page-specific content, and the secondary left column renders the `navigationset( 'static_navigation' )`
- **Used On:** CTUS, FAQS, PRPO, SARP

### Layout: Account Navigation

- **Content Section Code:** `layout_account_navigation`
- **Description:** This is a two-column layout that is used on most-all of the account pages. The main right column is for page-specific content, and the secondary left column renders the `navigationset( 'account_navigation' )`
- **Used On:** ABAL, ACED, ACLN, CABK, CADA, CADE, CEML, CPCA, CPCD, CPCE, CPWD, CSBE, CSUB, ORDH, RGFT, WISH, WLAD, WLED, WLST, WPCK

### Layout: Checkout

- **Code:** `layout_checkout`
- **Description:** This is a two-column wide layout that is used throughout the checkout. The main-left column is used for the page-specific content, and the secondary right column is used to render the `<mvt:item name="basket" />` which holds the cart-summary.
- **Used On:** OCST, OSEL, OPAY

## Variables

There was a lot of logic within the Global Header & Footer that would have page-code-specific logic to alter how the header/footer rendered. We've removed the page-specific logic and refactored it to reference a set of `l.settings:ui` variables instead.

Set any of these variables at the very top of a page, and it will alter how the Global Header/Footer output items within their templates

### `page_layout:minimal:enabled`

```xml
<mvt:assign name="l.settings:ui:page_layout:minimal:enabled" value="1" />
```

- **Description:** Set this variable to a truthy value at the top of a page and it will render the minimal global header/footer that typically used to show in the checkout-process or reset-password flows.
- **Possible Values:** `1`
- **Used On:** ORDL, OCST, UATM, UATR, OUS1, OUSM, OSEL, & OPAY

### `page_layout:minimal:classes:hidden`

Output this variable in an HTML class attribute on any element in the Global Header & Footer that you would like to **hide** when [`page_layout:minimal:enabled`](#page_layoutminimalenabled) is enabled. This variable is automatically set within the HTML Profile.

*For example:*

```html
<div class="o-layout u-bg-style--primary t-site-header__top-navigation &mvte:ui:page_layout:minimal:classes:hidden;">
	<!-- DO NOT SHOW me in the minimal layout -->
</div>
```

### `page_layout:minimal:classes:shown`

Output this variable in an HTML class attribute (with the `u-hidden`) on any element in the Global Header & Footer that you would like to **show** when [`page_layout:minimal:enabled`](#page_layoutminimalenabled) is enabled. This variable is automatically set within the HTML Profile.

*For example:*

```html
<div class="u-hidden &mvte:ui:page_layout:minimal:classes:shown;">
	<!-- SHOW me in the minimal layout -->
</div>
```

### `global_header:omit_page_title`

```xml
<mvt:assign name="l.settings:ui:global_header:omit_page_title" value="1" />
```

- **Description:** Set this variable to a truthy value at the top of a page and it not output the Global Header's `<h1>&mvt:page:name;</h1>` and keyline.
- **Possible Values:** `1`
- **Used On:** ABUS, ACLN, BASK, CSBE, CTUS, FAQS, FPWD, LOGN, PROD, PRPO, SARP, SFNT

### `global_header:omit_breadcrumbs`

```xml
<mvt:assign name="l.settings:ui:global_header:omit_breadcrumbs" value="1" />
```

- **Description:** Set this variable to a truthy value at the top of a page and it not output the Global Header's `<mvt:item name="breadcrumbs" />`.
- **Possible Values:** `1`
- **Used On:** It is not currently used on any pages, but it is available for you to use.

## Changes

[Click here to view the pull-request that outlines all of the changes](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis-framework/pull-requests/31/overview) that were made to simplify the page-layouts & global header/footer logic.

### Changes: ABUS Before/After

Here's an example from the `ABUS` page that shows how page layouts looked and where there code have been moved to:

#### Changes: ABUS Before

```html
<mvt:item name="html_profile" />
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<base href="&mvt:global:basehref;">
	<mvt:if expr="NOT ISNULL l.settings:page:title">
		<title>&mvt:page:title;</title>
	<mvt:else>
		<title>&mvt:store:name;: &mvt:page:name;</title>
	</mvt:if>
	<mvt:item name="head" param="head_tag" />
</head>
<body id="js-&mvte:page:code;" class="o-site-wrapper t-page-&mvt:global:pageClass;">
	<mvt:item name="hdft" param="global_header" />

	<section class="o-layout o-layout--column-reverse o-layout--row-reverse--l">
		<div class="o-layout__item u-width-12 u-width-9--l u-width-10--w">
			<section class="o-layout">
				<div class="o-layout__item">
					<h1 class="c-heading-delta u-text-bold u-text-uppercase">&mvte:page:name;</h1>
					<hr class="c-keyline">
				</div>
			</section>

			<section class="o-layout">
				<div class="o-layout__item">
					<mvt:item name="hdft" param="header" />
				</div>
			</section>

			<section class="o-layout">
				<div class="o-layout__item">
					<mvt:item name="readytheme" param="contentsection( 'abus_content' )" />
				</div>
			</section>

			<section class="o-layout">
				<div class="o-layout__item">
					<mvt:item name="hdft" param="footer" />
				</div>
			</section>
		</div>
		<aside class="o-layout__item u-width-12 u-width-3--l u-width-2--w">
			<mvt:item name="readytheme" param="navigationset( 'static_navigation' )" />
		</aside>
	</section>

	<mvt:item name="hdft" param="global_footer" />
</body>
</html>
```

#### Changes: ABUS After

```html
<mvt:assign name="l.settings:ui:global_header:omit_page_title" value="1" />

<mvt:item name="html_profile" />
<head>
	<mvt:item name="head" param="head_tag" />
</head>
<body id="js-&mvte:page:code;">
	<mvt:item name="hdft" param="global_header" />
	<mvt:item name="readytheme" param="contentsection( 'layout_static_navigation' )" />
	<mvt:item name="hdft" param="global_footer" />
</body>
</html>
```

### Changes: Global Header & Footer

See the [variables section above](#variables) for more information on what changed & why.

### Changes: Head Tag

Each page's head content has been moved to the `User Interface > Settings > Head Tag Content`.

For example, this content is now in the global "Head Tag Content" section.

```html
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<base href="&mvt:global:basehref;">
<mvt:if expr="NOT ISNULL l.settings:page:title">
	<title>&mvt:page:title;</title>
<mvt:else>
	<title>&mvt:store:name;: &mvt:page:name;</title>
</mvt:if>
```

### Changes: Page Layout

The page's layout/structure has been moved to one of the ReadyTheme Content Sections.

In this ABUS example, the `layout_static_navigation` RT CS is being used to load a layout similar to what was there:

```html
<section class="o-layout o-layout--column-reverse o-layout--row-reverse--l">
	<div class="o-layout__item u-width-12 u-width-9--l u-width-10--w">
		<section class="o-layout">
			<div class="o-layout__item">
				<h1 class="c-heading-delta u-text-bold u-text-uppercase">&mvte:page:name;</h1>
				<hr class="c-keyline">
			</div>
		</section>

		<section class="o-layout">
			<div class="o-layout__item">
				<mvt:item name="hdft" param="header" />
			</div>
		</section>

		<section class="o-layout">
			<div class="o-layout__item">
				<mvt:item name="readytheme" param="contentsection( 'abus_content' )" />
			</div>
		</section>

		<section class="o-layout">
			<div class="o-layout__item">
				<mvt:item name="hdft" param="footer" />
			</div>
		</section>
	</div>
	<aside class="o-layout__item u-width-12 u-width-3--l u-width-2--w">
		<mvt:item name="readytheme" param="navigationset( 'static_navigation' )" />
	</aside>
</section>
```

### Changes: Page Content

Page specific content has been moved to the Page's > Content template.

```html
<div class="o-layout__item">
	<mvt:item name="readytheme" param="contentsection( 'abus_content' )" />
</div>
```
