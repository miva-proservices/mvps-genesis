# Product Card

## Overview
The product card concept allows us to use a single product card template that can easily be re-used and updated on product listings and carousel sections across the whole site. 

The product card template can be found in the `product_card_iterator` under ReadyTheme -> Content Sections. This template is loaded in on the `html_profile` item on line 51. Page templates will need to have the `readythemes` item assigned to the page to use this feature.

By default, the product card template has been retrofitted on the `product_listing` ReadyTheme Content Section and the _Related Products_ section on the product page template. To use the template in a custom listing, use the following code inside your foreach loop. Make sure that the iterator matches what we're using for the product card.

```xml
	<mvt:comment> Load Product Card </mvt:comment>
	<mvt:do file="g.Module_Feature_TUI_DB" name="l.loaded" value="ManagedTemplate_Load_ID( g.readytheme_templates:product_card_iterator:templ_id, l.loaded_paged )" />
	<mvt:do file="g.Store_Template_Path $ l.loaded_paged:filename" name="l.success" value="Template_Render( l.empty, l.settings )" />
```

Full Example

```xml
	<mvt:do file="g.Module_Feature_TUI_DB" name="l.loaded" value="ManagedTemplate_Load_ID( g.readytheme_templates:product_card_iterator:templ_id, l.loaded_paged )" />
	<mvt:foreach iterator="product" array="product_listing:products">
		<mvt:comment> Load Product Card </mvt:comment>
		<mvt:do file="g.Store_Template_Path $ l.loaded_paged:filename" name="l.success" value="Template_Render( l.empty, l.settings )" />
	</mvt:foreach>
```

## Template

By default, the product card uses the following template:

```xml
<div class="x-product-card o-layout__item">
	<div class="x-product-card__image">
		<span class="x-card-messages">Some Message</span>
		<span class="x-product-card__header x-product-card__header-bottom-has-message">
			<p class="x-flag__primary x-flag__br-none-top-left x-flag__br-none-bottom-right">Primary</p>
			<p class="x-flag__secondary">Secondary</p>
			<p class="x-flag__tertiary x-flag__br-none-top-right x-flag__br-none-bottom-right">Tertiary</p>
		</span>
		<mvt:if expr="NOT ISNULL l.settings:product:imagetypes:main">
			<img src="&mvte:product:imagetypes:main;" alt="&mvte:product:name;">
		<mvt:else>
			<img src="&mvte:image_placeholder;" alt="&mvte:product:name;">
		</mvt:if>
	</div>
	<div class="x-product-card__footer">
		<mvt:comment>
			Uncomment if you need to display brand
			<sup class="x-product-card__brand">Brand</sup>
		</mvt:comment>
		<p class="x-product-card__name">&mvte:product:name;</p>
		<div class="x-product-card__information">
			<p class="x-product-card__price">
				<mvt:if expr="l.settings:product:base_price GT l.settings:product:price">
					<s>&mvt:product:formatted_base_price;</s>
				</mvt:if>
				&mvt:product:formatted_price;
			</p>
			<div class="x-product-card__rating rating-footer-bottom-right">
				<span class="u-icon-star-full"></span>
				<span class="u-icon-star-full"></span>
				<span class="u-icon-star-full"></span>
				<span class="u-icon-star-full"></span>
				<span class="u-icon-star-full"></span>
			</div>
		</div>
	</div>
</div>
```

## Modifiers

The following classes can be used to modify several elements of the product card template.

### Container

You can apply the following optional classes on the main container `x-product-card` 

| __Class__ 						| __Description__                     						 |
|-----------------------------------|------------------------------------------------------------|
| x-product-card__hoverable       	| Adds a hover effect (drop-shadow) on the product card    	 |
| x-product-card__shadow-on 		| Adds a shadow effect (drop-shadow) on the product card     |

### Header

You can apply the following optional classes on `x-product-card__header` 

| __Class__ 									| __Description__                     						 		|
|-----------------------------------------------|-------------------------------------------------------------------|
| x-product-card__header-bottom-has-message 	| Adds an offset if the product card is using the message element  	|


### Flag

#### Border Classes
| __Class__ 						| __Description__                            |
|-----------------------------------|--------------------------------------------------------------------|
| x-flag__br-none      				| Removes the border radius from the product flag    |
| x-flag__br-none-top-left      	| Removes the border radius from the top left corner of the product flag    |
| x-flag__br-none-top-right     	| Removes the border radius from the top right corner of the product flag    |
| x-flag__br-none-bottom-left      	| Removes the border radius from the bottom left corner of the product flag    |
| x-flag__br-none-bottom-right      | Removes the border radius from the bottom right corner of the product flag    |

#### Background Classes
| __Class__ 			| __Description__                            									|
|-----------------------|-------------------------------------------------------------------------------|
| x-flag__error     	| Applies the error color to the background color of the product flag   		|
| x-flag__primary     	| Applies the primary color to the background color of the product flag     	|
| x-flag__secondary   	| Applies the secondary color to the background color of the product flag    	|
| x-flag__tertiary     	| Applies the tertiary color to the background color of the product flag     	|
| x-flag__warning     	| Applies the warning color to the background color of the product flag   		|

### Messages

By default the messages element is set to display none. Adding any of the following classes will set the display to `block`.

| __Class__ 				| __Description__                            									|
|---------------------------|-------------------------------------------------------------------------------|
| x-card-message__error   	| Applies the error color to the background color of the product flag   		|
| x-card-message__info    	| Applies the info color to the background color of the product flag    	 	|
| x-card-message__neutral	| Applies the neutral color to the background color of the product flag   	 	|
| x-card-message__success   | Applies the success color to the background color of the product flag     	|
| x-card-message__warning  	| Applies the warning color to the background color of the product flag   		|


### Review Stars

The following classes can be used to change the placement of the rating container

| __Class__ 				| __Description__                            										|
|---------------------------|-----------------------------------------------------------------------------------|
| rating-footer-bottom-right  | Places the ratings container on the bottom right of the product card's footer 	|
| rating-footer-top-right   | Places the ratings container on the top right of the product card's footer  	 	|
| rating-header-top-right	| Places the ratings container on the top right of the product card's header 	 	|


## Known Issues & Workarounds

* If you display more than 2 flags at a time, the third flag will overflow on mobile. A way around this would be to add a rule for mobile that will change the flex-wrap of the `x-product-card__header` class from `nowrap` to `wrap`.
* Depending on the rules of the parent container, the box shadow modifiers on the product card container may be cut-off. You will need to update the overflow of the parent container accordingly.
