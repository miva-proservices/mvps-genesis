# Product Image Sizes

Throughout the store, there were many inconsistencies with the image-sizes that were used to display thumbnails, main-images, and close-up images in different but similar dimensions. This created an over-abundance of images on the server and resulted in addition network-bandwidth being used as visitors browsed throughout the site.

For example, on the Basket page, the image might have been 130x130, in the checkout process image might have been 140x140, and in the mini-basket it might have been 125x15, so by setting them all to be 140x140 we add a negligible about of size to the images and avoid having the user download 3-different versions of the file.

Therefore, we have consolidated and normalized the image-dimensions that are used to ensure efficient disk-space usage and limit downloads for clients by setting the following image dimension groups.

!!! note
    If you need to change the dimensions of any of the groups, then you should change them on all of the similarly-grouped pages too.

## 100x100px

* Global Mini-basket
* Search Preview Settings
* BASK
* OCST
* OSEL
* OPAY
* INVC
* ORDS
* ACLN > Subscription List
* WISH
* email_abandoned_basket
* email_orderconf_customer
* email_orderconf_merchant

### 360x360px

* SRCH
* CTGY
* PLST
* CSBE
* CSUB
* PROD > Related Products
* ReadyTheme Product Listings

## 730x730px  & 960x960px

* PROD

