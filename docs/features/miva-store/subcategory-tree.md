# Subcategory Tree

A ReadyTheme Content Section that displays the current category's subcategories in a list of links.

It uses `g.category:subcategories` data that is loaded in the HTML Profile

!!! note
	This feature has been de-activated by default, but it can easily be activated by enabling the "ReadyTheme > Content Sections > subcategory_tree" RT CS.

## Screenshots

![Subcategory Tree on Mobile](../../images/subcategory_tree--mobile.png)

![Subcategory Tree on Desktop](../../images/subcategory_tree--desktop.png)
