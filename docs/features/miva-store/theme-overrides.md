# Theme Overrides

We have added two extra/in-active CSS & JS Resources to the Miva Admin that are placeholder-resources to be activated if the client would like to add/update their own JavaScript & CSS. This file setup allows clients to add their own CSS and JS without interfering with our codebase. These files exist on the server but are ignored from the repo.

## Features

- Assets loaded in last, below main.css and main.js respectively.
- Clients can edit directly from the Miva Admin using Edit Source.
- JS is deferred.

### CSS

- **CSS Resource Code:** `theme-overrides-css`
- **Description:** This is a CSS file used to allow clients to make theme and framework overrides.
- **Used On:** Placed in the `head_tag` CSS resource group; loaded after the `main.css` resource.

### JS

- **JS Resource Code:** `theme-overrides-js`
- **Description:** This is a JS file used to make theme and framework overrides.
- **Used On:** Placed in the `global_footer` JS Resource Group; loaded after the `main.js` resource.
