# Accordion

The accordion component allows you to add accordions to any _Components & Layouts_ layout. Uses HTML5's detail and summary syntax to display content in accordion tabs. See this [MDN article](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/details) for more information. While this component is standalone, it works best when nested in an _Container_ component.

## Attributes

|Attribute|Description|Required|
| -------- |:-------------| -------- :|
|Accordion Content| The content to display when the accordion tab expands or is visible. | Yes | 

!!! Note
	The _Component Name_ determines the label of the actual accordion tab.

!!! Attention "About IE11"
	The use of this syntax requires a polyfill for it to work correctly on IE11. This polyfill should already be included as a dependency on _Genesis_ site builds.


## Examples

![](/images/ui-library/ui_accordion.png)

