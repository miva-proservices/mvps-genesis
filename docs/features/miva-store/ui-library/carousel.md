# Carousel

The carousel component allows you to add carousels to any _Components & Layouts_ layout. You can either load in a category's assigned products or display the nested children in a carousel.

## Attributes

|Attribute|Description|Required|
| -------- |:-------------| -------- :|
| Number of Slides (Desktop) | The number of slides to display on desktop devices or screen resolutions. (Defaults to 4) | No |
| Number of Slides (Mobile) | The number of slides to display on mobile devices or screen resolutions. (Defaults to 2.5) | No |
| Number of Slides (Tablet) | The number of slides to display on tablet devices or screen resolutions. (Defaults to 4) | No |
| Show Arrows | Show carousel arrows. | No |
| Show Bullets | Show pagination bullets. | No |
| Mode | Allows you to use a specific mode for the carousel. | No |
| Load Category | Pre-populate the carousel with product listings from the selected category | No |
| Number of Products to Load | The number of products to load from the selected category. (Defaults to 8) | No |
| Space Between Slides | The space between slide elements (Defaults to 16) | No |
| Peek Left (Desktop) | Number of pixels to the left to show. Allows you to peek slides to the left. (Glide.js specific) | No |
| Peek Left (Mobile) | Number of pixels to the left to show on mobile. Allows you to peek slides to the left. (Glide.js specific) | No |
| Peek Left (Tablet) | Number of pixels to the left to show on tablet. Allows you to peek slides to the left. (Glide.js specific) | No |
| Peek Right (Desktop) | Number of pixels to the right to show. Allows you to peek slides to the right. (Glide.js specific) | No |
| Peek Right (Mobile) | Number of pixels to the right to show on mobile. Allows you to peek slides to the right. (Glide.js specific) | No |
| Peek Right (Tablet) | Number of pixels to the right to show on tablet. Allows you to peek slides to the right. (Glide.js specific) | No |

!!! Attention "About the "Peek Options""
	To use the peek options, you will need to have a left and right value or it will not work properly. These options are typically used on mobile where design tends to show a portion of the next set of slides.


## Examples
> __Carousel with Products__
> ![](/images/ui-library/ui_prodlist_2.png)

> __Carousel with Hero Sections__
> ![](/images/ui-library/ui_carousel_2.png)

> __Carousel with Images__
>![](/images/ui-library/ui_carousel_3.png)


## Misc Notes

Note the framework package defaults to using _Glide.js_ as the library for the carousel. But you can use your own version _Carousel.js_ to extend or use another carousel library. For example, the _Duluth Pack_ site build uses _Swiper_ as the carousel library.
