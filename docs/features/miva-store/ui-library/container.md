# Container

A high-level component used primarily for spacing. Anything can be nested inside of a container.

## Attributes

|Attribute|Description|
| -------- |:------------- |
|Background Color| Add a background color to the container element |
|Responsive Background Images| Add a background image to the container element |
|Padding-Y| Add vertical padding (inside space) to the container |
|Padding-X| Add horizontal padding (inside space) to the container |
|Margin Bottom| Add bottom spacing (outer space) to the container. Commonly used to add spacing between elements. |
|Theme Font Color Select| Determine the text color of the container according to theme settings (primary, secondary and tertiary). |
|Custom Font Color Input| Determine the text color of the container. Accepts hex or the actual color name (ie. red). |
|Layer Opacity| Add an opacity layer above the image and behind the text color. Used to add more contrast to the container's copy. |
|Custom Classes| Assign custom classes to the container |
