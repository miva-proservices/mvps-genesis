# Embedded Content

A standalone element for embedded content, but can also be used to store static HTML content. Ideal if you want to embed content such as iframe content or JavaScript widgets.

![](/images/ui-library/ui_embed.png)

![](/images/ui-library/ui_embed_2.png)


## Attributes

|Attribute|Description|Required|
| -------- |:-------------| -------- :|
|Embedded Code| Use this field to store your embedded content (ie. YouTube video). | Yes | 
|Container Class| Use this field to assign additional classes to the container of this element/component. | No |
