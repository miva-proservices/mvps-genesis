# Header

The header component allows you to add a title section to any _Components & Layouts_ layout. 

## Attributes

|Attribute|Description|Required|
| -------- |:-------------| -------- :|
|Main Text| The main title that will be displayed for the component | Yes | 
|Lead-in Text| The lead-in text that will be displayed before the _Main Text_ element | No |
|Additional Copy| Allows you to write some copy below the _Main Text_ element | No |
|CTA Link| Allows you to display a CTA button link below the _Main Text_ and _Additional Copy_ element | No |
|CTA Copy| The copy to display in the CTA button | No |
|CTA Style| The style of the CTA button to apply | No |
|Text Align| The alignment of the text elements in the section | No |
|Spacing Bottom| Allows you to add bottom spacing (margin) to the header section. Similar to the _Container_ component | No |

!!! Attention "Note"
	The CTA button will only show if *both* the CTA link and CTA copy is filled out.

## Examples
![](/images/ui-library/ui_header.png)
