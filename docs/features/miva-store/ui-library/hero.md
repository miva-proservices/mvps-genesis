# Hero

A standalone element for hero sections, but could be used for anything. Standard UI is 2/3 Image with 1/3 text. Can support full width images.

## Attributes

* Header
	* Header Font Size
	* Header Position (controlled with Flex)
	* Header Padding (dictates the height of the container)
	* Header Text Alignment
	* Header Background Color (will cover the entire column in the container, not just the bounding box for the text)
* Subheader (is displayed in smaller text above the header)
* Copy
* CTA
	* Link
	* Copy 
	* Style
	* Destination
* Alternate Link
	* Link (if a product is linked, copy and subcopy will populate with product name and price)
	* Copy 
	* Subcopy
	* Position (absolutely positioned, controlled by flex)
	* Destination
* Layout - Full Image (suggest using opacity if picking this option)
* Responsive Images (mobile, tablet and desktop)
* Theme Font Color Select
* Custom Font Color