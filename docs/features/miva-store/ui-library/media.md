# Media

These are standalone elements that are used to display image or video content (on the server).

## Image

### Attributes

|Attribute|Description|Required|
| -------- |:-------------| -------- :|
|Mobile Image| The image to display on mobile devices and screen sizes | No | 
|Tablet Image| The image to display on tablet devices and screen sizes | No |
|Desktop Image| The image to display on desktop devices and screen sizes | Yes |
|CTA Link| Allows you to display a CTA button link below the image | No |
|CTA Copy| The copy to display in the CTA button | No |
|Container Type| The container type for the image container (based on Elements framework) | No |

### Examples

![](/images/ui-library/ui_image.png)

![](/images/ui-library/ui_image_2.png)

## Video

### Attributes

|Attribute|Description|Required|
| -------- |:-------------| -------- :|
|Fallback Image| The fallback/poster image to use for the video. | Yes | 
|MP4| The MP4 version of the video file | No |
|WebM| The WebM version of the video file | No |
|OGG| The OGG version of the video file | No |
|CTA Link| Allows you to display a CTA button link below the video | No |
|CTA Copy| The copy to display in the CTA button | No |
|Container Type| The container type for the video container (based on Elements framework) | No |
