# Product List

The product list component allows you to load a category's assigned products to any _Components & Layouts_ layout. 

## Attributes

|Attribute|Description|Required|
| -------- |:-------------| -------- :|
|Category| The category code to load products from | Yes | 
|Number of Products| The number of products to load from the category. Defaults to 8 products at a time. | No |

## Examples

![](/images/ui-library/ui_prodlist.png)

![](/images/ui-library/ui_prodlist_2.png)
