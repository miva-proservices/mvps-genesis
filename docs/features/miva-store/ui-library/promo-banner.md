# Promo Banner

A skinny banner usually placed at the top of a layout. If linked, the entire container will be clickable.

## Attributes

* Promo Text
* Background Image
* Background Color
* Font Bold
* Text Uppercase
* CTA
	* Link
	* Link Text
	* Destination
* Theme Font Color Select
* Custom Font Color