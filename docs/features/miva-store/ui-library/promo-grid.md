# Promo Grid

A set of images displayed as either a 1 (row) * N (column) layout or a 1 (large) + 2(small) layout.

## 1+2 Layout

A group of 3 images arranged into one large image with two smaller images. Split into two columns (66% / 33%).

**Component Code**: `cl_promo_grid_1+2`

### Attributes

| Code | Type | Required | Description |
| --- | --- | --- | --- |
| layout | Radio Buttons | Yes | Used to determine which side the large image is on: left or right |
| large_img_desktop | Image | Yes | The `src` attribute of the large Desktop image |
| large_img_tablet | Image | No | The `src` attribute of the large Tablet image |
| large_img_mobile | Image | No | The `src` attribute of the large Mobile image |
| large_header_leadin_text | Text Field | No | The `innerText` of the element positioned above the large Main Text |
| large_header_main_text | Text Field | No | The `innerText` of the large Main Text header |
| large_header_sub_text | Text Area | No | The `innerText` of the element positioned below the large Main Text |
| large_cta_link | Link | No | The `href` attribute of the large call to action anchor link |
| large_cta_text | Text Field | No | The `innerText` of the large call to action anchor link |
| smalltop_img_desktop | Image | Yes | The `src` attribute of the small-top Desktop image |
| smalltop_img_tablet | Image | No | The `src` attribute of the small-top Tablet image |
| smalltop_img_mobile | Image | No | The `src` attribute of the small-top Mobile image |
| smalltop_header_main_text | Text Field | No | The `innerText` of the element positioned above the small-top Main Text | The `href` attribute of the large call to action anchor link |
| smalltop_cta_link | Link | No | The `href` attribute of the small-top call to acition anchor link |
| smalltop_cta_text | Text Field | No | The `innerText` of the small-top call to action anchor link |
| smallbot_img_desktop | Image | Yes | The `src` attribute of the small-bot Desktop image |
| smallbot_img_tablet | Image | No | The `src` attribute of the small-bot Tablet image |
| smallbot_img_mobile | Image | No | The `src` attribute of the small-bot Mobile image |
| smallbot_header_main_text | Text Field | No | The `innerText` of the element positioned above the small-bot Main Text | The `href` attribute of the large call to action anchor link |
| smallbot_cta_link | Link | No | The `href` attribute of the small-bot call to acition anchor link |
| smallbot_cta_text | Text Field | No | The `innerText` of the small-bot call to action anchor link |

## 1xN Layout

A single row of images arranged in a column layout. It uses the `u-grids-{N}` helper classes to arrange the images.

**Component Code Pattern**: `cl_promo_grid_1x{N}` - where `N` is a number from 1 to `$column-count` (12)

**Attribute Code Pattern**: `i{N}_{attribute code}` - where `N` is a number starting at 1. Create a copy of the following attributes with increasing `N` value up until the total number of columns.

### Attributes

| Code | Type | Required | Description |
| --- | --- | --- | --- |
| i{N}_img | Image | Yes | The `src` of the image tag |
| i{N}_cta_link | Link | No | The `href` attribute of the call to action anchor |
| i{N}_cta_text | Text Field | No | The `innerText` of the call to action anchor |
| i{N}_cta_type | Drop-down List | No | The display style of the call to action anchor: button or link |
| i{N}_cta_behavior | Drop-down List | No | The `target` attribute: _blank or _self |