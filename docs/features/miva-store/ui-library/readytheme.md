# ReadyTheme

A standalone element that is used to display ReadyTheme content within a _Components and Layouts_ layout.

## Attributes

|Attribute|Description|Required|
| -------- |:-------------| -------- :|
|ReadyTheme Code| The ReadyTheme code to use/display | Yes | 
|ReadyTheme Type| The type (ie. Content Section, Image and etc) of the ReadyTheme section | Yes |
|Container Type| The container type for the ReadyTheme container (based on Elements framework) | No |
