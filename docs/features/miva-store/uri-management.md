# URI Management

The URI Management templates & settings do not import into a store after a applying a framework pkg file, so if the client does not already have an established URI scheme, then we recommend manually updating them to the following templates. Take not that:

* The [Page URIs](#page-uris) have logic to only output a `/` for the SFNT page
* Each page, category, & product template has a static-directory prefix at the beginning of the URI (`page/`, `category/`, `product/`) so that it can be used for analytics tracking & reporting.

## URI Templates

!!! attention
    Only make these changes if the client does not already have an established URI scheme

### Page URIs

```xml
/<mvt:if expr="l.settings:page:code NE 'SFNT'">page/<mvt:if expr="NOT ISNULL l.settings:page:slugified_title"><mvt:eval expr="tolower( l.settings:page:slugified_title )" /><mvt:else><mvt:eval expr="tolower( l.settings:page:slugified_name )" /></mvt:if>.html</mvt:if>
```

### Category URIs

```xml
/category/<mvt:if expr="NOT ISNULL l.settings:category:slugified_page_title"><mvt:eval expr="tolower( l.settings:category:slugified_page_title )" /><mvt:else><mvt:eval expr="tolower( l.settings:category:slugified_name )" /></mvt:if>.html
```

### Product URIs

```xml
/product/<mvt:if expr="NOT ISNULL l.settings:product:slugified_page_title"><mvt:eval expr="tolower( l.settings:product:slugified_page_title )" /><mvt:else><mvt:eval expr="tolower( l.settings:product:slugified_name )" /></mvt:if>.html
```
