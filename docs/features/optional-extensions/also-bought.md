# Also Bought

Display to customers the last 5 products that have been recently purchased with the product they are currently viewing.

## Installation

1. Install the `optional-extensions/also-bought` package on your theme:
	* Copy the extension from `optional-extensions/also-bought` to `themes/custom-extensions/also-bought`
2. Create a ReadyTheme Content Section `also_bought__prod` and [place this template code in it](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/optional-extensions/also-bought/miva-templates/readytheme/contentsection/also_bought__prod.mvt)
3. Add the `<mvt:item name="readytheme" param="contentsection( 'also_bought__prod' )" />` to the `PROD` page where you want the products to display
4. Create a ReadyTheme Content Section `also_bought__order` and [place this template code in it](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/optional-extensions/also-bought/miva-templates/readytheme/contentsection/also_bought__order.mvt)
5. Add the `<mvt:item name="readytheme" param="contentsection( 'also_bought__order' )" />` to the footer of the `INVC`.
6. Create a product textarea customfield with the code `also_bought`
7. Create an order checkbox customfield with the code `also_bought_updated`
8. Import the also-bought scss by placing this into the top of `.scss` file of your desired page/functionality. This will usually be `pages/PROD/PROD.scss`

```css
@import "custom-extensions/also-bought/index";
```
