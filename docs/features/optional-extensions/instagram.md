# Instagram Social Feed

The Instagram Social feed allows store-owners to display their Instagram profiles most recent posts. This is typically used as a component on the SFNT page.

## Store Implementation Setup

1. Get the client's Instagram username & password (or do a screen-share with the client as you go through this process)
2. Go to [developers.facebook.com](https://developers.facebook.com/), and log into our `mivaprofsrvcs1@gmail.com` Facebook account (see PS team-mate or ssoule for password) 
3. Open the [Genesis Social Feed app](https://developers.facebook.com/apps/235256434540010/settings/basic/)
4. [Navigate to Products > Instagram > Basic Display](https://developers.facebook.com/apps/235256434540010/instagram/basic-display/)
5. Scroll down to "User Token Generator" and click "Add or Remove Instagram Testers"
6. Scroll down to "Instagram Testers" and click "Add Instagram Testers"
7. Lookup/enter the client's company instagram profile/username and invite them.
8. Login into [the client's Instagram account and go to their: Apps & Websites > Tester Invites](https://www.instagram.com/accounts/manage_access/) section to Approve the invite
9. Go back to [Navigate to Products > Instagram > Basic Display](https://developers.facebook.com/apps/235256434540010/instagram/basic-display/), scroll down to User Token Generator and click on "Generate Token" for the client's account.
10. Log into the client's instagram account
11. After logging in, you'll get an access token. 
12. Save a copy of the access token so you can use it on their store.
13. Install [the latest Transients module](https://github.com/tessguefen/miva_transients/releases) on their store.
14. Create a ReadyTheme Content Section with a code of `instagram_feed` and [fill it with the sample template code](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/optional-extensions/instagram-feed/miva-templates/readytheme/contentsection/instagram_feed.mvt)
15. Update the `l.access_token` and `l.developer_email` variables for your store.
16. Add the `instagram_feed` RT CS to the store where you want it to display
17. Customize and extend the layout/design to fit the customer's needs.

## Resources

* https://www.instagram.com/develop/
	* https://developers.facebook.com/docs/instagram-basic-display-api/overview#user-token-generator
* https://www.instagram.com/about/legal/terms/api/?fbclid=IwAR3zQqGJU5ihC2VTCKScqONrammuWD8OszV7pSMsXHvSh2j1qS-owcv5ROY
* https://developers.facebook.com/docs/instagram-basic-display-api
* https://developers.facebook.com/docs/instagram-basic-display-api/overview
* https://developers.facebook.com/docs/instagram-basic-display-api/guides/getting-access-tokens-and-permissions
* https://developers.facebook.com/docs/instagram-basic-display-api/guides/long-lived-access-tokens#refresh-a-long-lived-token
* https://developers.facebook.com/docs/instagram/oembed