# Share Product

Use this module to include product sharing on social channels (Facebook, Twitter, Pinterest) and email.

## Installation

Copy `optional-extensions/share-product` to `themes/custom-extensions/share-product`.
Add `import shareProduct from 'custom-extensions/share-product';` to your `PROD.js` file.

We recommend creating two Readytheme Content Sections to house the necessary code, see below. 

Call the share_product_entities in the HEAD tag of your template. 

Call the share_product_ui where you want the icons to display.

## Example Usage

Use the following code to import the product share pop up functionality. 

```js

// Place this into the top of PAGE.js file of your desired page (will usually be PROD.js).

import shareProduct from 'custom-extensions/share-product'; 

// This goes inside your .onready() function call 

shareProduct(); 

```

```html

<!-- Entities -->

<mvt:assign name="l.settings:share_product:url" value="l.settings:product:link" />
<mvt:assign name="l.settings:share_product:text" value="'Check out the deal on ' $ l.settings:product:name $ ' at ' $ g.store:name" />
<mvt:do file="g.Module_Library_DB" name="l.success" value="ImageType_Load_Code('main', l.settings:imagetype)" />
<mvt:do name="l.success" file="g.Module_Library_DB" value="ProductImage_Load_Type(l.settings:product:id, 1, l.settings:imagetype)" />
<mvt:do name="l.success" file="g.Module_Library_DB" value="Image_Load_ID(l.settings:imagetype:image_id, l.settings:imagedata)" />
<mvt:assign name="l.settings:share_product:image" value="g.baseurl $ l.settings:imagedata:image" />

<mvt:assign name="l.settings:share_product:mail:to_email" value="''" />
<mvt:assign name="l.settings:share_product:mail:to_subject" value="g.store:name $ ' - ' $ l.settings:product:name" />
<mvt:assign name="l.settings:share_product:mail:to_body" value="l.settings:share_product:text $ '%0A%0A' $ l.settings:share_product:url" />


<!-- UI -->

<div class="social-links">
    <mvt:if expr="l.settings:readytheme:social_facebook">
        <a class="js-share-product-button" data-social="facebook" title="Facebook" href="https://www.facebook.com/sharer/sharer.php?" data-url="&mvte:share_product:url;" target="_blank" >
            <span class="u-icon-facebook"></span>
        </a>
    </mvt:if>
    <mvt:if expr="l.settings:readytheme:social_twitter">
        <a class="js-share-product-button" data-social="twitter" title="Twitter" href="https://twitter.com/intent/tweet?url=&mvt:share_product:url;&text=&mvte:share_product:text;" target="_blank" >
            <span class="u-icon-twitter"></span>
        </a>
    </mvt:if>
    <mvt:if expr="l.settings:readytheme:social_pinterest">
        <a class="js-share-product-button" data-social="pinterest" data-media="&mvt:share_product:image;" data-description="&mvte:share_product:text;" data-url="&mvte:share_product:url;" title="Pinterest" href="http://pinterest.com/pin/create/button/" target="_blank" >
            <span class="u-icon-pinterest"></span>
        </a>
    </mvt:if>
    <a href="mailto:&mvt:share_product:mail:to_email;?subject=&mvte:share_product:mail:to_subject;&body=&mvte:share_product:mail:to_body;" title="Email a Friend" >
        <span class="u-icon-envelope"></span>
    </a>
</div>

<!-- Entities should go inside the head tag of your template. Use this snippet to load without the wrapping content section div. If you do not do this, it will break and load inside the body tag -->

<!-- Place meta tags below entity load -->

<mvt:item name="readytheme" param="load_contentsection( 'share_product_entities', g.readytheme_templates:share_product_entities )" />
<mvt:do file="g.Module_Feature_TUI_DB" name="l.loaded" value="ManagedTemplate_Load_ID( g.readytheme_templates:share_product_entities:templ_id, l.loaded_paged )" />
<mvt:do file="g.Store_Template_Path $ l.loaded_paged:filename" name="l.success" value="Template_Render( l.empty, l.settings )" />

<meta property="og:title" content="&mvte:product:name;">
<meta property="og:type" content="product">
<meta property="og:image" content="&mvte:share_product:image;">
<meta property="og:url" content="&mvte:share_product:url;">
<meta property="og:site_name" content="&mvte:global:store:name;">
<meta property="og:description" content="&mvte:share_product:text;">
<meta property="og:locale" content="en_US">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@TWITTER_ACCOUNT">
<meta name="twitter:creator" content="@TWITTER_ACCOUNT">
<meta name="twitter:url" content="&mvte:share_product:url;">
<meta name="twitter:title" content="&mvte:product:name;">
<meta name="twitter:description" content="&mvte:share_product:text;">
<meta name="twitter:image" content="&mvte:share_product:image;">
<meta name="twitter:image:alt" content="&mvte:product:name;">

```
