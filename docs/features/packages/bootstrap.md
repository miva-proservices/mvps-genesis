# Bootstrap

Required scripts and styles for development with the Genesis framework.

[View in BitBucket](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/packages/bootstrap)

## Installation

`$ pnpm add @mvps-genesis/bootstrap`

## Features

### [@Class] - PageManager

Standardize page behavior into an extendable class.

### [@function] - bootstrap

Configure context variables, global script slot and routing for screens/pages.

## Example Usage

### .js
```js
// ==== Page Manager ==== //
import { PageManager } from '@mvps-genesis/bootstrap';

// extend PageManager class for each page/screen
class Global extends PageManager {
	
	constructor( pageContext ) {

		super( pageContext );

		// runs immediately

	}

	onReady() {

		// runs after `DOMContentLoaded` event

	}

}

// ==== bootstrap ==== //
import { bootstrap } from '@mvps-genesis/bootstrap';

// route pages/screens to PageManager extensions
bootstrap({

	// pass your MVT to JS namespace
	context: mivaJS,

	// Global Page class
	globalClasss: Global,

	// A directory of page classes or scss files to be loaded by matching the `Screen` context variable
	screenImports: {
		// EXAMPLE: () => import( 'pages/example/example.js' )
	},

	// A directory of page classes or scss files to be loaded by matching the `Page_Code` context variable
	pageCodeImports: {
		// EXAMPLE: () => import( 'pages/example/example.js' )
	},

	// A directory of page classes or scss files to be loaded by matching the `Screen` or `Page_Code` context variable against a regular expression
	regexImports: {
		// '/example/i': () => import( 'pages/example/example.js' )
	}

});
```

### .scss
```scss
@import "scss/styles";
```

## Options

### PageManager

Standardize page behavior into an extendable class

| Key | Required | Default | Description |
| --- | --- | --- | --- |
| pageContext | No | [@object undefined] | an object that contains mapping from MVT to JS variables |

### bootstrap

Configure context variables, global script slot and routing for screens/pages.

| Key | Required | Default | Description |
| --- | --- | --- | --- |
| context | Yes | [@object window.mivaJS] | an object that contains mapping from MVT to JS variables |
| globalClass | No | [@object null] | a class instance (extension of PageManager) that will be loaded first for every page request |
| screenImports | Yes | [@object {}] | an object of functions that return a dynamic import to a file (js or scss); the key within the object will be tested against the `Screen` context variable |
| pageCodeImports | Yes | [@object {}] | an object of functions that return a dynamic import to a file (js or scss); the key within the object will be tested against the `Page_Code` context variable |
| regexImports | Yes | [@object {}] | an object of functions that return a dynamic import to a file (js or scss); the key within the object will be tested against both `Screen` and `Page_Code` context variable via a regex expression |