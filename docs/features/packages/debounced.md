# Debounced

Returns a function that will wait a certain amount of time before running the passed function.

[View in BitBucket](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/packages/debounced)

## Parameters

### [@int] - delay

milliseconds to delay before running `fn`

### [@function] - fn

the function to run after the delay

## Return Value

A function, decorated with the debounce logic.

## Example Usage

```js
import debounced from '@mvps-genesis/debounced';

// storing as a variable to be called later
const debounced = debounced( 200, () => { console.log( 'Fire!' ) } );
debounced();

// self-executing
debounced( 200, () => { console.log( 'Fire!' ) } )();
```