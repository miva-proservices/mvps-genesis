# Decode Entities

Converts a string containing HTML entities to a plain string with ANSII characters. Also, can be used to strip HTML characters from a string.

[View in BitBucket](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/packages/decode-entities)

## Parameters

### [@string] - str

A string to convert to plain text.

## Return Value

The sanitized string.

## Example Usage

```js
import decodeEntities from '@mvps-genesis/decode-entities';

const plain = decodeEntities( '<div>Soon&trade;</div>' );

console.log( plain );

// Soon™
```