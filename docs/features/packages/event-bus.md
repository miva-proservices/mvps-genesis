# Event Bus

[View in BitBucket](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/packages/event-bus)

## Installation

`$ pnpm add @mvps-genesis/event-bus`

## Example Usage

### .js

```js
import EventBus from '@mvps-genesis/event-bus';

// create example listener callback function
const listener = ( e ) => {

	const data = e.detail;

};

// listen for a global event
EventBus.addEventListener( type, listener [, options] );

// remove listener for a global event
EventBus.removeEventListener( type, listener [, options] );

// dispatch global event. `data` will be accessable via `e.detail` within the listener callback.  
EventBus.dispatchEvent( type, { detail: data } );
```

## Options

See standard documentation for [EventTarget](https://developer.mozilla.org/en-US/docs/Web/API/EventTarget).