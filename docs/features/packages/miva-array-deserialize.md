# Miva Array Deserialize

Converts a `miva_array_serialize` string to a JavaScript object.

[View in BitBucket](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/packages/miva-array-deserialize)

## Parameters

### [@String] - input

the input to be parsed as a serialized string

## Return Value

A parsed object or value, `null` if unable to parse

## Example Usage

```js
import mivaArrayDeserialize from '@mvps-genesis/miva-array-deserialize';

const myObject = mivaArrayDeserialize( ':products[1]:code=123,:products[2]:code=456' );

console.log( myObject );

/*
output:

{
	products: [
		{
			code: 123
		},
		{
			code: 456
		}
	]
}
*/
```