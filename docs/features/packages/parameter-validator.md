# Parameter Validator

[View in BitBucket](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/packages/parameter-validator)

## Parameters

### [@Object] or [@string] - instanceOrName

Either `this` or a string representing the name of the function that validator is executed in. Used to enhance the error messages that are thrown.

## Return Value

A `ParameterValidator` instance.

## Methods

### Type Checks

All type check methods contain the following params/options:

* __key__ - The name of the property to test
* __value__ - The value of the property to test
* Options:
	* __required__ - whether to first check if value is defined
	* __throwError__ - whether to throw a TypeError or not
	* __returnValue__ - whether to return the value on success
	* __returnMessage__ - whether to return the message on success

---

* `isArray`
* `isBoolean`
* `isDefined`
* `isElement` - adds additional option, `container`; defaults to `document`
* `isElementList` - adds additional option, `container`; defaults to `document`
* `isFloat`
* `isFunction`
* `isInstance` (e.g. constructor = HTMLFormElement)
* `isInt`
* `isNumber`
* `isObject`
* `isString`

### Other

* `inSet` - Check if value exists within the array provided
* `validate` - Validate an object or an array against a "ruleset"

## Example Usage

### Type Checks

```js
import ParameterValidator from '@mvps-genesis/parameter-validator';

class Example {

	constructor( a, b, c, d, e ) {

		const validator = new ParameterValidator( this );

		a = validator.isString( 'a', a );
		b = validator.isNumber( 'b', b );
		c = validator.isObject( 'c', c );
		d = validator.isFunction( 'd', d );
		e = validator.isElement( 'e', e, { container: '#my-container' } );

	}

}

// PASS!
new Example(
	'myString',
	123,
	{ foo: 'bar' },
	() => console.log( 'Hello World!' ),
	'#my-element'
);

// FAIL! error = TypeError: Example :: `a` must be a string
new Example(
	123,
	...
	...
)
```

### Other

```js
import ParameterValidator from '@mvps-genesis/parameter-validator';

const validator = new ParameterValidator( 'Example' );

// ==== inSet ==== //

// PASS! 'foo' is within the set
validator.inSet( 'myValue', 'foo', [ 'foo', 'bar', 'baz' ] );

// FAIL! 'foo' is not in the set
validator.inSet( 'myValue', 'foo', [ 'bar', 'baz' ] );

// ==== validate ==== //

const myRules = {
	products: [
		{
			code: 'String',
			id: 'Int',
			name: 'String',
			url: 'URL?', // optional property descriptions include the `?`
			customfield_values: {
				customfields: {
					color: 'String'
				}
			}
		}
	],
	productsPerPage: 'Int'
};

const myData = {
	products: [
		{
			code: 'product-1',
			id: 1,
			name: 'Product 1',
			customfield_values: {
				customfields: {
					color: 'red'
				}
			}
		},
		{
			code: 'product-2',
			id: 2,
			name: 'Product 2',
			customfield_values: {
				customfields: {
					color: 'blue'
				}
			}
		}
	],
	productsPerPage: 12
};

validator.validate( 'myData', myData, myRules );
```