# string2regex

Converts a string, representing an expression, to a RegExp object

[View in BitBucket](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/packages/string2regex)

## Parameters

## [@string] - str

The string to convert. Must include the slashes.

## Return Value

A `RegExp` object if successful. `undefined` on error.

## Example Usage

```js
import string2regex from '@mvps-genesis/string2regex';

const regex = string2regex( '/[a-z]+/g' );

regex.exec( 'abc!' );
```