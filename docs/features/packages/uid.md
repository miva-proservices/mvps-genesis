# UID

Generates a unique id string.

[View in BitBucket](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/packages/uid)

## Parameters

None

## Return Value

A string containing the uid generated.

## Example Usage

```js
import uid from '@mvps-genesis/uid';

const myUid = uid();
const anotherUid = uid();

console.log( myUid === anotherUid ); // false
```