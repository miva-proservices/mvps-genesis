# Wait For Expression

Tests an expression every 200ms until true of 5000ms has passed. Runs a callback on Success and End.

[View in BitBucket](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/packages/wait-for-expression)

## Parameters

### [@Function] - expression

a function that will be evaluated every tick, stops the process if true

### [@Function] - onSuccess

A function that is called on success

### [@int] - options.intervalTime

How often to check the expression in ms. Defaults to 200ms.

### [@int] - options.maxWaitTime

The maximum time to check for before expiring. Defaults to 5000ms.

### [@Function] - options.onEnd

Callback function ran if timer expires. Defaults to logging a warning to the console.

## Return Value

`undefined`

## Example Usage

```js
import waitForExpression from '@mvps-genesis/wait-for-expression';

let myVar = false;

waitForExpression(
	() => myVar == true,
	() => alert( 'Success!' )
)
```