# PNPM Scripts

There are several pnpm scripts in the framework's `package.json` to help start your local development or build the assets to deploy to production.

There are four primary commands: `start`, `build`, `start-launch` and `build-launch`. Each one of these commands accepts an optional argument of `[env]`. The `env` argument points to a defined property within the `theme-settings.json` file. For example running `pnpm run start live` would start the local proxy pointing to the `live` environment.

For more information on how to use these scripts, see the [developer workflow](../develop/workflow.md) section.

We use a node file to handle these arguments and flags. It is is located at `./scripts/run.js`.

## Recommended Reading

- <https://docs.npmjs.com/misc/scripts>
- <https://css-tricks.com/why-npm-scripts/>

## Common Shortcuts

These are the two most common tasks that you may need to run:

### `start`

- *Command:* `$ pnpm run start [env]`
- *Description:* This script will trigger the [local development workflow](../develop/workflow.md) for webpack to compile your initial set of files, watch for updated SCSS & JS files, and setup the browser-sync local-server.

### `start-launch`

- *Command:* `$ pnpm run start-launch [env]`
- *Description:* Runs the same script as "start" but uses the "launchThemePath" instead.

### `build`

- *Command:* `$ pnpm run build [env]`
- *Description:* This script will run the production build command which is useful for generating the files that you can deploy to a miva-store

### `build-launch`

- *Command:* `$ pnpm run build-launch [env]`
- *Description:* Runs the same script as "build" but uses the "launchThemePath" instead.

## Internal `run.js` Command Arguments

- `--env.NODE_ENV`
	- *Possible Values:* `production` or `development`
	- *Description:* These correspond to [webpack's build-modes & environment variables](https://webpack.js.org/guides/environment-variables/) which optimize for speed (`development`) or minification of assets (`production`).
- `--config`
	- *Possible Values:* `webpack.prod.js` or `webpack.local.js`
	- *Description:* This describes which [config file is passed to webpack](https://webpack.js.org/configuration/#use-different-configuration-file)
- `--env.MIVA_ENV`
	- *Possible Values:* `live`, `staging`, or `dev`
	- *Description:* These values correspond to the object-keys of the `environments` object within the `theme-settings.json` file. These are primarily used for you local-development workflow to proxy various different miva-environments.
- `--env.LAUNCH`
	- *Possible Values:* Optional, `true` or omit it entirely
	- *Description:* When this is set to `true`, this will build a production version of the build to the `theme-settings.json` file's `launchThemePath` destination. This is useful for the uploading & testing changes before they are made live.

## Monorepo Scripts

### Common in Development

#### `bootstrap`

- *Command:* `$ pnpm run bootstrap`
- *Description:* Recursively install all of the packages' dependencies.
- *Uses:* Run this after switching between branches

#### `start`

- *Command:* `$ pnpm run start`
- *Description:* Shortcut for starting the BrowserSync local development server
- *Uses:* Use anytime your terminal is at the root of the repo and you want to quickly start the dev server

### One-time Use

#### `init`

- *Command:* `$ pnpm run init`
- *Description:* A one-time repo-setup command to enable LFS, setup the githooks, install dependencies
- *Uses:* Run this after cloning the repo for the first time.

#### `init:lfs`

- *Command:* `$ pnpm run init:lfs`
- *Description:*  A one-time repo-setup command to enable LFS
- *Uses:* Run this if you haven't run `pnpm run init` or if you haven't enabled LFS


#### `init:theme`

- *Command:* `$ pnpm run init:theme`
- *Description:* Installs dependencies and runs the build command

#### `init:githooks`

- *Command:* `$ pnpm run init:githooks`
- *Description:* A one-time repo-setup command to enable the repos githooks.
- *Uses:* Run this if you haven't run `pnpm run init` or if you haven't installed the githooks on your repo

### Publishing

#### `pre-release`

Publishes all packages within the workspace that do not have a version on the npm registry. Sets the npm tag to `alpha`.

#### `release`

Publishes all packages within the workspace that do not have a version on the npm registry. Uses the default tag (`latest`).

### Automatically Run

#### `preinstall`

- *Command:* `$ pnpm run preinstall`
- *Description:* This ensures that you are using `pnpm` to install decencies; instead of `npm` or `yarn`
- *Uses:* This is automatically called by pnpm when you `pnpm install/add`, so you do not need to run it.

#### `version` (Packages Only)

Recursively updates other packages within the workspace that depend on it.

#### `publish` (Packages Only)

Create and push a git tag with the following format:

```bash
$npm_package_name@$npm_package_version

# example:	@mvps-genesis/bootstrap@1.0.0
```