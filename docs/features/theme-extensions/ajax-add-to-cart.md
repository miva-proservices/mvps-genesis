# AJAX Add to Cart

Handle the form submission of a Miva ADPR form with AJAX. Fires a global event via the [EventBus](../packages/event-bus.md). The event name is `MVPS:addToCart`.

## Installation

Add `import AjaxAddToCart from 'extensions/ajax-add-to-cart';` To the top of any file that needs to import this extension

## Example Usage

Initialize the class using the following call:

### .js
```js
import AjaxAddToCart from 'extensions/ajax-add-to-cart';

new AjaxAddToCart( formElementOrSelector [, options] );
```

### .mvt
```
<form id="js-purchase-form" action="&mvte:urls:BASK:auto;" method="post">

	<input type="hidden" name="Action" value="ADPR" />
	<input type="hidden" name="Product_Code" value="&mvte:product:code;" />
	<input type="hidden" name="Category_Code" value="&mvte:global:category_code;" />

	<div class="js-add-to-cart-message"></div>

	<span onclick="document.forms.add.action = '&mvtj:urls:BASK:auto;'; document.forms.add.elements.Action.value = 'ADPR';">
		<input class="js-add-to-cart" data-action="&mvte:urls:BASK:auto_sep;ajax=1" data-value="Add To Cart" type="submit" value="Add To Cart">
	</span>

</form>
```

## Options

You can also pass the following settings to change the behavior of the class:

```js
new AjaxAddToCart(
	'#js-purchase-form', // Selector or Element for the form.
	{
		submitButtonElement: '.js-add-to-cart', // Selector or Element for the form submit button.
		actionInputElement: 'input[name="Action"]', // Selector or Element for the action input.
		responseMessageElement: '.js-add-to-cart-message', // Selector or Element for the message container.
		processingButtonText: 'Processing...', // Text to used as the value or innerHTML of `submitButtonElement` while the AJAX call is processing.
		stockMessageErrorSelector: '.x-messages--error', // This is used when the product is out of stock, or limited stock (POUT, PLMT)
		successMessage: {
			message: 'Added to cart.',
			options: {
				autohide: true,
				type: 'success'
			}
		},
		missingAttributesMessage: { // this message has a special token `%missingAttributes%` which will be replaced by a comma separated list of attribute prompts
			message: 'All <em class="u-color-error">Required</em> options have not been selected.<br />Please review the following options: <span class="u-color-error">%missingAttributes%</span>',
			options: {
				autohide: true,
				type: 'warning'
			}
		},
		limitedStockMessage: {
			dynamic: true, // If set to true, will use the error message returned from PLMT if available.
			message: 'We do not have enough of the combination you have selected.<br />Please adjust your quantity.',
			options: {
				autohide: true,
				type: 'warning'
			}
		},
		outOfStockMessage: {
			dynamic: true, // If set to true, will use the error message returned from POUT if available.
			message: 'The combination you have selected is out of stock.<br />Please review your options or check back later.',
			options: {
				autohide: true,
				type: 'warning'
			}
		},
		otherMessage: {
			message: 'Please review your selection.',
			options: {
				autohide: true,
				type: 'warning'
			}
		}
	}
)
```

## Event

### `MVPS:addToCart`

Triggers after the AJAX call is resolved. The event payload will contain the following information:

```
{
	success: Boolean,
	message: String,
	data: HTMLDocument,
	formData: FormData,
	request: XMLHttpRequest
}
```

## Extending the Class

If there are modifications to the base functionality that is not covered by options you can extend the class to modify it's functionality. You can extend it by creating a new file (most likely in your `extensions` folder) and import the `AjaxAddToCart`. After importing you can use the `extends` keyword to create a new extended class.

```js
import AjaxAddToCart from 'extensions/ajax-add-to-cart';

// you can call the class whatever you want
export default class ExtendedAjaxAddToCart extends AjaxAddToCart {

	// add or overwrite properties here

	constructor( formElement, options ) {
		
		super( formElement, options );

		// add additional constructor logic here

	}

	// add or overwrite methods here

};
```
