# Breadcrumbs
​
Use this module to include breadcrumbs on your Miva store with options for mobile UI. 

## Features

- 3 options for mobile breadcrumb UI.
- All options have the same desktop UI. 
- Home is always the first breadcrumb. 
- Parent Category is always the last breadcrumb, Current is left off.
- Schema markup is also included.

## Installation
​
Import into your `Global.scss` file with `@import "extensions/breadcrumbs/index"`
​
We recommend creating one Readytheme Content Section to house the necessary code, see below. 
​
## Example Usage
​
Use the following code to import the breadcrumb functionality. 
​
```js

// Place this into Javascript resources in a footer resource group. Set the type to application/ld+json.  

{
	"@context": "https://schema.org",
	"@type": "BreadcrumbList",
	"itemListElement": [{
		"@type": "ListItem",
		"position": 1,
		"name": "Home",
		"item": "&mvtj:breadcrumbs:homelink;"
		},
		<mvt:assign name="l.settings:breadcrumb:position" value="2"/>
		<mvt:foreach iterator="cats" array="breadcrumbs:links">
			{
				"@type": "ListItem",
				"position": &mvtj:breadcrumb:position;,
				"name": "&mvtj:cats:name;",
				"item": "&mvtj:cats:link;"
			<mvt:if expr="l.settings:cats:code EQ l.settings:breadcrumbs:current_item:code">
				}
			<mvt:else>
				},
			</mvt:if>
			
			<mvt:assign name="l.settings:breadcrumb:position" value="l.settings:breadcrumb:position + 1"/>

		</mvt:foreach>
	]
}
​
```

```css

/* Place this into the top of Global.scss file. */

@import "extensions/breadcrumbs/index";
​
```

## UI Types

### Default (Mobile Parent Category)

![alt text](../../images/breadcrumbs-mobile-parent.png "Breadcrumbs: Mobile Parent UI")

These breadcrumbs will always display the parent category as a button. <- More Parent Category.

```html
​
<!-- UI -->

<!-- Required: Get the parent of the current category -->
<mvt:assign name="l.parent_cat_index" value="miva_array_elements( l.settings:breadcrumbs:links ) - 1" />

<!-- .x-breadcrumbs -->
<div class="o-wrapper t-breadcrumbs">
	<nav class="x-breadcrumbs x-breadcrumbs--mobile-parent">
		<ul class="x-breadcrumbs__list u-flex u-block--l o-list-inline">
			<mvt:if expr="l.parent_cat_index EQ 0">
				<li class="o-list-inline__item x-breadcrumbs__list__item u-inline--l x-breadcrumbs--current">
					<a class="u-color-black u-flex u-inline--l o-layout--align-center" href="&mvt:breadcrumbs:homelink;">
						<span class="u-hidden--l u-flex o-layout--align-center"><span class="x-breadcrumbs--arrow-icon u-icon-arrow-left"></span> Home</span>
					</a>
				</li>
			<mvt:else>
				<li class="o-list-inline__item x-breadcrumbs__list__item u-hidden u-inline--l">
					<a class="u-color-black" href="&mvt:breadcrumbs:homelink;">
						<span>Home</span>
					</a>
				</li>
			</mvt:if>
			<mvt:foreach iterator="cats" array="breadcrumbs:links">
				<mvt:if expr="l.settings:cats:code EQ l.settings:breadcrumbs:current_item:code">
					<mvt:foreachcontinue />
				<mvt:elseif expr="l.pos1 EQ l.parent_cat_index">
					<li class="o-list-inline__item u-text-bold u-inline--l x-breadcrumbs__list__item x-breadcrumbs--current">
						<a class="u-color-black u-flex u-inline--l o-layout--align-center" href="&mvt:cats:link;" title="&mvt:cats:name;">
							<span class="u-hidden--l u-flex o-layout--align-center"><span class="x-breadcrumbs--arrow-icon u-icon-arrow-left"></span> More</span> <span class="x-breadcrumb--mobile-parent--name">&mvt:cats:name;</span>
						</a>
					</li>
				<mvt:else>
					<li class="o-list-inline__item x-breadcrumbs__list__item u-hidden u-inline--l">
						<a class="u-color-black" href="&mvt:cats:link;" title="&mvt:cats:name;">
							<span>&mvt:cats:name;</span>
						</a>
					</li>
				</mvt:if>
			</mvt:foreach>
		</ul>
	</nav>
</div>
<!-- end .x-breadcrumbs -->
​
```

### Mobile Scroll

![alt text](../../images/breadcrumbs-mobile-scroll.png "Breadcrumbs: Mobile Scroll UI")

These breadcrumbs will always display the entire breadcrumb list as a horizontal scroll menu.

```html
​
<!-- UI -->

<!-- Required: Get the parent of the current category -->
<mvt:assign name="l.parent_cat_index" value="miva_array_elements( l.settings:breadcrumbs:links ) - 1" />

<!-- .x-breadcrumbs--mobile-scroll -->
<div class="o-wrapper t-breadcrumbs">
	<nav class="x-breadcrumbs x-breadcrumbs--mobile-scroll">
		<ul class="x-breadcrumbs__list u-flex o-list-inline">
			<li class="o-list-inline__item x-breadcrumbs__list__item">
				<a class="u-color-black" href="&mvte:breadcrumbs:homelink;">
					<span>Home</span>
				</a>
			</li>
			<mvt:foreach iterator="cats" array="breadcrumbs:links">
				<mvt:if expr="l.settings:cats:code EQ l.settings:breadcrumbs:current_item:code">
					<mvt:foreachcontinue />
				<mvt:elseif expr="l.pos1 EQ l.parent_cat_index">
					<li class="o-list-inline__item u-text-bold u-inline--l x-breadcrumbs__list__item x-breadcrumbs--current">
						<a class="u-color-black" href="&mvte:cats:link;" title="&mvte:cats:name;">
							<span>&mvte:cats:name;</span>
						</a>
					</li>
				<mvt:else>
					<li class="o-list-inline__item x-breadcrumbs__list__item">
						<a class="u-color-black" href="&mvte:cats:link;" title="&mvte:cats:name;">
							<span>&mvte:cats:name;</span>
						</a>
					</li>
				</mvt:if>
			</mvt:foreach>
		</ul>
	</nav>
</div>
<!-- end .x-breadcrumbs--mobile-scroll -->
​
```

### Mobile Dropdown

![alt text](../../images/breadcrumbs-mobile-dropdown.png "Breadcrumbs: Mobile Dropdown UI")

These breadcrumbs will always display the entire breadcrumb list as a dropdown menu.

```html
​
<!-- UI -->

<!-- Required: Get the parent of the current category -->
<mvt:assign name="l.parent_cat_index" value="miva_array_elements( l.settings:breadcrumbs:links ) - 1" />

<!-- .x-breadcrumbs--mobile-dropdown -->
<div class="o-wrapper t-breadcrumbs">
	<nav class="x-breadcrumbs x-breadcrumbs--mobile-dropdown">
		<input id="breadcrumbTrigger" class="x-breadcrumbs--mobile-dropdown--trigger u-hidden--l" type="checkbox" />
		<label class="x-breadcrumbs--mobile-dropdown--label u-color-black u-hidden--l u-flex o-layout--align-center u-text-bold" for="breadcrumbTrigger">More Categories <span class="x-breadcrumbs--mobile-dropdown--icon x-breadcrumbs--mobile-dropdown--down-icon u-icon-chevron-down"></span><span class="x-breadcrumbs--mobile-dropdown--icon x-breadcrumbs--mobile-dropdown--up-icon u-icon-chevron-up"></span></label>
		<ul class="x-breadcrumbs__list o-list-inline">
			<li class="o-list-inline__item x-breadcrumbs__list__item u-block u-inline--l">
				<a class="u-color-black" href="&mvte:breadcrumbs:homelink;">
					<span>Home</span>
				</a>
			</li>
			<mvt:foreach iterator="cats" array="breadcrumbs:links">
				<mvt:if expr="l.settings:cats:code EQ l.settings:breadcrumbs:current_item:code">
					<mvt:foreachcontinue />
				<mvt:elseif expr="l.pos1 EQ l.parent_cat_index">
					<li class="o-list-inline__item u-text-bold u-inline--l x-breadcrumbs__list__item x-breadcrumbs--current">
						<a class="u-color-black" href="&mvte:cats:link;" title="&mvte:cats:name;">
							<span>&mvte:cats:name;</span>
						</a>
					</li>
				<mvt:else>
					<li class="o-list-inline__item x-breadcrumbs__list__item u-block u-inline--l">
						<a class="u-color-black" href="&mvte:cats:link;" title="&mvte:cats:name;">
							<span>&mvte:cats:name;</span>
						</a>
					</li>
				</mvt:if>
			</mvt:foreach>
		</ul>
	</nav>
</div>
<!-- end .x-breadcrumbs--mobile-dropdown -->
​
```

### Desktop

Screenshot of shared Desktop UI

![alt text](../../images/breadcrumbs-desktop.png "Breadcrumbs: Mobile Parent UI")
​
