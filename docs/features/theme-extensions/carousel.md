# Carousel

`Glide.js` is currently used as our default carousel. Please [click here](https://glidejs.com/) for more information on the JS library. This extension can be modified to support or be swapped with any carousel library.

## Installation

Add `import {initCarousels} from 'extensions/carousel';` to your `Global.js` file.
Add `@import "extensions/carousel/index";` to your `Global.scss` file.

### Future Version
Will be moved as an extension in the themes directory for easy access.


### Initialization

Initialize the function using the following call:

```js
import {initCarousels} from 'extensions/carousel'; // Already included in Global.js

initCarousels() // Already included in Global.js
```

The above code will look for any elements with the `js-carousel` class and automatically initiate the carousel.

### Custom Initialization
```js
// Initialize own custom instance
new Carousel( element, parameters );
```

### HTML Structure
```html
<div class="x-carousel u-mb" data-navigation="perslide|pergroup">
	<div class="x-carousel__track">
		<div class="x-carousel__slides">
			<div class="x-carousel__slide">
				Slide
			</div>
			<div class="x-carousel__slide">
				Slide
			</div>
			<div class="x-carousel__slide">
				Slide
			</div>
			<div class="x-carousel__slide">
				Slide
			</div>
		</div>
	</div>
	<div class="x-carousel__arrows">
		<button type="button" class="x-carousel__arrow--prev u-icon-chevron-left"><span class="u-hide-visually">prev</span></button>
		<button type="button" class="x-carousel__arrow--next u-icon-chevron-right"><span class="u-hide-visually">next</span></button>
	</div>
	<div class="x-carousel__bullets" data-component-label="Some Label"></div>
</div>
```

## Options

|	Option					|	Description																		|
| ------------------------- | --------------------------------------------------------------------------------- |
|	type					|	Type of the movement															|
|	startAt					|	Start at specific slide number													|
|	perView					|	A number of visible slides														|
|	focusAt					|	Focus currently active slide at a specified position							|
|	gap						|	A size of the space between slides												|
|	autoplay				|	Change slides after a specified interval										|
|	hoverpause				|	Stop autoplay on mouseover														|
|	keyboard				|	Change slides with keyboard arrows												|
|	bound					|	Stop running perView number of slides from the end								|
|	swipeThreshold			|	Minimal swipe distance needed to change the slide								|
|	dragThreshold			|	Minimal mousedrag distance needed to change the slide							|
|	perTouch				|	A maximum number of slides moved per single swipe or drag						|
|	touchRatio				|	Alternate moving distance ratio of swiping and dragging							|
|	touchAngle				|	Angle required to activate slides moving										|
|	animationDuration		|	Duration of the animation														|
|	rewind					|	Allow looping the slider type													|
|	rewindDuration			|	Duration of the rewinding animation												|
|	animationTimingFunc		|	Easing function for the animation												|
|	direction				|	Moving direction mode															|
|	peek					|	The value of the future viewports which have to be visible in the current view	|
|	breakpoints				|	Collection of options applied at specified media breakpoints					|
|	classes					|	Collection of used HTML classes													|
|	throttle				|	Throttle costly events															|

[See the following page for information and API usage](https://glidejs.com/docs/options/)

## Components and Layouts
[See the following page for the *Components & Layouts* setup](/features/miva-store/ui-library/carousel/)

## Caveats (Glide)

### Center Mode 
The "Center" mode corresponds to the "focusAt" setting in the options for the Glide plugin. To use "Center" mode optimally, consider changing the "Arrow Navigation Direction" to "Per Slide". As we've found that the default "Per Group" direction is not ideal for this mode and has a few issues. Namely:
** Slides per view is less accurate
** If the number of actual slides is equal to the "Number of Slides" setting, you won't be able to view the last slide.

_As of version 0.67, this option is bugged and will not function correctly. This should be resolved in version 0.7 of the framework along with the new option mentioned above for "Arrow Navigation Direction"; which allows the carousel to move forwards and backwards one slide_

### Carousel Navigation
By default, the previous and next buttons will move the carousel at the same amount as the _Number of Slides_ option. This is determined via package JS in `Carousel.js` starting on line 70. As of 0.7, a new component field was added to the carousel component settings in _Components and Layouts_. This new field _Arrow Navigation Direction_ allows you to set the forward and backward movement on a per slide basis. See the [documentation](https://glidejs.com/docs/setup/) for more information on navigation controls. 

### Peek Values
If you plan on using "peek" values, make sure to designate a left *and* right value. Setting only one value will break the carousel.
