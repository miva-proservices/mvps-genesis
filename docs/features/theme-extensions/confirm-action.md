# Confirm Action

Use this extension to prompt the user (via confirmation dialog) if they want to proceed with the action.

It can be used for link-clicks, form-submits, and button-presses, too. For example, this is currently being used on the global header image for checkout (OCST, OSEL, & OPAY) to encourage the user to stay in the checkout process. It is also used within the My Account pages to ensure the user really wants to delete Cards, Address-Books, or Wishlists.

!!! notice
	When using this for form-submits, be sure to [configure the `triggerEvent` and attach the confirmation to the `<form>` tag; not the submit-button](#simple-form). This will ensure that it prompts users when they hit the `Enter` key too.

## Installation

Add `import ConfirmAction from 'extensions/confirm-action';` to any page you want to use this package on.

### Options

| __Option__ 	| __Required?__ | __Default/Type__ 									| __Description__																																		|
|---------------|---------------|---------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| element       | No			| [@string '.js-confirm-action']					| Supports more then one element by using a group selector; ex: `'.a-element, #b-element'`																|
| message  		| No			| [@string 'Are you sure you want to continue?']	| String value that stores the question that will be used in the confirmation prompt. It can be overwritten by the element's `[data-message]` content	|
| triggerEvent 	| No			| [@string 'click']									| String value that represents the url that the user will be redirected to.																				|


## Example Usage

### Simple Link

### JavaScript

```js
import ConfirmAction from 'extensions/confirm-action';

new ConfirmAction();
```

#### HTML

```html
<!-- Example: -->
<a href="&mvte:urls:SFNT:auto;" class="js-confirm-action">Home</a>
```

### Simple Form

### JavaScript

```js
import ConfirmAction from 'extensions/confirm-action';

new ConfirmAction('.js-confirm-submit', {
	triggerEvent: 'submit'
});
```

#### HTML

```html
<form class="js-confirm-submit">
	<!-- Misc. form-fields here -->
	<button>Delete</button>
</form>
```

### Complex Examples

### JavaScript

```js
import ConfirmAction from 'extensions/confirm-action';

new ConfirmAction('.js-confirm-click', {
	message: 'Are you sure you meant to click that?'
});

new ConfirmAction('.js-confirm-submit', {
	message: 'Are you sure you meant to submit that form?',
	triggerEvent: 'submit'
});
```

#### HTML

```html
<!-- Example: -->
<a href="&mvte:urls:SFNT:auto;" class="js-confirm-click">Home</a>
<a href="&mvte:urls:SFNT:auto;" class="js-confirm-click" data-message="You sure about that? (This message will overwrite the default message)">Home</a>

<form class="js-confirm-submit" data-message="Are you sure? (This message will overwrite the default one)">
	<!-- Misc. form-fields here -->
	<button>Delete</button>
</form>
```
