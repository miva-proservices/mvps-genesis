# Dynamic Form Element Attributes

Add `id` and `class` attributes to `input` and `select` elements dynamically created by Miva.

Example:
```xml
<mvt:item name="states" param="Customer_ShipStateSelect" />
```

## Installation

Add `import dynamicFormElementAttributes from 'extensions/dynamic-form-element-attributes';` to your `Global.js` file

## Example Usage

### .mvt

First, setup the container element that the dynamically created form element lives under. The container element must have either `js-mvt-select` or `js-mvt-input` in the `class` attribute, depending on what HTML element will be dynamically created.

To add a `class` attribute to the dynamically created element, add a `data-classlist` attribute to the container element with the desired class list for the value.

To add an `id` attribute to the dynamically created element, add a `data-id` attribute to the container element with the desired id for the value.

Example:

```xml
<div class="js-mvt-select" data-classlist="c-form-select__dropdown c-form-input--large" data-id="ShipStateSelect">
    <div class="c-form-select">
        <mvt:item name="states" param="ShipStateSelect" />
    </div>
</div>
```

### .js

Next, initialize the function using the following call:

```js
import dynamicFormElementAttributes from 'extensions/dynamic-form-element-attributes';

dynamicFormElementAttributes();
```
