# Environment Helpers
A file that contains a list of multiple ways to interact with environment variables.

## Helpers
Here are the current environment helpers that are available.

__Global Variables:__

`window.USER_CAN_TOUCH` [@boolean] User is on a device that supports 'touchstart'

`window.USER_CAN_HOVER` [@boolean] User is on a device that supports 'mouseover'

__Session Storage Keys/Properties:__

`USER_CAN_TOUCH` [@boolean] User is on a device that supports 'touchstart'

`USER_CAN_HOVER` [@boolean] User is on a device that supports 'mouseover'

__CSS Classes:__

`html.js`: User has javascript enabled

`html.no-js`: User does not have javascript enabled

`html.touch`: User is on a device that supports 'touchstart'

`html.hover`: User is on a device that supports 'mouseover'

## Installation

Terminal
```shell
$ pnpm add @mvps-genesis/environment-helpers
```

## Example Usage

Javascript
```js
import "@mvps-genesis/environment-helpers";
```
