# Form Validation

A helper utility for easy front-end form validation, both inline (onBlur events) and on form submit.

For the extensions documentation please visit https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-form-validation/browse or clone the repository and view the `/docs` or `/examples` directory.

## Installation

This comes pre-installed in genesis in `Global.js` with the following function:

```js
async initializeHTMLFormValidator () {

	/**
	 * Initialize Inline Form Validations
	 */

	const forms = document.getElementsByClassName('js-form-validator');

	if (forms.length === 0) {
		return;
	}

	const {HTMLFormValidator} = await import('extensions/form-validation');

	forms.forEach(function (form) {
		form.HTMLFormValidator = new HTMLFormValidator({
			formElement: form,
			wrapperSelector: '.c-form-list__item'
		});
	});
}
```

## Example Usage

HTML
```html
<form id="form" action="post" action="/login.html">
	<input type="text" name="username" placeholder="Username" required>
	<input type="password" name="password" required>
	<button type="submit">Submit</button>
</form>
```

Javascript
```js
// Import the default function from the extension
import { HTMLFormValidator } from 'extensions/form-validation';

// Create a new Validator form instance
let myForm = new HTMLFormValidator({
	formId: 'form'
});
```
