# Messages

This package defines default helper SCSS and JS classes for working with messages.

## Installation

Add `import {GlobalMessageContainer} from 'extensions/messages';` to your `Global.js` file.
Add `@import "extensions/messages/index";` to your `Global.scss` file.

## Example Usage

The Messages extension comes with three different JS classes:

### Message

Returns an instance of the `Message` class. Chain the `render()` method to create and return a HTML message. The `render()` method also calls the `postRender()` method internally, starting the timer for autoclose and attaching event listeners for closeable messages.

#### .js

```js
import { Message } from 'extensions/messages';

// create message instance and return HTML string
let messageHtml = new Message( message [, options] ).render();

// - or create message instance only (can be rendered later)
let message = new Message( message [, options] );

// render a message instance later
message.render();

// create an instance from an existing HTML element
let existingMessage = Message.fromHTML( selectorOrElement );
```

#### .scss

```scss
@import "~extensions/messages/index";
```

---

### MessageContainer

Stores a list of [Message](#message) instances. Optionally inserts rendered messages into an HTML container. The container will attempt to construct [Message](#message) instances from children within the passed HTML container element.

#### .js

```js
import { MessageContainer, Message } from 'extensions/messages';

// the "options" parameter is optional, as well as the "container" key
let messageContainer = new MessageContainer( { container?: selectorOrElement } );

let message = new Message( 'Hello World!' );
messageContainer.add( message );
messageContainer.remove( message );
messageContainer.clear();
```

---

### GlobalMessage

Extended from [Message](#message). Opon instantiation, this class will trigger a global event using the [EventBus](../packages/event-bus.md). The event name is `MVPS:addMessage`.

#### .js

```js
import { GlobalMessage } from 'extensions/messages';

new GlobalMessage( message [, options] );
```

#### .scss

```scss
@import "~extensions/messages/index";
```

---

### GlobalMessageContainer

Extended from [MessageContainer](#messagecontainer). Creates a container that listens for global events dispatched from the [EventBus](../packages/event-bus.md). The event names are:

* `MVPS:addMessage`
* `MVPS:removeMessage`
* `MVPS:clearMessages`

#### .js

```js
import { GlobalMessageContainer } from 'extensions/messages';

new GlobalMessageContainer( selectorOrElement );
```

## Options

### Message / GlobalMessage

```js
// Message and GlobalMessage have the same options
new Message( 'Hell World!', {
	type: 'info', // Determine the styling of the message. Options are: 'info', 'error', 'warning', 'success'.
	closeable: false, // Adds a close button to the message.
	autohide: false, // Hide the message after a delay.
	autohideTime: 5000, // Delay before message will be hidden if `autoclose` is true.
	icon: false, // Pass a glyph name (e.g. 'menu') to create an icon element.
	customClasses: '', // Adds additional classes to the root message element.
	customId: '' // Overrides the default generated ID.
});
```
