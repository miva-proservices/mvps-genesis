# Mini Basket

TODO

## Installation

Add `import MiniBasket from 'extensions/mini-basket';` to your `Global.js` file.
Add `@import "extensions/mini-basket/index";` to your `Global.scss` file.

## Example Usage

### .js

```js
import MiniBasket from 'extensions/mini-basket';

// the `options` parameter is optional
new MiniBasket( options );
```

### .scss

As of right now, we only have one style of Mini Basket out of the box.

```scss
// import a specific style
@import "extensions/mini-basket/lib/scss/off-canvas";

// ---- FOR TESTING ONLY! ----
// this will import all styles
@import "extensions/mini-basket/index";
```

## Options

```js
new MiniBasket({
	containerElement: '.js-mini-basket', // Selector or Element for the root element
	closeElements: '.js-mini-basket-close', // Selector or Element List of close buttons
	openElements: '.js-mini-basket-open', // Selector or Element List of open buttons
	countElements: '.js-mini-basket-count', // Selector or Element List of basket count elements
	subtotalElements: '.js-mini-basket-subtotal', // Selector or Element List of subtotal elements
	openEvent: 'click', // Event that triggers the "open" state
	closeEvent: 'click', // Event that triggers the "close" state
	isOpenByDefault: false, // Opens the mini basket on instanstiation
	closeOnEsc: true, // Binds a keyup event that closes the mini basket on Escape key
	openOnAddToCart: false, // Listens for the global `MVPS:addedToCart` event and opens if sucessful
	closeAfterOpen: false, // Close mini basket after opened via `openOnAddToCart`
	closeAfterOpenTime: 5000 // Close mini basket after delay if `closeAfterOpen` is true
});
```