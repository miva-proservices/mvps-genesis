# Shipping Estimator

Use this class to retrieve estimated shipping rates data from the Miva SERT page and interact with the estimate shipping form.

## Installation

Add `import ShippingEstimator from 'extensions/shipping-estimator';` to your `BASK.js` file.


## Example Usage

### .mvt

Setup a form that will submit the necessary shipping information to the SERT page. Assuming the default options for the form selector elements, the form must contain the following elements:

* `.js-shipping-estimate-fields` - The form fields container selector.
* `.js-shipping-estimate-results` - The results template container selector.

The form `action` attribute **MUST** also point to the **SERT** page URL.

Example form setup:

```xml
<form name="shipestimate_form" action="&mvte:urls:SERT:auto;">
	<fieldset>
		<legend class="u-hide-visually">Estimate Shipping</legend>
		<div class="js-shipping-estimate-fields c-form-list">
			<div class="c-form-list__item c-form-list__item--full">
				<label class="c-form-label" for="shipestimate_state_select" title="State">State</label>
				<div class="c-form-select">
					<mvt:do file="g.Module_Library_DB" name="l.success" value="StateList_Load_All( l.settings:states )" />
					<select id="shipestimate_state_select" class="c-form-select__dropdown" name="ShipEstimate:ship_state_select">
						<mvt:foreach iterator="state" array="states">
							<mvt:do file="g.Module_Library_Utilities" name="l.success" value="DrawOption( l.settings:state:code, g.Basket:ship_state, l.settings:state:name )" />
						</mvt:foreach>
					</select>
				</div>
			</div>
			<div class="c-form-list__item c-form-list__item--full">
				<label class="c-form-label" for="shipestimate_state">Other State/Province</label>
				<input id="shipestimate_state" class="c-form-input" type="text" name="ShipEstimate:ship_state" value="&mvte:global:ShipEstimate:ship_state;">
			</div>
			<div class="c-form-list__item c-form-list__item--full">
				<label class="c-form-label" for="shipestimate_zip">Zip/Postal Code</label>
				<input id="shipestimate_zip" class="c-form-input" type="text" name="ShipEstimate:ship_zip" value="&mvte:global:ShipEstimate:ship_zip;">
			</div>
			<div class="c-form-list__item c-form-list__item--full">
				<label class="c-form-label" for="shipestimate_cntry" title="Country">Country</label>
				<div class="c-form-select">
					<mvt:comment>
						Assign default/fallback/session-based countries
					</mvt:comment>
					<mvt:if expr="ISNULL g.ShipEstimate:ship_cntry">
						<mvt:if expr="g.Basket:ship_cntry">
							<mvt:assign name="g.ShipEstimate:ship_cntry" value="g.Basket:ship_cntry" />
						<mvt:else>
							<mvt:assign name="g.ShipEstimate:ship_cntry" value="g.Store:country" />
						</mvt:if>
					</mvt:if>

					<mvt:comment>
						Output the Countries
					</mvt:comment>
					<mvt:do file="g.Module_Library_DB" name="l.success" value="StoreCountryList_Load_All( l.settings:countries )" />
					<select id="shipestimate_cntry" class="c-form-select__dropdown" name="ShipEstimate:ship_cntry">
						<mvt:foreach iterator="country" array="countries">
							<mvt:do file="g.Module_Library_Utilities" name="l.success" value="DrawOption( l.settings:country:alpha, g.ShipEstimate:ship_cntry, l.settings:country:name )" />
						</mvt:foreach>
					</select>
				</div>
			</div>
			<div class="c-form-list__item c-form-list__item--full u-text-center">
				<input class="c-button u-cta-style--primary c-button--large" type="submit" value="Calculate" title="Calculate Shipping">
			</div>
		</div>
		<div class="js-shipping-estimate-results"></div>
	</fieldset>
</form>
```

### .js

Initialize the ShippingEstimator object by passing in a form `Element` object as the first argument (**required**):

```js
import ShippingEstimator from 'extensions/shipping-estimator';

const shipEstimateForm = document.forms.shipestimate_form;

if (shipEstimateForm) {
	new ShippingEstimator(shipEstimateForm);
}
```

#### Options

The `ShippingEstimator` object takes an optional object as a second argument that is utilized for overriding any of the default configuration settings. The following table defines the available options:

| Name | Type | Description | Default Value |
| --- | :---: | --- | :---: |
| formFieldsContainer | Element | Overwrite form fields container selector. | `.js-shipping-estimate-fields` |
| formFieldsControlClass | String | Overwrite the class that controls the form fields display. | `u-hidden` |
| formSubmitText | String | Overwrite form submission element text while shipping rates are being calculated. | `Calculating...` |
| recalcTriggerClass | String | Overwrite the Recalculate button class name. | `js-shipping-estimate-recalculate` |
| resultsTemplateContainer | Element | Overwrite shipping rates template container selector. | `.js-shipping-estimate-results` |
| sertApiFieldName | String | Overwrite the SERT API field name. This field name is used on the SERT page to enable the API and return a JSON response. | `sert_api` |
| shipStateSelectElem | Element | Overwrite the form field for "Ship State Select" | `[name="ShipEstimate:ship_state_select"]`
| shipStateElem | Element | Overwrite the form field for "Ship State" | `[name="ShipEstimate:ship_state"]` |
