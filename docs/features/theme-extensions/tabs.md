# Tabs

Tab content display using CSS only.

## Installation

Add `@import "extensions/tabs";` to your `PROD.scss` file.

## Example Usage (General)

Use the following structure inside a container with a class of `x-flex-tabs`:

```html

	<input id="tab-unique-identifier" data-flex-tab type="radio" name="flex-tab"> <!-- Add the check attribute for the default tab -->
	<label class="x-flex-tabs__tab u-text-uppercase" for="tab-unique-identifier">Tab Name</label>
	<div class="x-flex-tabs__content">
		Tab content
	</div>

```

Full Example

```html
<div class="x-flex-tabs o-layout--justify-center">
	
	<input id="tab-tab1" data-flex-tab type="radio" name="flex-tab" checked>
	<label class="x-flex-tabs__tab u-text-uppercase" for="tab-tab1">Tab 1</label>
	<div class="x-flex-tabs__content">
		Your content
	</div>

	<input id="tab-tab2" data-flex-tab type="radio" name="flex-tab">
	<label class="x-flex-tabs__tab u-text-uppercase" for="tab-tab2">Tab 2</label>
	<div class="x-flex-tabs__content">
		Your Content
	</div>

</div>
```



## Usage (Product Display)

### Overview
The template code for the product display's tabbed content can be found in the `product_tabs` content section. The content section has code that will iterate over a customfield group and it's assigned customfields to generate the tab display on the product page.

### Requirements 

* Create a custom product field for "Tab Groups" with a code of `tab_groups`
* Assign the customfield to the product display layout template

> :boom:
The customfield mentioned above needs to be created and assigned to the product display layout for the tabs display to function correctly on the product page template.

---

### How it works

* Create customfield groups in the admin that will represent your tab content. Make note of the code you give and the name as this will be used in the template.
* Create product customfields that will represent your tabbed content and assign it to the appropriate customfield group
* The user will then need to add values to these customfields for the tabs to show on the product display layout.
* If you would like to determine the tabbed content at the product level, enter a comma separated list of customfield group codes using the `tab_groups` customfield in the product's admin settings.
* If you would like to display a default set of tabbed content, navigate to the `product_tabs` content section and define the customfield groups to display via comma separated list for the `l.settings:tabs` mvtassign declaration.


