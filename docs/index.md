<img src="images/logo.png" alt="Genesis Logo">

## Quick Links

- [Ready to use genesis?](develop/index.md)
- [Want to help improve genesis?](contribute/index.md)
- [Recent updates & roadmap](changelog.md)
- [Questions, comments, found an issue, or need help?](support.md)

## Vision

Develop sites faster, efficiently, and enjoyably.

- Make Genesis optimized from the start
	- Specifically in the areas of accessibility, semantics, page speed, ecommerce best-practices, and our [code standards](standards.md)
	- Incorporate changes that typically needed to be done at the start of each project
	- Include advanced features that typically need to get added to customer sites (Components & Layouts, CSS & JS minification, etc.)
- Make Genesis more developer friendly
	- Structure it more like a framework than a theme
	- Use more modern tools & technologies
	- Apply feedback and requests from developers
- Have good documentation
	- Improve the documentation of the whole & individual parts of the framework
	- Provide a more code-sample & real-world use-cases of the parts of the framework
	- Include a pattern-library that works well with our design-system to streamline builds
- Make Genesis dynamic and cohesive
	- Reduce or refactor duplicate & long-hand code
	- Leverage more variables (SASS, page-layouts, etc.)
	- Move parts of the framework to npm packages to encourage code-reuse & ease release distribution

## What is Genesis?

Genesis is a custom Miva framework being built by the Professional Services Web Development Team as an internal open source project. All web developers will have an opportunity to contribute to this framework. Genesis is intended for exclusive use by our team to accelerate new site build projects. Think of Genesis as a highly streamlined version of Shadows with a few Colossus features built into it (e.g. mega menu).

### Key Genesis features include:

- **Accelerated development time** - a baseline, no customizations site build can be delivered in about 100 development hours.
- **Site speed optimizations** - high Google site speed is built into the framework.
- **Accessibility optimizations** - sites built on Genesis will eventually be WCAG 2.1 AA compatible.
- **Streamlined DOM** - HTML markup will be streamlined and more semantic to reduce page load times and increase accessibility.
- **Webpack baked in** - modern front-end automation tools help speed up development and implementation processes.
- **Components and Layouts** - C&L comes built into the storefront and custom pages templates by default.
- **UI Component library** - a continually growing set of UI components will be available to be integrated and configured for our clients' data models.
- **Comprehensive design system** - the framework is pre-built to plug in design deliverables (colors, fonts, images, icons, etc...) for rapid baseline development.
- **Pattern Library** - based on atomic design principles, the built-in pattern library displays a live preview of the design system deliverables as implemented.
- **Redis** - caching will be enabled by default. You will need to add the following page to be excluded for redis caching: `wp` (if utilized)
- **Automated testing** - test scripts will be created to shorten and standardize our QA process.
- **Full documentation** - training and usage documentation will be maintained and updated for both clients and internal developers.

### What Genesis is not

Here are a few things Genesis is not:

- Genesis is not a "plug-and-play" or "cut-and-paste" solution. Configuration for each client is needed (at minimum).
- Genesis is not going to be made available to the public. It is tailored specifically for our Professional Services design and development process.
- Genesis is not pre-configured for third-party integrations. Integration efforts and timelines are not reduced with Genesis.
- Genesis is not available for framework paste projects. All Genesis builds are considered new site builds.

### Even More Information

Interested in learning more about Genesis, or eager to see it in action? Check out the following resources:

- [Genesis test site](https://genesis.mivamerchantdev.com/) (under active development)
- [Genesis test site pattern library](https://genesis.mivamerchantdev.com/pattern-library.html?show=kitchen-sink) (under active development)
- [Genesis Jira epic](https://design.mivamerchant.net/browse/PS-8924)
- [Genesis full release schedule](https://design.mivamerchant.net/projects/PS?selectedItem=com.atlassian.jira.jira-projects-plugin:release-page&status=no-filter)
- [Genesis Documentation](https://genesis.mivamerchantdev.com/docs/)
