## Installation

Use the following steps to add Genesis to onto a Miva Store:

1. Install the latest version of the [Components & Layouts module](https://github.com/tessguefen/Components-Layouts/releases)
2. Install the latest corresponding version of the [mvps-template-export module](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-template-export/browse)
3. Install the Genesis framework-pkg onto your store
	- For the latest stable version, go to [the Changelog page, find the "Downloads" section of your desired release](changelog.md#released)
	- For the latest development version, [get it from the `develop` branch in BitBucket](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/generator-quickstart/app/templates/miva-server/%25WEB_ROOT%25/%25MIVA_ROOT%25/frameworks/%25STORE_ID%25)
4. Import download & install the framework-pkg-file along with the Components & Layouts XML provision files, then import them into your store.
5. Consider changing the Store's currency-module to the [Generic Currency Conversion module & settings described here](features/miva-store/currency-formatting.md)
6. Consider changing the Store's URI Management to [URI Management Settings depicted here](features/miva-store/uri-management.md)
7. Set the `Store's > Store Settings > Character Set` to `utf-8`
8. Optionally, (but recommended) install the [latest version of the Redis module](https://software.dev.miva.com/production/Redis%20Page%20Cache/)
	- Be-sure that the following pages are in the "Excluded Screens" list: `BASK,CACT,CTUS,INVC,MNTN,SERT,wp`
	- If you plan on creating a C&L form (typically this is done on the CTUS page), then you will need to be sure those pages are excluded as well. Cached Redis pages will not be able to send the notification email to the store-owner.
	- [More information on the Redis Page Cache Module](http://blog.merchant.local/?p=6026)
