# Issue Template

[You can create issues in Jira by cloning this Issue](https://design.mivamerchant.net/browse/PS-15374), or by copying and pasting the following template and filling out each section.

## Expected Behavior

For bugs, tell us what you expect to happen

For issues, tell us how it should work

## Current Behavior

For bugs, describe the problem that is happening

For features, explain the difference from current behavior

## Attempted Fixes

What have you tried to resolve this issue? What have you done to mitigate this problem within your own application?

## Steps to Reproduce (for bugs)

Provide a link to a live example or a set of steps to reproduce this bug.
Include code to reproduce, if relevant

## Context

How has this issue affected you? Why is this change required? What problem does it solve?

## Your Environment

Include as many relevant details about the environment you experienced the bug in

* Device (ex. iPhone 11 Pro)
* Browser (ex. IE 11)
* Operating System (ex. Windows 10)
