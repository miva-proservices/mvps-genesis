# Prerequisites

## Development Environment

### Developers

- Install [Git](https://git-scm.com/downloads) & [Git LFS](https://git-lfs.github.com/)
- Install [Node](https://nodejs.org/en/download/)
	- Consider using `nvm` instead:
		- [Mac](https://github.com/nvm-sh/nvm)
		- [Windows](https://github.com/coreybutler/nvm-windows)
- Install [pnpm](https://pnpm.js.org/en/installation)
	- Use the following command to install `pnpm` via `npx`:
		- `npx pnpm add -g pnpm`
- Install [yeoman](https://yeoman.io/)
	- `pnpm add -g yo`
- Install our Genesis framework generator
	- `pnpm add -g @mvps-genesis/generator-quickstart`
- [Configure your linters](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-configs/browse)

### Contributors

If you're going to be contributing to Genesis & writing documentation:

- Install mkdocs' dependencies
	- [Install Python](https://www.python.org/)
	- [Install pip](http://pip.readthedocs.io/en/stable/installing/)
- Install mkdocs & extensions
	- `pip install mkdocs mkdocs-material pygments pymdown-extensions pymdown-typehint`
	- View their docs for more info:
		- [mkdocs](https://www.mkdocs.org/#installation)
		- [mkdocs-material](https://squidfunk.github.io/mkdocs-material/)
		- [pygments](http://pygments.org/)
		- [pygments-extensions](https://squidfunk.github.io/mkdocs-material/extensions/pymdown/)
		- [pymdown-typehint](https://pypi.org/project/pymdown-typehint/)
- [Optionally, download and install PuttyGen](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
	- *Note: This is only needed for [publishing features/releases to `genesis.mivareadythemes.com`](contribute/stores.md#main-genesis-mivareadythemes-com)*

## Development Experience

- Using the Framework
	- [ES6](http://es6-features.org/)
	- [pnpm](https://pnpm.js.org/en/motivation)
	- [Sass](https://sass-lang.com/)
- Contributing to the Framework
	- Review the [standards](standards.md)
	- Review the [changelog](changelog.md)
	- [Babel](https://babeljs.io/)
	- [BrowserSync](https://browsersync.io/)
	- [Node.js](https://nodejs.org/en/about/)
	- [PostCSS](https://postcss.org/)
	- [Webpack](https://webpack.js.org/)
- Coming Soon?
	- [TypeScript](https://www.typescriptlang.org/)
	- [Vue.js](https://vuejs.org/)

## Miva Store

- Miva Merchant > 9.0 CSSUI
- MivaScript Engine v5.33
- Database API: mysql
