# Code Standards

## Git

- Ensure you're commiting changes with the Jira Task ID in your commit msg. You can use the [`prepare-commit-msg`](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/.githooks/prepare-commit-msg?at=refs%2Fheads%2Fdevelop) git hook to do this automatically by either:
	- Running the `pnpm` Script:
		- `$ pnpm run init:githooks`
	- Manually copying the file:
		- [Copy the `prepare-commit-msg` file](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-genesis/browse/.githooks/prepare-commit-msg?at=refs%2Fheads%2Fdevelop) into your repo's `.git/hooks/` folder
		- Ensure the file has execute-permissions: `$ chmod +x .git/hooks/prepare-commit-msg`

## Packages

- The `generator-quickstart/app/templates/` Directory structure should mirror the Miva server. The themes folder will sit relative to templates folder but should NOT be uploaded to the server.

## PageSpeed

Goals:

- TTFB <= 500ms
- TTI <= 3sec
- PageSpeed score > 90

## Code Standards

- [Adhere to linters](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-configs/browse)

## Browser Support

- Babel/PostCSS
	-
	```
	$ npx browserslist "defaults"

	# Result as of 2021-01-29:
	#	and_chr 87
	#	and_ff 83
	#	and_qq 10.4
	#	and_uc 12.12
	#	android 81
	#	baidu 7.12
	#	chrome 87
	#	chrome 86
	#	chrome 85
	#	edge 87
	#	edge 86
	#	firefox 83
	#	firefox 82
	#	firefox 78
	#	ie 11
	#	ios_saf 14.0-14.2
	#	ios_saf 13.4-13.7
	#	ios_saf 12.2-12.4
	#	kaios 2.5
	#	op_mini all
	#	op_mob 59
	#	opera 72
	#	opera 71
	#	safari 14
	#	safari 13.1
	#	samsung 13.0
	#	samsung 12.0
	```
	<!-- <https://browserl.ist/?q=defaults> -->
- Manual QA List
	```
	$ npx browserslist "cover 90% in US and last 1 version"

	# Result as of 2021-01-29:
	# 	and_chr 87
	# 	chrome 87
	# 	edge 87
	# 	ie 11
	# 	ios_saf 14.0-14.2
	# 	safari 14
	```
	<!-- - <https://browserl.ist/?q=cover+90%25+in+US+and+last+1+version> -->

### A note about Internet Explorer

Unfortunately, if we want to [support 95% of the browsers that our client's customers are using](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-browserslist-ga/browse/results/2019/.browserslist--95.0-coverage), then we should continue to support IE 11. We would have to lower our coverage [to 60% in order for us to stop supporting IE 11 and that would not create a solid product for our customers](https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-browserslist-ga/browse/results/2019/.browserslist--60.0-coverage)

IE 11 support should be as functional as possible, but it may be up to each client to improve functionality below IE 11.

## [Testing](https://design.mivamerchant.net/browse/PS-9259)

- Manual Tests
	- Per Issue & Minor Release
		- Personal Unit Tests
		- Pod/Peer/Team Unit Tests
		- Automated Tests *(coming soon)*
	- Per Major Release
		- Integration tests
		- Cross browser tests
		- Bryson QA

## Accessibility

- Passing lighthouse audits by in versions 0.1.0 to 0.5.0
- WCAG 2.1 AA by version 1.0.0

## Developer Workflow

[Uses our implementation of GitFlow](http://blog.merchant.local/?p=5321)

## JavaScript Strategies

- [JS Modules](https://design.mivamerchant.net/browse/PS-9257)
	- [ES6 Modules](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules)
	- If you're writing a JavaScript module-file that returns a class, then name it in the PascalCase to match the name of the class it contains. Otherwise, if you're writing a file that returns a function, object, or set of variables, then name it with camelCase. [See the Naming Conventions](#naming-conventions) for more examples
- [jQuery/JS-Library Usage](https://design.mivamerchant.net/browse/PS-9256)
	- Prefer Vanilla JS; except when it makes sense to leverage a library.
	- Eventually, we will be removing jquery from global webpack provide, and choose to import it per extension
	- Avoid using jquery in core files, but allow developer to add jquery or other libraries in theme files
	- The framework wont start out leveraging other libraries (jQuery, AngularJS, React, Vue, etc.), but future extensions might add them as need be
		- [VueJS](https://design.mivamerchant.net/browse/PS-9258)
- [AJAX](https://design.mivamerchant.net/browse/PS-9298)
	- Lean on vanilla JS ([fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)) as much as we can, especially for the core files.
	- Avoid having to rely on jQuery as a dependency
- [DOM Selection](https://design.mivamerchant.net/browse/PS-9299)
	- Core framework JS files will use vanilla JS selectors to eliminate the need for a dependency in the core framework.
	- Use "js-" prefixed selectors & vanilla JS (ex. `document.getElementsByClassName('js-foobar')`) as opposed to jQuery Selectors (`$('.js-foobar')`) and Hook selectors (`$.hook('foobar')`/`<span data-hook="foobar">`)
	- Also instead of just using `document.getElementsByClassName`, consider using a more-specific `document.getElementById('js-your-container').getElementsByClassName('js-your-elements')` to increase the performance.

## [CSS Strategies](https://design.mivamerchant.net/browse/PS-9253)

- [BEMIT](https://csswizardry.com/2015/08/bemit-taking-the-bem-naming-convention-a-step-further/)
- [BEMIT Cheatsheet](https://gist.github.com/stephenway/a6145d9b4430e8c55a77)
- [Grid Systems](https://design.mivamerchant.net/browse/PS-9254)

### Further Reading

- [CSS Architecture For Design Systems](https://bradfrost.com/blog/post/css-architecture-for-design-systems/)
- [CSS Guidelines](https://cssguidelin.es)
- [SASS Guidelines](https://sass-guidelin.es/)
- [sass-lang.com Code Styleguide](https://sass-lang.com/styleguide/code)
- [Quick Tip Name Your Sass Variables Modularly](https://webdesign.tutsplus.com/articles/quick-tip-name-your-sass-variables-modularly--webdesign-13364)

## [Resource Group Usage](https://design.mivamerchant.net/browse/PS-9281)

- Add an override CSS & JS file to the Miva admin's Resources for the store-owner to use. Turn it off by default. Don't store/commit these files in the Repo
- Add the webpack distribution files (main.js & main.css) to the Resource Groups as well so that it outputs the cache-buster (ex. `/main.js?T=5a4bec32`)
- Setup the mivaJS & dataLayer JS Resource group

## Naming Conventions

The follow examples illustrate how "Lorem's Ipsum Dalor for ACME!" would be output in each scenario.

## Pascal Case

**Example: LoremsIpsumDalorForAcme**

- JavaScript class files

## Camel Case

**Example: loremsIpsumDalorForAcme**

- JavaScript files, functions, & variable names.

## Kebab Case

**Example: lorems-ipsum-dalor-for-acme**

- Markdown Files
- CSS classes, file-names
- npm packages
- Directories
- Repos

## Snake Case

**Example: lorems_ipsum_dalor_for_acme**

- Miva Variables
- HTML Input names (ex. `<input name="foo_bar">`)
- SASS variables names

## Constant (Snake Case & All Caps)

**Example: LOREMS_IPSUM_DALOR_FOR_ACME**

- Constants

## BEMIT

- CSS classes
