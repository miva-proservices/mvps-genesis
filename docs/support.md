# Support

- Do you need help getting started?
- Do you have a question about the project?
- Are you running into an issue?

If so, please reach out to the [lead contributors](contribute/contributors.md#leads) and we'll help you out.
