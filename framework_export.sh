# Script to build a framework_export.mvt file
# which helps build a framework-pkg file
# with all of the files from out webpack build

cd generator-quickstart/app/templates

echo "<mvt:comment>The following links are used to determine which files get included in the framework export."

echo "<link href=\"404.html\">"
echo "<link href=\"robots.txt\">"

# Find all non-hidden-files in the public directory
# hidden files do not seem to be added to pkg files by Miva's framework export process

find -L themes/genesis/public -name '[!.]*'|while read fname; do
  echo "<link href=\"$fname\">"
done

echo "</mvt:comment>"
