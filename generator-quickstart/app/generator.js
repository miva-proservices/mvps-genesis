import Generator from 'yeoman-generator';
import rename from 'gulp-rename';
import ParameterValidator from '@mvps-genesis/parameter-validator';
import theme from '../package.json';
import defaultThemeSettings from './default-theme-settings.json';

const padStoreId = (storeId = 1) => {
	return String(storeId).padStart(8, '0');
};

export default class MVPSGenesis extends Generator {

	constructor (args, opts) {

		super(args, opts);

		this.props = {
			THEME_VERSION: theme.version
		};

		this.validator = new ParameterValidator(this, {
			returnValue: false,
			throwError: false
		});

	}

	// eslint-disable-next-line max-lines-per-function
	async prompting () {

		const answers = await this.prompt([
			{
				default: 'httpdocs',
				message: 'What is your public web root?',
				name: 'WEB_ROOT',
				type: 'input'
			},
			{
				default: defaultThemeSettings.MIVA_ROOT,
				message: 'What is your miva root?',
				name: 'MIVA_ROOT',
				type: 'input'
			},
			{
				default: padStoreId(1),
				filter: padStoreId,
				message: 'What is your store id?',
				name: 'STORE_ID',
				transformer: padStoreId,
				type: 'input'
			},
			{
				default: 'MVPS',
				message: 'What is the client\'s name?',
				name: 'CLIENT_NAME',
				type: 'input'
			},
			{
				default: '0.1.0',
				message: 'What is the current version of the client\'s site?',
				name: 'CLIENT_VERSION',
				type: 'input'
			},
			{
				default: 'design@miva.com',
				message: 'What is the developer or pod name?',
				name: 'POD_OR_DEV',
				type: 'input'
			},
			{
				default: defaultThemeSettings.LIVE_STORE_URL,
				message: 'What is your live store url?',
				name: 'LIVE_STORE_URL',
				type: 'input',
				validate: (input) => this.validator.isUrl('LIVE_STORE_URL', input, {
					returnValue: false,
					throwError: false
				})
			},
			{
				default: defaultThemeSettings.STAGING_STORE_URL,
				message: 'What is your staging store url?',
				name: 'STAGING_STORE_URL',
				type: 'input',
				validate: (input) => this.validator.isUrl('STAGING_STORE_URL', input, {
					returnValue: false,
					throwError: false
				})
			},
			{
				default: defaultThemeSettings.DEV_STORE_URL,
				message: 'What is your dev store url?',
				name: 'DEV_STORE_URL',
				type: 'input',
				validate: (input) => this.validator.isUrl('DEV_STORE_URL', input, {
					returnValue: false,
					throwError: false
				})
			}
		]);

		// Merge answers into data
		this.props = {...this.props,
			...answers};

	}

	writing () {

		this.registerTransformStream(rename(path => {

			path.dirname = path.dirname.replace(/\%([^%]+)\%/g, ($0, $1) => {
				return this.props[$1];
			});

			path.basename = path.basename.replace(/\%([^%]+)\%/, () => {
				return this.props[arguments[1]];
			});

			return path;

		}));

		this.fs.copyTpl(
			[
				this.templatePath('**'),
				'!**/dist',
				'!**/node_modules',
				'!**/theme-settings.json'
			],
			this.destinationPath('.'),
			this.props,
			{},
			{
				globOptions: {
					dot: true
				}
			}
		);

	}

};
