# <%= CLIENT_NAME %>


## Project Information

**Pod/Developer(s)**: <%= POD_OR_DEV %>

**Store URL**: <%= LIVE_STORE_URL %>

**Genesis Version**: <%= THEME_VERSION %>

## Getting Started

1. Upload the [genesis.pkg](./miva-server/<%= WEB_ROOT %>/<%= MIVA_ROOT %>/frameworks/<%= STORE_ID %>/genesis-v<%= THEME_VERSION %>.pkg) framework file to your Miva store.

2. Create repository for the client via [BitBucket](https://bitbucket.mivamerchant.net/) or clone if it exists already.

3. Ensure that LFS is turned on in the BitBucket repo settings.

4. Run `pnpm run init` within the root folder.

5. Open terminal and `cd` into the `themes/genesis` folder.
	* Run `pnpm run start` to start localhost to your dev domain.