/* eslint-disable array-element-newline, no-console */
const chalk = require('chalk');
const program = require('commander');
const {spawn, execSync} = require('child_process');
const {readFileSync, existsSync} = require('fs');
const {sep} = require('path');

const logError = chalk.bold.red;
const logWarn = chalk.keyword('orange');
const logSuccess = chalk.green;

const getThemeSettings = () => {
	const themeSettings = 'theme-settings.json';
	const defaultThemeSettingsPath = `..${sep}..${sep}..${sep}default-theme-settings.json`;

	console.log(logSuccess('Getting theme settings...'));

	if (!existsSync(themeSettings)) {
		console.log(logWarn('Unable to find theme-settings.json. Creating one...'));
		if (existsSync(defaultThemeSettingsPath)) {
			console.log(logSuccess('Applying default settings...'));
			execSync(`pnpx ejs theme-settings.example.json -f ${defaultThemeSettingsPath} -o theme-settings.json`);
		}
		else {
			console.log(logSuccess('Copying from example...'));
			execSync('cp theme-settings.example.json theme-settings.json');
		}
	}

	console.log(logSuccess('√ Done'));

	return JSON.parse(readFileSync('theme-settings.json'));
};

const envExists = (currentEnv, themeSettings) => {
	return Object.keys(themeSettings.environments).some(env => env === currentEnv);
};

program
	.command('local [env]')
	.option('-l, --launch', 'Uses the "-launch" public path instead.')
	.description('Run local development process.', {
		env: 'The target environment to use (default: dev). Defined in `theme-settings.json`.'
	})
	.action((argEnv, opts) => {
		// Define default environment
		const env = argEnv ? argEnv : 'dev';

		// Get theme settings
		const themeSettings = getThemeSettings();

		// Check if env exists
		if (!envExists(env, themeSettings)) {
			console.error(logError(`Error: Environment "${env}" does not exist in "theme-settings.json".`));
			return;
		}

		// Create child process with handled args/opts
		spawn(
			'pnpx webpack',
			[
				...opts.l
					? [
						'--env',
						'LAUNCH=true'
					]
					: [],
				'--env',
				'NODE_ENV=development',
				'--env',
				`MIVA_ENV=${env}`,
				'--config',
				'webpack.local.js'
			],
			{
				shell: true,
				stdio: 'inherit'
			}
		);
	});

program
	.command('build [env]')
	.option('-l, --launch', 'Uses the "-launch" public path instead.')
	.description('Run production build process.', {
		env: 'The target environment to use (default: live). Defined in `theme-settings.json`.'
	})
	.action((argEnv, opts) => {
		// Define default environment
		const env = argEnv ? argEnv : 'live';

		// Get theme settings
		const themeSettings = getThemeSettings();

		// Check if env exists
		if (!envExists(env, themeSettings)) {
			console.error(logError(`Error: Environment "${env}" does not exist in "theme-settings.json".`));
			return;
		}

		// Create child process with handled args/opts
		spawn(
			'pnpx webpack',
			[
				...opts.l
					? [
						'--env',
						'LAUNCH=true'
					]
					: [],
				'--env',
				'NODE_ENV=production',
				'--env',
				`MIVA_ENV=${env}`,
				'--config',
				'webpack.prod.js'
			],
			{
				shell: true,
				stdio: 'inherit'
			}
		);
	});

program.parse(process.argv);
