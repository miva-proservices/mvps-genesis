/* eslint-disable max-lines-per-function */
import ParameterValidator from '@mvps-genesis/parameter-validator';
import EventBus from '@mvps-genesis/event-bus';
import {MessageContainer, Message} from 'extensions/messages';

export default class AjaxAddToCart {

	// Define "private" properties
	_validator;

	// Define "public" properties
	submitButtonTextProperty;

	submitButtonText;

	constructor (
		formElement,
		{
			submitButtonElement = '.js-add-to-cart',
			actionInputElement = 'input[name="Action"]',
			responseMessageElement = '.js-add-to-cart-message',
			processingButtonText = 'Processing...',
			stockMessageErrorSelector = '.x-messages--error',
			successMessage = {
				message: 'Added to cart.',
				options: {
					autohide: true,
					type: 'success'
				}
			},
			missingAttributesMessage = {
				message: 'All <em class="u-color-error">Required</em> options have not been selected.<br />Please review the following options: <span class="u-color-error">%missingAttributes%</span>',
				options: {
					autohide: true,
					type: 'warning'
				}
			},
			limitedStockMessage = {
				dynamic: true,
				message: 'We do not have enough of the combination you have selected.<br />Please adjust your quantity.',
				options: {
					autohide: true,
					type: 'warning'
				}
			},
			outOfStockMessage = {
				dynamic: true,
				message: 'The combination you have selected is out of stock.<br />Please review your options or check back later.',
				options: {
					autohide: true,
					type: 'warning'
				}
			},
			otherMessage = {
				message: 'Please review your selection.',
				options: {
					autohide: true,
					type: 'warning'
				}
			}
		} = {}
	) {

		// Create validator
		this._validator = new ParameterValidator(this);

		// Set properties from options
		this.formElement = this._validator.isElement('formElement', formElement);
		this.submitButtonElement = this._validator.isElement('options.submitButtonElement', submitButtonElement, {container: this.formElement});
		this.actionInputElement = this._validator.isElement('options.actionInputElement', actionInputElement, {container: this.formElement});
		this.processingButtonText = this._validator.isString('options.processingButtonText', processingButtonText);
		this.successMessage = this._validator.isObject('options.successMessage', successMessage);
		this.stockMessageErrorSelector = this._validator.isString('options.stockMessageErrorSelector', stockMessageErrorSelector);
		this.missingAttributesMessage = this._validator.isObject('options.missingAttributesMessage', missingAttributesMessage);
		this.limitedStockMessage = this._validator.isObject('options.limitedStockMessage', limitedStockMessage);
		this.outOfStockMessage = this._validator.isObject('options.outOfStockMessage', outOfStockMessage);
		this.otherMessage = this._validator.isObject('options.otherMessage', otherMessage);

		// Create message container from option
		this.messageContainer = new MessageContainer({container: this._validator.isElement('options.responseMessageElement', responseMessageElement, {container: this.formElement})});

		// Set cached / computed values
		this.submitButtonTextProperty = this.submitButtonElement instanceof HTMLButtonElement ? 'textContent' : 'value';
		this.submitButtonText = this.submitButtonElement[this.submitButtonTextProperty];

		// Run bootstrap functions
		this.bindEvents();

	}

	bindEvents () {

		this.formElement.addEventListener('submit', this.onSubmit.bind(this), false);

	}

	onSubmit (e) {

		if (this.actionInputElement.value !== 'ADPR') {
			return;
		}

		e.preventDefault();
		e.stopImmediatePropagation();

		// Set form action / input action
		this.formElement.action = this.submitButtonElement.dataset.action;
		this.actionInputElement.value = 'ADPR';

		const formData = new FormData(this.formElement);
		const request = new XMLHttpRequest();

		// Set idle status
		this.formElement.dataset.status = 'idle';

		if (this.formElement.dataset.status !== 'processing') {

			// Set status while processing
			this.formElement.dataset.status = 'processing';

			// Disable button while processing
			this.submitButtonElement.disabled = true;

			// Set text of button to processing
			this.submitButtonElement[this.submitButtonTextProperty] = this.processingButtonText;

			// Clear response message
			this.messageContainer.clear();

			// Handle readystatechange callback
			request.onreadystatechange = () => {

				// Only run if the request is complete
				if (request.readyState !== 4) {
					return;
				}

				// Define event payload structure
				let payload = {
					data: request.response,
					formData: formData,
					// To be updated later in the switch statement
					message: null,
					request: request,
					success: false
				};

				// Process our return data
				if (request.status === 200) {

					// What do when the request is successful
					const pageCodeResponseHeader = request.getResponseHeader('x-mm-page-code');

					// Reset button text
					this.submitButtonElement.disabled = false;
					this.submitButtonElement[this.submitButtonTextProperty] = this.submitButtonText;

					switch (pageCodeResponseHeader) {

					case 'BASK': {

						this.onSuccess();

						payload.success = true;

						break;
					}
					case 'PATR':
						this.onMissingAttributes();
						break;
					case 'PROD':
						this.onMissingAttributes();
						break;
					case 'PLMT':
						this.onLimitedStock(payload);
						break;
					case 'POUT':
						this.onOutOfStock(payload);
						break;
					default:
						this.onOther();
						break;

					}

					// Set payload message
					payload.message = this.messageContainer.currentMessage.message;

				}
				else {

					let errorMessage = `${ this.constructor.name } - The request failed with a status of: ${ request.status }`;

					// What do when the request fails
					payload.message = errorMessage;

				}

				// Dispatch global event
				EventBus.dispatchEvent('MVPS:addToCart', payload);

				// Reset form status
				this.formElement.dataset.status = 'idle';

			};


			/**
			 * Create and send a request
			 * The first argument is the post type (GET, POST, PUT, DELETE, etc.)
			 * The second argument is the endpoint URL
			 */
			request.open(this.formElement.method, this.formElement.action, true);
			request.responseType = 'document';
			request.send(formData);

		}

	}

	// Message breaks
	onSuccess () {

		this.messageContainer.add(new Message(this.successMessage.message, this.successMessage.options));

		// Re-initialize Attribute Machine (if it is active)
		if (typeof attrMachCall !== 'undefined') {
			// eslint-disable-next-line new-cap
			window.attrMachCall.Initialize();
		}

	}

	onMissingAttributes () {

		const findRequired = this.formElement.getElementsByClassName('is-required');
		const missingAttributes = [];

		for (let id = 0; id < findRequired.length; id++) {
			missingAttributes.push(' ' + findRequired[id].title);
		}

		this.messageContainer.add(new Message(this.missingAttributesMessage.message.replace('%missingAttributes%', missingAttributes), this.missingAttributesMessage.options));

	}

	onLimitedStock (payload) {

		let message = this.limitedStockMessage.message;

		if (typeof payload.data === 'object' && this.limitedStockMessage.dynamic === true) {
			const payloadErrorMessage = this.getStockMessage(payload.data);

			if (payloadErrorMessage !== '') {
				message = payloadErrorMessage;
			}
		}

		this.messageContainer.add(new Message(message, this.limitedStockMessage.options));

	}

	onOutOfStock (payload) {

		let message = this.outOfStockMessage.message;

		if (typeof payload.data === 'object' && this.outOfStockMessage.dynamic === true) {
			const payloadErrorMessage = this.getStockMessage(payload.data);

			if (payloadErrorMessage !== '') {
				message = payloadErrorMessage;
			}
		}

		this.messageContainer.add(new Message(message, this.outOfStockMessage.options));

	}

	onOther () {

		this.messageContainer.add(new Message(this.otherMessage.message, this.otherMessage.options));

	}

	getStockMessage (payloadData) {

		const stockMessageElement = payloadData.querySelector(this.stockMessageErrorSelector);

		if (stockMessageElement) {
			return stockMessageElement.innerHTML;
		}

		return '';

	}

};
