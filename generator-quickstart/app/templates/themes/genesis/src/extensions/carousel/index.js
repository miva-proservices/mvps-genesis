import Carousel from './lib/Carousel';
import initCarousels from './lib/initCarousels';

export {
	Carousel,
	initCarousels
};
