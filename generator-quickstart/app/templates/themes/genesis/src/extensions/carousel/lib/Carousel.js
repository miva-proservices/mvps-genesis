import ParameterValidator from '@mvps-genesis/parameter-validator';
import decodeEntities from '@mvps-genesis/decode-entities';
import Glide from '@glidejs/glide';

export default class Carousel {

	// Default/fallback slider parameters
	defaultParameters = {
		breakpoints: {
			640: {
				gap: 16,
				perView: 2
			},
			960: {
				gap: 16,
				perView: 3
			}
		},
		gap: 8,
		perView: 4
	};

	constructor (container, parameters) {

		// Validator
		this._validator = new ParameterValidator(this);

		// Glide elements to validate
		this.container = this._validator.isElement('container', container);
		this.track = this._validator.isElement('carousel track container', '.x-carousel__track', {container: this.container});
		this.slides = this._validator.isElement('carousel slides container', '.x-carousel__slides', {container: this.container});

		// Optional glide elements
		this.arrows = this._validator.isElement('carousel arrows container', '.x-carousel__arrows', {container: this.container,
			required: false});
		this.bullets = this._validator.isElement('carousel bullets container', '.x-carousel__bullets', {container: this.container,
			required: false});

		// Carousel parameters set (via JS code) vs dataset (via template code). dataset value will take priority
		const options = this.container.dataset.options.length ? JSON.parse(decodeEntities(this.container.dataset.options)) : parameters;

		// Use default parameters if nothing is passed
		this.parameters = options ? options : this.defaultParameters;

		// Attach classes and carousel specific attributes (Glide.js)
		this.build();

		// Initialize slider
		this.init();

	}

	build () {

		this.container.classList.add('glide');

		// Build default elements
		if (this.slides) {
			this.buildSlides();
		}
		if (this.track) {
			this.buildTrack();
		}

		// Build optional elements
		if (this.arrows) {
			this.buildArrows();
		}
		if (this.bullets && this.slideCount > 0) {
			this.buildBullets();
		}

	}

	buildArrows () {

		this.arrows.classList.add('glide__arrows');
		this.arrows.dataset.glideEl = 'controls';

		const arrowLeft = this.arrows.querySelector('.x-carousel__arrow--prev');
		const arrowRight = this.arrows.querySelector('.x-carousel__arrow--next');

		if (arrowLeft) {
			arrowLeft.classList.add('glide__arrow', 'glide__arrow--left');
			arrowLeft.dataset.glideDir = '<';
		}

		if (arrowRight) {
			arrowRight.classList.add('glide__arrow', 'glide__arrow--right');
			arrowRight.dataset.glideDir = '>';
		}

	}

	buildBullets () {

		this.bullets.classList.add('glide__bullets');
		this.bullets.dataset.glideEl = 'controls[nav]';

		const bulletsLabel = this.bullets.dataset.componentLabel;

		let bulletCounter = 0;

		while (bulletCounter < this.slideCount) {
			const bulletButton = document.createElement('button');
			const bulletText = document.createElement('span');

			bulletButton.classList.add('glide__bullet');
			bulletButton.dataset.glideDir = '=' + bulletCounter;

			if (bulletsLabel.length) {
				bulletText.classList.add('u-hide-visually');
				bulletText.textContent = bulletsLabel + ' Slide ' + (bulletCounter + 1);
				bulletButton.appendChild(bulletText);
			}

			this.bullets.appendChild(bulletButton);

			bulletCounter++;
		}

	}

	buildSlides () {

		this.slides.classList.add('glide__slides');

		this.slideElements = this.slides.getElementsByClassName('x-carousel__slide');
		this.slideCount = this.slideElements.length;

		for (let slideElement of this.slideElements) {
			slideElement.classList.add('glide__slide');
		}

	}

	buildTrack () {

		this.track.classList.add('glide__track');
		this.track.dataset.glideEl = 'track';

	}

	destroy () {

		if (!this.container.classList.contains('glide--slider') && !this.container.classList.contains('glide--carousel')) {
			return false;
		}

		if (this.slider) {
			this.slider.destroy();
		}

		return true;

	}

	init () {

		this.destroy();

		this.slider = new Glide(this.container, this.parameters).mount();

	}

}
