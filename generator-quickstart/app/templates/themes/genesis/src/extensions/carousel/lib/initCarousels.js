import Carousel from './Carousel';

export default function initCarousels () {

	const carouselElements = document.getElementsByClassName('js-carousel');

	for (let element of carouselElements) {

		if (element.classList.contains('lazyloaded') || !element.classList.contains('lazyload')) {

			new Carousel(element);

		}

	}

	document.addEventListener(
		'lazybeforeunveil',
		(e) => {

			if (e.target.classList.contains('js-carousel')) {

				new Carousel(e.target);

			}

		}
	);

}
