import ParameterValidator from '@mvps-genesis/parameter-validator';

export default class ConfirmAction {

	// Define "public" properties
	element;

	dialog;

	redirectUrl;

	constructor (
		element = '.js-confirm-action',
		{
			message = 'Are you sure you want to continue?',
			triggerEvent = 'click'
		} = {}
	) {

		// Create validator instance
		const validator = new ParameterValidator(this);

		// Validate and set properties
		this.element = Array.from(validator.isElementList('element', element));
		this.message = validator.isString('options.message', message);
		this.triggerEvent = validator.isString('options.triggerEvent', triggerEvent);

		// Bind any events
		this.bindEvents();

	}

	bindEvents () {
		this.element.forEach(element => element.addEventListener(this.triggerEvent, this.onEvent.bind(this), false));
	}

	determineMessage (element) {
		let message = typeof element === 'object' && element instanceof HTMLElement && 'message' in element.dataset && element.dataset.message.length ? String(element.dataset.message) : this.message;

		message = message.replace(/\\n/g, String.fromCharCode(10));

		return message;
	}

	onConfirm () {

		return true;

	}

	onCancel (e) {

		e.preventDefault();
		e.stopPropagation();

		return false;

	}

	openDialog (e) {

		let message = this.determineMessage(e.currentTarget);

		// eslint-disable-next-line no-alert
		this.dialog = window.confirm(message);

	}

	onEvent (e) {

		this.openDialog(e);

		if (this.dialog) {

			return this.onConfirm();

		}

		return this.onCancel(e);

	}

}
