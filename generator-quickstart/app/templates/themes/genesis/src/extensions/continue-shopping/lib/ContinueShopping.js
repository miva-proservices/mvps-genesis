export default class ContinueShopping {

	_defaultOptions = {
		links: '.js-continue-shopping',
		screens: [
			'CLST',
			'CTGY',
			'PLST',
			'PROD',
			'SRCH'
		]
	}

	element = null;

	constructor (options = {}) {

		this.options = {...this._defaultOptions,
			...options};

		this.setContinueShoppingLink();

		this.links = this._getElementsFromSelectorOrElementList(this.options.links, 'options.links');

		this.bindEvents();

	}

	_getElementsFromSelectorOrElementList (selectorOrElements) {

		return typeof selectorOrElements === 'string' ? document.querySelectorAll(selectorOrElements) : selectorOrElements;

	}

	bindEvents () {

		if (this.links && this.links.length > 0) {
			for (const link of this.links) {
				link.addEventListener('click', this.onClick.bind(this), false);
			}
		}

	}

	onClick (e) {

		e.preventDefault();

		window.location = sessionStorage.getItem('continue_url');


	}

	setContinueShoppingLink () {

		if (this.options.screens.indexOf(window.mivaJS.Screen) !== -1) {
			sessionStorage.setItem('continue_url', window.mivaJS.Breadcrumbs_Current_Item_Link);
		}

	};

}
