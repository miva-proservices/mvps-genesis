/**
 * Add `id` and `class` attributes to `input` and `select` elements
 * dynamically created by Miva.
 *
 * Example: <mvt:item name="states" param="Customer_ShipStateSelect" />
 */

export default function dynamicFormElementAttributes () {
	const mvtInputContainers = document.getElementsByClassName('js-mvt-input');
	const mvtSelectContainers = document.getElementsByClassName('js-mvt-select');

	if (mvtInputContainers && mvtInputContainers.length) {
		for (const element of mvtInputContainers) {
			const mvtInputs = element.querySelectorAll('input:not([type="hidden"])');

			if (mvtInputs && mvtInputs.length) {
				const classes = element.getAttribute('data-classlist');
				const id = element.getAttribute('data-id');

				for (const input of mvtInputs) {
					input.setAttribute('class', classes);
					input.setAttribute('id', id);
				}
			}
		}
	}

	if (mvtSelectContainers && mvtSelectContainers.length) {
		for (const element of mvtSelectContainers) {
			const mvtSelects = element.querySelectorAll('select');

			if (mvtSelects && mvtSelects.length) {
				const classes = element.getAttribute('data-classlist');
				const id = element.getAttribute('data-id');

				for (const select of mvtSelects) {
					select.setAttribute('class', classes);
					select.setAttribute('id', id);
				}
			}
		}
	}
};
