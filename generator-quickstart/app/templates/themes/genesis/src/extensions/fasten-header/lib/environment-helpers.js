/**
 *    HTML Class Name
 *    This function will check if JavaScript is enabled, detect touch and hover
 *    capabilities, and modify the class list as needed.
 */

export default (function () {
	'use strict';

	const html = document.documentElement;

	html.classList.remove('no-js');
	html.classList.add('js');

	/**
	 * Detect if the user is utilizing a touch interface.
	 */
	window.addEventListener('touchstart', function onFirstTouch () {
		html.classList.add('touch');
		sessionStorage.setItem('USER_CAN_TOUCH', true);
		window.USER_CAN_TOUCH = true;

		window.removeEventListener('touchstart', onFirstTouch, false);
	}, false);

	/**
	 * Detect if the user can hover over elements.
	 */
	window.addEventListener('mouseover', function onFirstHover () {
		html.classList.add('hover');
		sessionStorage.setItem('USER_CAN_HOVER', true);
		window.USER_CAN_HOVER = true;

		window.removeEventListener('mouseover', onFirstHover, false);
	}, false);
}());
