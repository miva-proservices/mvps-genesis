'use strict';

/* jslint plusplus: true, devel: true */
/* eslint-env browser */

/**
 * @fileOverview A helper utility for easy front-end form validation
 * @author <a href="mailto:sterry@miva.com">Shawn Terry</a>
 * @version 1.0
 */

/**
 * Notes for improvements
 * - input[type="radio"][required] works
 * - Pass in options as object to allow for extension and legibility
 * Issues
 * - Checkbox doesn't validate on click/check, consider validating on input/change?
 * - File input shows error imediately after clicking "Choose File" button and trying to do an upload
 *
 */

// We need to include a polyfill for using closest() in IE
if (!Element.prototype.matches) {
	Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
}
if (!Element.prototype.closest) {
	Element.prototype.closest = function (s) {
		var el = this;
		do {
			if (el.matches(s)) { return el; }
			el = el.parentElement || el.parentNode;
		} while (el !== null && el.nodeType === 1);
		return null;
	};
}

/**
 * JSOO constructor pattern for form validation methods. Creates a new instance of class HTMLFormValidator
 * @constructor
 * @param {object} options - An object literal containing the options for instantiating a new HTMLFormValidator
 * @param {string} options.formId - The ID of the form element being validated
 * @param {HTMLElement} options.formElement - The form element being validated, must be a DOM element
 * @param {string} [options.wrapperSelector="li"] - An optional selector used to identify the form field wrapper element, defaults to "li"
 * @param {function} [options.preSubmitRef] - An optional reference to a function to be called after validation, right before the form submittal
 * @param {function} [options.customValidationRef] - An optional reference to a function to be called during form validation, MUST return boolean
 * @param {string} [options.errorClass="u-font-small"] - An optional class to use for styling the error message, defaults to "u-font-small"
 * @param {string} [options.errorLabelClass="has-error"] - An optional class to use for styling the error input, defaults to "has-error"
 * @example
 * // standard implementation
 * var form1Validator = new HTMLFormValidator({formId: 'testFormOne'});
 * @example
 * // full implementation
 * var form2Validator = new HTMLFormValidator({
 *     formElement: document.getElementById('testFormTwo'),
 *     wrapperSelector: '.c-form-list__item',
 *     preSubmitRef: preSubmit,
 *     customValidationRef: customValidation,
 *     errorClass: 'u-font-tiny u-text-italic'
 * });
 */
export default class HTMLFormValidator {
	constructor (options) {
		this.formElement = options.formElement || document.getElementById(options.formId);
		this.wrapperSelector = options.wrapperSelector;
		this.preSubmitRef = options.preSubmitRef;
		this.customValidationRef = options.customValidationRef;
		this.errorClass = options.errorClass;
		this.errorLabelClass = options.errorLabelClass;

		// Validate arguments
		if (!this.formElement || this.formElement instanceof window.HTMLElement === false || this.formElement.tagName !== 'FORM') {
			this.consoleError('Validator requires a valid `formID` or `formElement`');
			return;
		}

		// Object global variables
		this.objGlobals = {};
		this.objGlobals.formElement = this.formElement;
		this.objGlobals.wrapperSelector = this.wrapperSelector || 'li';
		this.objGlobals.validatedElements = this.objGlobals.formElement.querySelectorAll('[required], [pattern]');
		this.objGlobals.preSubmitRef = typeof this.preSubmitRef === 'function' ? this.preSubmitRef : null;
		this.objGlobals.customValidationRef = typeof this.customValidationRef === 'function' ? this.customValidationRef : null;
		this.objGlobals.errorClass = this.errorClass || 'u-font-small';
		this.objGlobals.errorLabelClass = this.errorLabelClass || 'has-error';

		// Map object for default error messages
		this.messageMap = {
			DEFAULT: 'Please enter a value',
			INPUT_CHECKBOX: 'Please check the checkbox',
			INPUT_DATE: 'Please enter a valid date',
			INPUT_EMAIL: 'Please enter a valid email',
			INPUT_FILE: 'Please select a valid file',
			INPUT_NUMBER: 'Please enter a valid number',
			INPUT_PASSWORD: 'Please enter a valid password',
			INPUT_RADIO: 'Please select an option',
			INPUT_RANGE: 'Please select a range',
			INPUT_SEARCH: 'Please enter a value',
			INPUT_TEL: 'Please enter a valid telephone number',
			INPUT_TEXT: 'Please enter a value',
			INPUT_URL: 'Please enter a valid URL',
			SELECT: 'Please select an option',
			TEXTAREA: 'Please enter a value'
		};

		// Main Function
		this.init();
	}

	/**
	 * Utility function for outputting an error message to the console
	 * @param {string} errorMsg - The message you would like sent as a console error
	 */
	consoleError (errorMsg) {
		console.error('ERROR: ' + errorMsg);
	};

	/**
	 * Function for adding novalidate to the form tag, turns off browser generated validation messages
	 */
	disableBrowserValidation () {
		if (!this.objGlobals.formElement.hasAttribute('novalidate')) {
			this.objGlobals.formElement.setAttribute('novalidate', true);
		}
	};

	/**
	 * DOM element creation function for generating an error message
	 * @param {HTMLElement} element - The form field being validated, must be a DOM element
	 * @returns {HTMLElement} A new DOM element containing an error message, to be placed below the form field
	 */
	createInlineErrorContainer (element) {
		var elementType,
			errorElement = document.createElement('div'),
			errorMessage;

		// Validate arguments
		if (!element || element instanceof window.HTMLElement === false) {
			this.consoleError('createInlineErrorContainer requires a valid DOM element');
			return errorElement;
		}

		// Build the message
		if (element.hasAttribute('data-errormessage')) {
			// Custom error message
			errorMessage = element.getAttribute('data-errormessage');
		} else if (element.hasAttribute('title')) {
			// Custom error message from title
			errorMessage = element.getAttribute('title');
		} else {
			// Canned message based on type of element
			elementType = element.tagName;
			if (elementType === 'INPUT') {
				elementType += '_' + element.getAttribute('type').toUpperCase();
			}
			errorMessage = this.messageMap[elementType] || this.messageMap.DEFAULT;
			// Add min and max values to errorMessage
			if (elementType === 'INPUT_NUMBER') {
				let min = element.getAttribute('min'),
					max = element.getAttribute('max');

				if (min) {
					errorMessage += ', minimum: ' + min;
				}
				if (max) {
					errorMessage += ', maximum: ' + max;
				}
			}
		}

		// Add class and message to error element
		errorElement.className = this.objGlobals.errorClass;
		errorElement.textContent = errorMessage;
		errorElement.setAttribute('data-errorhook', true);

		// Return the element to the calling method
		return errorElement;
	};

	/**
	 * Function for displaying or hiding an error message for a validated form field
	 * @param {HTMLElement} element - The form field being validated, must be a DOM element
	 * @param {'show'|'hide'} action - Show or hide the error message
	 */
	toggleError (element, action) {
		var actionVal = action === 'show' || action === 'hide' ? action : null,
			inlineErrorContainer,
			wrapper;
		// Validate arguments
		if (!element || element instanceof window.HTMLElement === false || !action) {
			this.consoleError('toggleError requires a valid DOM element and an action of "show" or "hide"');
			return;
		}
		// Grab needed elements
		wrapper = element.closest(this.objGlobals.wrapperSelector);
		if (!wrapper) {
			this.consoleError('toggleError requires a valid wrapper element');
			return;
		}
		inlineErrorContainer = wrapper.querySelector('[data-errorhook]');
		// Toggle appropriate classes and error messages
		if (actionVal === 'show') {
			wrapper.classList.add(this.objGlobals.errorLabelClass);
			if (inlineErrorContainer) {
				inlineErrorContainer.classList.remove('u-hidden');
			} else {
				// Create an error message if one does not exist yet
				inlineErrorContainer = this.createInlineErrorContainer(element);
				if (inlineErrorContainer.textContent.length !== 0) {
					wrapper.appendChild(inlineErrorContainer);
				}
			}
		} else {
			wrapper.classList.remove(this.objGlobals.errorLabelClass);
			if (inlineErrorContainer) {
				inlineErrorContainer.classList.add('u-hidden');
			}
		}
	};

	/**
	 * Function for validating a single form field
	 * @param {HTMLElement} element - The form field being validated, must be a DOM element
	 * @param {Event} [event] An optional event emitted from the form field
	 * @returns {boolean} Returns true if element is hidden or passes validation, returns false if element fails validation
	 */
	validateFormElement (element, event) {
		// Validate argument
		if (!element || element instanceof window.HTMLElement === false) {
			this.consoleError('validateFormElement requires a valid DOM element');
			return false;
		}

		// Only validate if element is visible
		if (element.offsetParent === null) {
			return true;
		}
		// Show or hide error based on validity
		if (element.validity.valid) {
			this.toggleError(element, 'hide');
			return true;
		}
		// If theres an element event, then only show the error when it has blurred
		if (!event || event && event.type === 'blur') {
			this.toggleError(element, 'show');
		}
		return false;

	};

	/**
	 * Function for validating all required fields of a form, one field at a time.
	 * Calls any custom validation script referenced during instantiation before standard validation
	 * Calls any custom pre-submission script referenced during instantiation after successful validation
	 * Submits the form if all validation is successful
	 */
	validateForm (event) {
		var index,
			elemsLength = this.objGlobals.validatedElements.length,
			bFormValid = true;
		// Run custom validation first
		if (this.objGlobals.customValidationRef !== null) {
			bFormValid = this.objGlobals.customValidationRef();
		}
		// Validate each form element
		for (index = 0; index < elemsLength; index++) {
			if (!this.validateFormElement(this.objGlobals.validatedElements[index])) {
				bFormValid = false;
			}
		}
		// Submit form if valid
		if (bFormValid) {
			// Run the pre-submit reference function if provided
			if (this.objGlobals.preSubmitRef !== null) {
				this.objGlobals.preSubmitRef();
			}
		} else {
			event.preventDefault();
		}
	};

	/**
	 * Function for adding an event listener for inline validation on a single form field
	 * @param {HTMLElement} element - The form field being validated, must be a DOM element
	 */
	addListener (element) {
		var self = this;
		// Validate argument
		if (!element || element instanceof window.HTMLElement === false) {
			this.consoleError('addListener requires a valid DOM element');
			return;
		}
		// Add listeners
		if (element.type === 'radio' || element.type === 'checkbox' || element.type === 'input') {
			element.addEventListener('change', function (event) {
				self.validateFormElement(this, event);
			});
		} else {
			element.addEventListener('input', function (event) {
				self.validateFormElement(this, event);
			});

			element.addEventListener('blur', function (event) {
				self.validateFormElement(this, event);
			});
		}
	};

	/**
	 * Function for adding blur event listeners for inline validation on all required fields of a form, one field at a time
	 */
	addAllListeners () {
		var index,
			elemsLength = this.objGlobals.validatedElements.length;
		// Add listener to each form element
		for (index = 0; index < elemsLength; index++) {
			this.addListener(this.objGlobals.validatedElements[index]);
		}
	};

	/**
	 * Function for adding a click event listener to the submit button of a form
	 */
	addSubmitListener () {
		var self = this,
			submitButton = this.objGlobals.formElement.querySelector('[type="submit"]');
		// Make sure we have one
		if (submitButton === null) {
			this.consoleError('addSubmitListener requires a valid submit button');
			return;
		}
		// Add listener to the submit button
		submitButton.addEventListener('click', function (event) {
			self.validateForm(event);
		});
	};

	/**
	 * Init function to be called during instantiation
	 */
	init () {
		this.disableBrowserValidation();
		this.addAllListeners();
		this.addSubmitListener();
	};

}
