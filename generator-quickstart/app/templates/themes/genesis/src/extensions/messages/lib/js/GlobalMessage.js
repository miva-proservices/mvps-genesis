import Message from './Message';
import EventBus from '@mvps-genesis/event-bus';

export default class GlobalMessage extends Message {

	constructor (message, options) {

		super(message, options);

		EventBus.dispatch('MVPS:addMessage', this);

	}

};
