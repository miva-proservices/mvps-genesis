import EventBus from '@mvps-genesis/event-bus';
import MessageContainer from './MessageContainer';

export default class GlobalMessageContainer extends MessageContainer {

	constructor (options) {

		super(...options);

	}

	bindEvents () {

		EventBus.addEventListener('MVPS:addMessage', this.onAddMessage.bind(this), false);
		EventBus.addEventListener('MVPS:removeMessage', this.onRemoveMessage.bind(this), false);
		EventBus.addEventListener('MVPS:clearMessages', this.onClearMessages.bind(this), false);

	}

	onAddMessage (e) {

		this.add(e.detail);

	}

	onRemoveMessage (e) {

		this.remove(e.detail);

	}

	onClearMessages () {

		this.clear();

	}

};
