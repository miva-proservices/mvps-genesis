import ParameterValidator from '@mvps-genesis/parameter-validator';
import uid from '@mvps-genesis/uid';

export default class Message {

	// Define static properties
	static types = {
		error: 'error',
		info: 'info',
		success: 'success',
		warning: 'warning'
	};

	static classes = {
		autohide: 'auto-hide',
		close: 'x-messages__close',
		icon: 'x-messages__icon',
		iconFontPrefix: 'u-icon-%icon%',
		root: 'x-messages',
		rootIconModifier: 'x-messages--with-icon',
		type: 'x-messages--%type%'
	};

	// Define "private" properties
	_validator;

	// Define "public" properties
	message;

	id;

	element;

	constructor (
		message,
		{
			type = 'info',
			closeable = false,
			autohide = false,
			autohideTime = 5000,
			icon = false,
			customClasses = '',
			customId = ''
		}
	) {

		// Create validator
		this._validator = new ParameterValidator(this);

		// Set message text
		this.message = this._validator.isString('message', message);

		// Set parameters from options
		this.type = this._validator.isString('type', type);
		this.closeable = this._validator.isBoolean('closeable', closeable);
		this.autohide = this._validator.isBoolean('autohide', autohide);
		this.autohideTime = this._validator.isInt('autohideTime', autohideTime);
		this.icon = this._validator.isBoolean('icon', icon);
		this.customClasses = this._validator.isString('customClasses', customClasses);
		this.customId = this._validator.isString('customId', customId);

		// Generate message id
		this.id = uid();

		// Render message
		return this;

	}

	setElement (element) {

		this.element = this._validator.isElement('element', element);

	}

	setId (id) {

		this.element.id = id;

	}

	close () {

		this.element.classList.add('hide');

	}

	getType () {

		const type = Message.types[this.type];

		if (type === undefined) {
			throw new Error('Message::getType - Unsupported `type` option');
		}

		return type;

	}

	getTypeClass () {

		return Message.classes.type.replace('%type%', this.getType());

	}

	getIconClass () {

		if (!this.icon) {
			return false;
		}

		return Message.classes.type.replace('%icon%', this.icon);

	}

	// eslint-disable-next-line max-lines-per-function
	render () {

		// Create root element
		const element = document.createElement('div');
		element.classList.add(Message.classes.root);
		element.classList.add(this.getTypeClass());

		// Set id
		let id = `js-message-${ this.id }`;
		if (this.customId && this.customId.length > 0) {
			element.id = this.customId;
			element.datset.id = id;
		} else {
			element.id = id;
		}

		// Set custom classes
		if (this.customClasses) {
			element.className += ` ${ this.customClasses }`;
		}

		// Add close button
		if (this.closeable) {
			const closeElement = document.createElement('button');
			closeElement.className = Message.classes.close;
			element.appendChild(closeElement);
		}

		// Create icon element
		if (this.icon) {
			const iconElement = document.createElement('span');
			iconElement.className = this.getIconClass();
			element.appendChild(iconElement);
		}

		// Create message inner
		const messageInner = document.createElement('span');
		messageInner.innerHTML = this.message;
		element.appendChild(messageInner);

		// Add auto-hide class and time delay
		if (this.autohide) {
			element.classList.add(Message.classes.autohide);
			element.style.animationDelay = this.autohideTime;
		}

		this.setElement(element);
		this.postRender(element);

		return this.element;

	}

	postRender (element = this.element) {

		if (this.closeable) {

			const closeElement = element.querySelector(Message.classes.close);
			closeElement.addEventListener('click', this.close.bind(this), false);

		}

		if (this.autohide) {

			setTimeout(this.close.bind(this), this.autohideTime + 500);

		}

	}

	/**
	 * Create a message instance from a passed HTMLElement or valid selector
	 * @param {String} elementOrSelector
	 */
	static fromHTML (elementOrSelector) {

		const validator = new ParameterValidator(Message);
		const element = validator.isElement('elementOrSelector', elementOrSelector);
		const options = {
			autohide: element.classList.contains(Message.classes.autohide),
			closeable: element.dataset.closeable !== undefined,
			customId: element.id
		};

		// Identify type via classList
		for (let type in Message.types) {

			let typeClass = Message.classes.type.replace('%type%', type);

			if (element.classList.contains(typeClass)) {

				options.type = type;
				break;

			}

		}

		// Autohide time
		if (element.dataset.autohideTime !== undefined) {

			options.autohideTime = element.dataset.autohideTime;

		}

		const instance = new Message(element.innerHTML, options);
		instance.setElement(element);
		instance.setId(instance.id);
		instance.postRender(element);

		return instance;

	};

};
