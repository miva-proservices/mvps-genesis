import ParameterValidator from '@mvps-genesis/parameter-validator';
import Message from './Message';

export default class MessageContainer {

	// Define "private" properties
	_validator;

	// Define "public" properties
	messages = [];

	container;

	currentMessage;

	constructor ({
		container = '#js-messages'
	} = {}) {

		// Create validator
		this._validator = new ParameterValidator(this);

		// Define container element (optional)
		this.container = this._validator.isElement('container', container, {required: false});

		// Bind any events that need to be attached
		this.bindEvents();

		// If container is set then bootstrap the messages
		if (this.container) {
			this.instanceMesagesFromContainer();
		}

	}

	// eslint-disable-next-line no-empty-function
	bindEvents () {}

	instanceMesagesFromContainer () {

		const messages = this.container.getElementsByClassName(Message.classes.root);

		for (let message of messages) {

			this.messages.push(Message.fromHTML(message));

		}

	}

	add (message) {

		// Validate message type
		this._validator.isInstance('message', message, Message);

		// Set current message property
		this.currentMessage = message;

		// Add to message array
		this.messages.push(message);

		// Add to html container
		if (this.container) {
			this.container.appendChild(message.render());
		}

	}

	remove (message) {

		// Validate message type
		this._validator.isInstance('message', message, Message);

		// Remove from messages array
		this.messages = this.messages.filter((m) => m.id !== message.id);

		// Remove from html container
		if (this.container) {
			this.container.removeChild(this.container.children.namedItem(message.id));
		}

		// Set current message property
		this.currentMessage = this.messages[this.messages.length - 1];

	}

	clear () {

		this.currentMessage = undefined;

		this.messages = [];

		if (this.container) {
			this.container.innerHTML = '';
		}

	}

};
