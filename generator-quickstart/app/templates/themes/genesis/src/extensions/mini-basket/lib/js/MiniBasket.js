import EventBus from '@mvps-genesis/event-bus';
import ParameterValidator from '@mvps-genesis/parameter-validator';

export default class MiniBasket {

	// Define "private" properties
	_validator;

	// Define "public" properties
	isOpen = false;

	constructor ({
		containerElement = '.js-mini-basket',
		closeElements = '.js-mini-basket-close',
		openElements = '.js-mini-basket-open',
		countElements = '.js-mini-basket-count',
		subtotalElements = '.js-mini-basket-subtotal',
		openEvent = 'click',
		closeEvent = 'click',
		isOpenByDefault = false,
		closeOnEsc = true,
		openOnAddToCart = false,
		closeAfterOpen = false,
		closeAfterOpenTime = 5000
	} = {}) {

		// Create validator
		this._validator = new ParameterValidator(this);

		// Save selectors
		this.elementSelectors = {
			closeElements,
			containerElement,
			countElements,
			openElements,
			subtotalElements
		};

		// Create properties from options
		this.containerElement = this._validator.isElement('options.containerElement', containerElement);
		this.closeElements = this._validator.isElementList('options.closeElements', closeElements);
		this.openElements = this._validator.isElementList('options.openElements', openElements);
		this.countElements = this._validator.isElementList('options.countElements', countElements);
		this.subtotalElements = this._validator.isElementList('options.subtotalElements', subtotalElements);
		this.openEvent = this._validator.isString('options.openEvent', openEvent);
		this.closeEvent = this._validator.isString('options.closeEvent', closeEvent);
		this.isOpenByDefault = this._validator.isBoolean('options.isOpenByDefault', isOpenByDefault);
		this.closeOnEsc = this._validator.isBoolean('options.closeOnEsc', closeOnEsc);
		this.openOnAddToCart = this._validator.isBoolean('options.openOnAddToCart', openOnAddToCart);
		this.closeAfterOpen = this._validator.isBoolean('options.closeAfterOpen', closeAfterOpen);
		this.closeAfterOpenTime = this._validator.isInt('options.closeAfterOpenTime', closeAfterOpenTime);

		// Run bind events init function
		this.bindEvents();

		// Run open command if isOpenByDefault is true
		if (this.isOpenByDefault) {
			this.open();
		}

	}

	bindEvents () {

		// Listen for global event - `MVTPS:addToCart` and handle response
		EventBus.addEventListener('MVPS:addToCart', this.onAddToCart.bind(this), false);

		this.bindOpenTransitionEvents();

		// Bind events for buttons
		for (let element of this.openElements) {

			element.addEventListener(this.openEvent, this.open.bind(this), false);

		}

		for (let element of this.closeElements) {

			element.addEventListener(this.closeEvent, this.close.bind(this), false);

		}

	}

	bindOpenTransitionEvents () {

		// Set focus to first close element when container opens
		this.containerElement.addEventListener('transitionend', (event) => {
			if (event.target === this.containerElement && event.propertyName === 'visibility' && document.activeElement !== this.closeElements[0]) {
				this.closeElements[0].focus();
			}
		});

	}

	onKeyup (e) {

		if (e.keyCode === 27) {

			this.close();

		}

	}

	onAddToCart (e) {

		const payload = e.detail;

		if (payload.success) {

			this.onAddToCartSuccess(payload);

		}
		else {

			this.onAddToCartError(payload);

		}

	};

	onAddToCartSuccess (payload) {

		// Replace container element with payload version
		this.replaceContainerElement(this.getContainerElementFromPayload(payload));

		// Re-bind close elements
		this.closeElements = this._validator.isElementList('options.closeElements', this.elementSelectors.closeElements);
		for (let element of this.closeElements) {
			element.addEventListener(this.closeEvent, this.close.bind(this), false);
		}

		// Re-bind open transition events
		this.bindOpenTransitionEvents();

		// Set count and subtotal
		this.count = this.getCount();
		this.subtotal = this.getSubtotal();

		// Update count / subtotal elements
		this.updateCounts();
		this.updateSubtotals();

		// Open mini basket if configured
		if (this.openOnAddToCart) {

			setTimeout(() => {

				this.open();

				// Close mini basket after X ms if configured
				if (this.closeAfterOpen) {
					setTimeout(this.close.bind(this), this.closeAfterOpenTime);
				}

			}, 200);

		}

	}

	onAddToCartError () {

		return false;

	}

	getContainerElementFromPayload (payload) {

		return payload.data.querySelector(this.elementSelectors.containerElement);

	}

	replaceContainerElement (newContainer) {

		this.containerElement.parentNode.replaceChild(newContainer, this.containerElement);

		this.containerElement = newContainer;

	}

	getCount () {

		return this.containerElement.dataset.count;

	}

	getSubtotal () {

		return this.containerElement.dataset.subtotal;

	}

	updateCounts () {

		for (let element of this.countElements) {

			element.textContent = this.count;

		}

	}

	updateSubtotals () {

		for (let element of this.subtotalElements) {

			element.textContent = this.subtotal;

		}

	}

	open (e) {

		if (e) {
			e.preventDefault();
		}

		this.isOpen = true;
		this.containerElement.classList.add('x-mini-basket--open');

		// Bind event for ESC button close
		if (this.closeOnEsc) {
			document.addEventListener('keyup', this.onKeyup.bind(this), false);
		}

	}

	close (e) {

		if (e) {
			e.preventDefault();
		}

		this.isOpen = false;
		this.containerElement.classList.remove('x-mini-basket--open');

		// Remove event for ESC button close
		if (this.closeOnEsc) {
			document.removeEventListener('keyup', this.onKeyup.bind(this), false);
		}

	}

	toggle () {

		return this.isOpen ? this.close() : this.open();

	}

};
