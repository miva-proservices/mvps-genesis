import waitForExpression from '@mvps-genesis/wait-for-expression';

waitForExpression(
	() => { return typeof window.MMSearchField !== 'undefined'; },
	() => {

		window.MMSearchField.prototype.onMenuAppendHeader = function () {
			return null;
		};

		window.MMSearchField.prototype.onMenuAppendItem = function (data) {
			var span = window.newElement('span', {class: 'x-search-preview__entry'}, null, null);
			span.innerHTML = data;

			return span;
		};

		window.MMSearchField.prototype.onMenuAppendStoreSearch = function (search_value) {
			var item = window.newElement('div', {class: 'x-search-preview__search-all'}, null, null);
			item.element_text = window.newTextNode('Search store for product "' + search_value + '"', item);

			return item;
		};

		window.MMSearchField.prototype.onFocus = function () {
			this.element_menu.classList.toggle('x-search-preview--open');
		};

		window.MMSearchField.prototype.onBlur = function () {
			this.element_menu.classList.toggle('x-search-preview--open');
		};

	}
);
