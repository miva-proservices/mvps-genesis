import ParameterValidator from '@mvps-genesis/parameter-validator';
import 'whatwg-fetch';

/*
 *|--------------------------------------------------------------------------
 *| Shipping Estimator
 *|--------------------------------------------------------------------------
 *|
 *| Use this class to retrieve estimated shipping rates data from the
 *| Miva SERT page and interact with the estimate shipping form.
 *|
 */
export default class ShippingEstimator {

	/**
	 * API settings object
	 *
	 * @var			Object
	 * @property	String	fieldName	The field name to enable the SERT page API
	 * @property	String	url			The SERT page URL
	 */
	#api;

	/**
	 * Shipping estimate form settings object
	 *
	 * @var			Object
	 * @property	Element	element				The shipping estimate form element
	 * @property	Object	fields				The shipping estimate form fields object
	 * @property	Element	fields.container	The shipping estimate form fields container
	 * @property	String	fields.controlClass	The shipping estimate form fields display control class
	 * @property	Object	submit				The shipping estimate form submit object
	 * @property	Element	submit.element		The shipping estimate form submit element
	 * @property	Element	submit.originalText	The shipping estimate form submit element original text value
	 * @property	Element	submit.text			The shipping estimate form submit element text value
	 */
	#form;

	/**
	 * Class options object. See class constructor() for option definitions.
	 *
	 * @var	Object
	 */
	#options;

	/**
	 * The estimated shipping results object
	 *
	 * @var			Object
	 * @property	Element	container					The estimated shipping results container element
	 * @property	Object	data						The SERT API response object
	 * @property	Object	data.rates					The SERT API response ship estimate rates data object
	 * @property	String	data.template				The SERT API response template
	 * @property	Boolean	data.success				The SERT API response success flag
	 * @property	Object	recalculate					The estimated shipping results recalculate object
	 * @property	Element	recalculate.element			The estimated shipping results recalculate element
	 * @property	String	recalculate.triggerClass	The estimated shipping results recalculate trigger class
	 */
	#results;

	/**
	 * Parameter Validator object
	 *
	 * @var	@mvps-genesis/parameter-validator	ParameterValidator
	 */
	#validator;

	/**
	 * Create a new ShippingEstimator object
	 *
	 * @param	Element	formElement							The shipping estimate form Element object. Required.
	 * @param	Element	options.formFieldsContainer			Overwrite form fields container selector. Defaults to '.js-shipping-estimate-fields'.
	 * @param	String	options.formFieldsControlClass		Overwrite the class that controls the form fields display. Defaults to 'u-hidden'.
	 * @param	String	options.formSubmitText				Overwrite form submission element text while shipping rates are being calculated. Defaults to "Calculating...".
	 * @param	String	options.recalcTriggerClass			Overwrite the Recalculate button class name. Defaults to 'js-shipping-estimate-recalculate'.
	 * @param	Element	options.resultsTemplateContainer	Overwrite shipping rates template container selector. Defaults to '.js-shipping-estimate-results'.
	 * @param	String	options.sertApiFieldName			Overwrite the SERT API field name. This field name is used on the SERT page to enable the API and return a JSON response. Defaults to 'sert_api'.
	 * @param	Element	options.shipStateSelectElem			Overwrite ship state select element. Defaults to '[name="ShipEstimate:ship_state_select"]'.
	 * @param	Element	options.shipStateElem				Overwrite ship state element. Defaults to '[name="ShipEstimate:ship_state"]'.
	 */
	constructor (formElement, options = {}) {
		// Create param validator
		this.#validator = new ParameterValidator(this);

		// Provided options
		this.#options = this.#validator.isObject('options', options);

		// Shipping estimate form object
		this.#form = {};
		this.#form.element = this.#validator.isElement('formElement', formElement);
		this.#form.fields = {};
		this.#form.fields.container = this.#validator.isElement('options.formFieldsContainer', this.#options.formFieldsContainer || '.js-shipping-estimate-fields', {container: this.#form.element});
		this.#form.fields.controlClass = this.#validator.isString('options.formFieldsControlClass', this.#options.formFieldsControlClass || 'u-hidden', {container: this.#form.element});
		this.#form.fields.shipStateSelectElem = this.#validator.isElement('options.shipStateSelectElem', this.#options.shipStateSelectElem || '[name="ShipEstimate:ship_state_select"]', {container: this.#form.element,
			optional: true});
		this.#form.fields.shipStateElem = this.#validator.isElement('options.shipStateElem', this.#options.shipStateElem || '[name="ShipEstimate:ship_state"]', {container: this.#form.element,
			optional: true});
		this.#form.submit = {};
		this.#form.submit.element = this.#validator.isElement('formElement.querySelector(\'[type="submit"]\')', '[type="submit"]', {container: this.#form.element});
		this.#form.submit.originalText = this.getFormSubmitText();
		this.#form.submit.text = this.#validator.isString('options.formSubmitText', this.#options.formSubmitText || 'Calculating...');

		// Results object
		this.#results = {
			data: {
				rates: {},
				template: ''
			}
		};
		this.#results.container = this.#validator.isElement('options.resultsTemplateContainer', this.#options.resultsTemplateContainer || '.js-shipping-estimate-results', {container: this.#form.element});
		this.#results.recalculate = {};
		this.#results.recalculate.triggerClass = this.#validator.isString('options.recalcTriggerClass', this.#options.recalcTriggerClass || 'js-shipping-estimate-recalculate');

		// API object
		this.#api = {
			url: this.#form.element.action
		};

		if (!this.#api.url) {
			throw new Error('ShippingEstimator :: Invalid form action provided');
		}

		this.#api.fieldName = this.#validator.isString('options.sertApiFieldName', this.#options.sertApiFieldName || 'sert_api');

		// Bind base event listeners
		this.bindEvents();
	}

	/**
	 * Bind the base events
	 *
	 * @return	Boolean
	 */
	bindEvents () {
		this.#form.element.addEventListener('submit', this.onSubmit.bind(this), false);

		if (this.#form.fields.shipStateSelectElem && this.#form.fields.shipStateElem) {
			this.#form.fields.shipStateSelectElem.addEventListener('change', () => this.shipStateSelectChange(), false);
			this.shipStateSelectChange();
		}

		return true;
	}

	/**
	 * Bind the Recalculate events. This will typically be issued after the SERT
	 * template is retrieved because the template contains the Recalculate button.
	 *
	 * @return	Boolean
	 */
	bindRecalculateEvents () {
		// Recalculate shipping button clicked, reset results HTML and show form fields
		this.#results.recalculate.element.addEventListener('click', () => {
			this.hideTemplate();
			this.showFormFields();
		});

		return true;
	}

	/**
	 * On change of Ship State Select, show/hide Ship State when applicable
	 */
	shipStateSelectChange () {
		if (this.#form.fields.shipStateSelectElem.value) {
			this.#form.fields.shipStateElem.disabled = true;

			if (this.#form.fields.shipStateElem.parentNode) {
				this.#form.fields.shipStateElem.parentNode.classList.add(this.#form.fields.controlClass);
			}
		} else {
			this.#form.fields.shipStateElem.disabled = false;

			if (this.#form.fields.shipStateElem.parentNode) {
				this.#form.fields.shipStateElem.parentNode.classList.remove(this.#form.fields.controlClass);
			}
		}
	}

	/**
	 * Form submit handler
	 *
	 * @param	Event	event
	 */
	async onSubmit (event) {
		event.preventDefault();

		// Disable the form submission while the rates are calculating
		this.disableSubmitElement();

		// Get the shipping rates
		this.#results.data = await this.getShippingRates();

		// Hide the shipping estimate form fields
		this.hideFormFields();

		// Display shipping estimate rates template
		this.showTemplate();

		// Set the Recalculate Shipping element now that the shipping rates template is loaded
		this.#results.recalculate.element = this.#validator.isElement('options.recalcTriggerClass', '.' + this.#results.recalculate.triggerClass, {container: this.#form.element});

		if (this.#results.recalculate.element) {
			this.bindRecalculateEvents();
		}

		// Enable the form submission element
		this.enableSubmitElement();
	}

	/**
	 * Get the shipping rates data
	 *
	 * @return	Object	Shipping rates response object. The object contains the following properties:
	 * 						- success: Flag to determine if the request was success/unsuccessful
	 * 						- rates: Object containing the shipping rates data
	 * 						- template: Rendered MVT SERT template
	 */
	async getShippingRates () {
		// Collect the estimated shipping form data
		const estShipFormData = new FormData(this.#form.element);

		// Append the sertApiFieldName field if it does not exist
		if (!estShipFormData.hasOwnProperty(this.#api.fieldName)) {
			estShipFormData.append(this.#api.fieldName, 1);
		}

		// Send the estimated shipping rates request and return the JSON response
		const response = await fetch(this.#api.url, {
			body: estShipFormData,
			method: 'POST'
		});

		if (!response.ok) {
			throw new Error('Network response was not successful');
		}

		return response.json();
	}

	/**
	 * Get the form submission element display text based on node name
	 *
	 * @return	String
	 */
	getFormSubmitText () {
		return this.isInputElement(this.#form.submit.element) ? this.#form.submit.element.value : this.#form.submit.element.text;
	}

	/**
	 * Set the form submission element display text based on node name
	 *
	 * @param	String	text	The text value
	 *
	 * @return	Boolean
	 */
	setFormSubmitText (text) {
		const submitText = this.#validator.isString('text', text);

		if (this.isInputElement(this.#form.submit.element)) {
			this.#form.submit.element.value = submitText;
		} else {
			this.#form.submit.element.text = submitText;
		}

		return true;
	}

	/**
	 * Determine if Element is an input Element
	 *
	 * @param	Element	element
	 *
	 * @return	Boolean
	 */
	isInputElement (element) {
		const testElm = this.#validator.isElement('element', element);

		return testElm.nodeName === 'INPUT';
	}

	/**
	 * Disable the form submit element
	 *
	 * @return	Boolean
	 */
	disableSubmitElement () {
		this.#form.submit.element.disabled = true;
		this.#form.submit.element.classList.add('is-disabled');
		this.setFormSubmitText(this.#form.submit.text);

		return true;
	}

	/**
	 * Enable the form submit element
	 *
	 * @return	Boolean
	 */
	enableSubmitElement () {
		this.setFormSubmitText(this.#form.submit.originalText);
		this.#form.submit.element.classList.remove('is-disabled');
		this.#form.submit.element.disabled = false;

		return true;
	}

	/**
	 * Hide the form fields
	 *
	 * @return	Boolean
	 */
	hideFormFields () {
		this.#form.fields.container.classList.add(this.#form.fields.controlClass);

		return true;
	}

	/**
	 * Show the form fields
	 *
	 * @return Boolean
	 */
	showFormFields () {
		this.#form.fields.container.classList.remove(this.#form.fields.controlClass);

		return true;
	}

	/**
	 * Hide the shipping rates template
	 */
	hideTemplate () {
		this.#results.container.innerHTML = '';
	}

	/**
	 * Show the shipping rates template
	 */
	showTemplate () {
		this.#results.container.innerHTML = this.#results.data.template;
	}
}
