import ParameterValidator from '@mvps-genesis/parameter-validator';
import debounced from '@mvps-genesis/debounced';

const getNextSibling = function (elem, selector) {

	// Get the next sibling element
	let sibling = elem.nextElementSibling;

	// If there's no selector, return the first sibling
	if (!selector) {
		return sibling;
	}

	/*
	 * If the sibling matches our selector, use it
	 * If not, jump to the next sibling and continue the loop
	 */
	while (sibling) {
		if (sibling.matches(selector)) {
			return sibling;
		}
		sibling = sibling.nextElementSibling;
	}

	return null;
};

const getParent = function (elem, selector) {

	let parent = elem.parentElement;

	if (!selector) {
		return parent;
	}

	while (parent) {
		if (parent.matches(selector)) {
			return parent;
		}
		parent = parent.parentElement;
	}

};

export default class TransfigureNavigation {

	/**
	 * Options.menu
	 * options.parentMenuItems
	 * options.menuLink
	 * options.mainMenuOpenButtons
	 * options.mainMenuCloseButtons
	 * options.showPreviousMenuButtons
	 */
	constructor (options) {

		// Create validator
		this._validator = new ParameterValidator(this);

		this.options = this._validator.isObject('options', options);
		this.options.breakpoint = this._validator.isNumber('options.breakpoint', options.breakpoint || 960);

		this.elements = {
			mainMenuCloseButtons: this._validator.isElementList('options.mainMenuCloseButtons', options.mainMenuCloseButtons),
			mainMenuOpenButtons: this._validator.isElementList('options.mainMenuOpenButtons', options.mainMenuOpenButtons),
			menu: this._validator.isElement('options.menu', options.menu),
			parentMenuItems: this._validator.isElementList('options.parentMenuItems', options.parentMenuItems),
			showPreviousMenuButtons: this._validator.isElementList('options.showPreviousMenuButtons', options.showPreviousMenuButtons)
		};

		this.setClientPort();
		this.bindEvents();

		if (this.shouldTrigger()) {
			this.showSubnavigation();
		}

	}

	setClientPort () {
		this.clientPort = document.documentElement.clientWidth;
	}

	shouldTrigger () {
		return this.clientPort < this.options.breakpoint;
	}

	showSubnavigation () {

		// Parent menu items
		for (let parentMenuItem of this.elements.parentMenuItems) {

			Array.from(parentMenuItem.children).forEach((link) => {
				if (link.matches(this.options.menuLink)) {
					link.addEventListener('click', this.onLinkClick, false);
				}
			});

		}

		// Show previous button
		for (let showPreviousMenuButton of this.elements.showPreviousMenuButtons) {

			showPreviousMenuButton.addEventListener('click', this.onPreviousButtonClick, false);

		}


	}

	bindEvents () {

		// Resize event
		window.addEventListener('resize', debounced(66, e => this.onResize(e)), false);

		// Open and close events
		for (let mainMenuOpenButton of this.elements.mainMenuOpenButtons) {

			mainMenuOpenButton.addEventListener('click', e => this.onOpen(e), false);

		}

		for (let mainMenuCloseButton of this.elements.mainMenuCloseButtons) {

			mainMenuCloseButton.addEventListener('click', e => this.onClose(e), false);

		}

	}

	onResize (e) {

		this.setClientPort();

		if (this.shouldTrigger()) {
			this.showSubnavigation();
		}

	}

	onOpen (e) {

		e.preventDefault();

		document.documentElement.classList.add('has-open-main-menu');
		this.elements.menu.classList.add('is-open');

	}

	onClose (e) {

		e.preventDefault();

		document.documentElement.classList.remove('has-open-main-menu');
		this.elements.menu.classList.remove('is-open');

	}

	onLinkClick (e) {

		e.preventDefault();

		let selected = this;

		let nextList = getNextSibling(selected, 'ul');
		if (nextList) {

			nextList.classList.remove('is-hidden');

		}

		let parent = getParent(selected, '.has-child-menu');
		let parentList = parent.closest('ul');

		if (parentList) {

			parentList.classList.add('show-next');

		}

	}

	onPreviousButtonClick (e) {

		e.preventDefault();

		let selected = this;

		let parentList = getParent(selected, 'ul');
		parentList.classList.add('is-hidden');

		let parentMenu = getParent(parentList, '.has-child-menu');
		let parentMenuList = parentMenu.closest('ul');

		if (parentMenuList) {

			parentMenuList.classList.remove('show-next');

		}

	}

};
