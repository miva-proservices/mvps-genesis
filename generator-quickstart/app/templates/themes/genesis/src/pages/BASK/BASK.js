import './BASK.scss';
import {PageManager} from '@mvps-genesis/bootstrap';
import 'extensions/quantify';
import ShippingEstimator from 'extensions/shipping-estimator';

export default class BASK extends PageManager {

	constructor (pageContext) {

		super(pageContext);

	}

	onReady () {

		/**
		 * Estimate Shipping
		 */
		window.mivaJS.estimateShipping = function (element) {
			'use strict';

			const currentModal = document.querySelector('[data-hook="' + element + '"]');

			if (currentModal) {
				const shippingEstimateForm = currentModal.querySelector('form[name="shipestimate_form"]');

				if (shippingEstimateForm) {
					// Initialize shipping estimator functionality
					new ShippingEstimator(shippingEstimateForm);
				}
			}
		};

	}
}
