import './Global.scss';
import 'extensions/fasten-header';
import TransfigureNavigation from 'extensions/transfigure-navigation';
import ContinueShopping from 'extensions/continue-shopping';
import {PageManager} from '@mvps-genesis/bootstrap';
import {GlobalMessageContainer} from 'extensions/messages';
import MiniBasket from 'extensions/mini-basket';
import {initCarousels} from 'extensions/carousel';
import dynamicFormElementAttributes from 'extensions/dynamic-form-element-attributes';
import WebFont from 'webfontloader';
import ConfirmAction from 'extensions/confirm-action';

export default class Global extends PageManager {

	constructor (pageContext) {

		super(pageContext);

		WebFont.load({
			typekit: {
				id: 'kpn1tpc'
			}
		});

		this.initSearchPreview();

		/**
		 * Create global message container
		 */
		new GlobalMessageContainer({container: '#js-messages'});

		/**
		 * Initialize the Drop-Down Navigation extension
		 */
		new TransfigureNavigation({
			mainMenuCloseButtons: '.js-close-main-menu',
			mainMenuOpenButtons: '.js-open-main-menu',
			menu: '.js-transfigure-navigation',
			menuLink: '.c-navigation__link',
			parentMenuItems: '.js-has-child-menu',
			showPreviousMenuButtons: '.js-show-previous-menu'
		});

		/**
		 * Initialize carousels
		 */
		initCarousels();

		this.initializeHTMLFormValidator();

		/**
		 * Initialize the Dynamic Form-Element Attributes
		 */
		dynamicFormElementAttributes();

		this.initMiniModal();
		this.accordionPolyfill();
		this.initLazyload();

	}

	onReady () {

		/**
		 * Initialize the Mini-Basket extension
		 */
		new MiniBasket({
			openOnAddToCart: true
		});

		/**
		 * Initialize continue shopping link
		 */
		new ContinueShopping();

		this.showRelated();
		this.triggerPrint();

		new ConfirmAction();

	}

	async initSearchPreview () {
		await import('extensions/search-preview');
	}

	async initLazyload () {
		// Elements
		const lazyLoadElements = document.getElementsByClassName('lazyload');

		if (lazyLoadElements.length === 0) {
			return;
		}

		// Module
		await import('lazysizes/plugins/unveilhooks/ls.unveilhooks');
		await import('lazysizes/plugins/bgset/ls.bgset');
		await import('lazysizes/plugins/custommedia/ls.custommedia');
		await import('lazysizes');
	}

	async accordionPolyfill () {

		/**
		 * Initialize Accordion Polyfill
		 */

		// Elements
		const accordionElements = document.getElementsByClassName('x-accordion');

		if (accordionElements.length === 0) {
			return;
		}

		// Module
		await import('details-polyfill');
	}

	async initializeHTMLFormValidator () {

		/**
		 * Initialize Inline Form Validations
		 */

		const forms = document.getElementsByClassName('js-form-validator');

		if (forms.length === 0) {
			return;
		}

		const {HTMLFormValidator} = await import('extensions/form-validation');

		forms.forEach(function (form) {
			form.HTMLFormValidator = new HTMLFormValidator({
				formElement: form,
				wrapperSelector: '.c-form-list__item'
			});
		});
	}

	async initMiniModal () {

		/**
		 * Mini Modal
		 */
		// Elements
		const miniModalElements = Array.from(document.querySelectorAll('[data-mini-modal]'));

		if (miniModalElements.length === 0) {
			return;
		}

		// Module
		const {default: minimodal} = await import('extensions/minimodal');

		// Initiate the modal for each element
		miniModalElements.forEach(element => {
			minimodal(element).init();
		});
	}

	showRelated () {

		/**
		 * Toggle the `is-open` state of the global account box.
		 */

		var trigger = document.querySelector('[data-hook="show-related"]');

		if (trigger) {
			var relatedTarget = document.querySelector('[data-hook="' + trigger.getAttribute('data-target') + '"]');

			trigger.addEventListener('click', function (event) {
				event.preventDefault();
				relatedTarget.classList.toggle('is-open');
			}, false);

			document.addEventListener('mousedown', function (event) {
				if (relatedTarget.classList.contains('is-open')) {
					if (!relatedTarget.contains(event.target) && event.target !== trigger) {
						relatedTarget.classList.toggle('is-open');
						event.preventDefault();
					}
				}
			}, false);

			window.addEventListener('keydown', function (event) {
				if (event.defaultPrevented) {
					return;
				}

				switch (event.key) {
				case 'Escape':
					if (relatedTarget.classList.contains('is-open')) {
						relatedTarget.classList.toggle('is-open');
					}
					break;
				default:
					return;
				}

				event.preventDefault();
			}, true);
		}

	}

	triggerPrint () {

		/**
		 *  Launch Printer Dialog
		 */
		const printTriggers = document.getElementsByClassName('js-trigger-print');

		if (printTriggers.length) {
			printTriggers.forEach(element => {
				element.addEventListener('click', function (e) {
					e.preventDefault();
					window.print();
				}, false);
			});
		}

	}

}
