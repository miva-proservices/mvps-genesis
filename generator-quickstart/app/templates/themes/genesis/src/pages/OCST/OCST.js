import {PageManager} from '@mvps-genesis/bootstrap';
import ConfirmAction from 'extensions/confirm-action';

export default class OCST extends PageManager {

	constructor (pageContext) {

		super(pageContext);

	}

	onReady () {

		new ConfirmAction('.js-confirm-exit', {
			message: 'Are you sure you want to exit checkout?',
			redirectUrl: this.pageContext.Basket_Url
		});

		this.ajaxedCouponForm();

	}

	ajaxedCouponForm () {

		this.bindAjaxCouponFormEvents();
	}

	bindAjaxCouponFormEvents () {

		const couponForms = document.querySelectorAll('[data-hook="basket__coupon-form"]');

		for (const couponForm of couponForms) {
			couponForm.addEventListener('submit', (e) => this.couponFormSubmit(e, couponForm));
		}

	}

	async couponFormSubmit (e, form) {

		e.preventDefault();
		e.stopImmediatePropagation();

		if (form.getAttribute('data-status') === 'submitting') {
			return;
		}

		const submitButton = form.querySelector('[data-hook="basket__coupon-form-submit"]');
		const submitButtonText = submitButton.nodeName.toLowerCase() === 'input' ? submitButton.value : submitButton.textContent;
		const data = new FormData(form);

		form.setAttribute('data-status', 'submitting');
		submitButton.setAttribute('disabled', 'disabled');

		if (submitButton.nodeName.toLowerCase() === 'input') {
			submitButton.value = 'Processing...';
		} else {
			submitButton.textContent = 'Processing...';
		}

		document.querySelector('#messages').parentNode.removeChild(document.querySelector('#messages'));

		const response = await fetch(form.action, {
			body: data,
			method: 'POST'
		});

		if (!response.ok) {
			throw new Error('Network response was not successful');
		}

		// Reset button text and form status
		submitButton.removeAttribute('disabled');

		if (submitButton.nodeName.toLowerCase() === 'input') {
			submitButton.value = submitButtonText;
		} else {
			submitButton.textContent = submitButtonText;
		}

		form.setAttribute('data-status', 'idle');

		this.handleCouponResponse(await response.text());

	}

	handleCouponResponse (returnData) {

		const response = document.createElement('html');
		response.innerHTML = returnData;

		const basketSummary = response.querySelector('#checkout_basket_summary');
		const responseMessage = response.querySelector('#messages');

		response.querySelector('[data-hook="basket__coupon-form"]').parentElement.prepend(responseMessage);
		document.querySelector('#checkout_basket_summary').innerHTML = basketSummary.innerHTML;

		this.bindAjaxCouponFormEvents();

	}

};
