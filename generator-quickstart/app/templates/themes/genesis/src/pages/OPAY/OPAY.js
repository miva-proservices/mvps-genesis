import {PageManager} from '@mvps-genesis/bootstrap';
import paymentMethod from 'extensions/payment-method';
import ConfirmAction from 'extensions/confirm-action';

export default class OPAY extends PageManager {

	constructor (pageContext) {

		super(pageContext);

	}

	onReady () {

		new ConfirmAction('.js-confirm-exit', {
			message: 'Are you sure you want to exit checkout?',
			redirectUrl: this.pageContext.Basket_Url
		});

		this.creditCardDetection();

		/**
		 * Added functionality to help style the default Miva output payment
		 * fields.
		 */

		const cvvElem = document.querySelector('[data-id="cvv"]');
		const cvvInput = '';

		if (cvvElem) {
			cvvInput = cvvElem.querySelector('input');

			if (cvvInput) {
				cvvInput.setAttribute('type', 'tel');
			}
		}

	}

	creditCardDetection () {

		/**
		 * Credit Card Detection
		 */

		const detectCard = document.getElementsByClassName('js-detect-card');
		const paymentMethodDisplays = document.getElementsByClassName('js-payment-method-display');
		const paymentMethods = document.getElementsByClassName('js-payment-method');

		const detectPaymentMethod = function (input) {
			if (isNaN(input.value)) {
				input.classList.add('has-error');
			}

			paymentMethod(input, function (paymentDetected) {
				if (!paymentDetected) {
					return;
				}

				input.classList.remove('has-error');

				if (paymentMethodDisplays.length) {
					paymentMethodDisplays.forEach(paymentMethodDisplay => {
						paymentMethodDisplay.innerText = paymentDetected.display;
					});
				}

				if (paymentMethods.length) {
					paymentMethods.forEach((payMethod) => {
						payMethod.value = window.supportedPaymentMethods.findPaymentMethod(paymentDetected.name);
					});
				}
			});
		};

		if (detectCard.length) {
			detectCard.forEach((input) => {
				input.setAttribute('type', 'tel');
				input.addEventListener('input', () => detectPaymentMethod(input), false);
				input.addEventListener('paste', () => detectPaymentMethod(input), false);
			});
		}
	}

};
