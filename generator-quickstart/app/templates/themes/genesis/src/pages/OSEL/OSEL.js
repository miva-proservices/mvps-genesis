import {PageManager} from '@mvps-genesis/bootstrap';
import ConfirmAction from 'extensions/confirm-action';

export default class OPAY extends PageManager {

	constructor (pageContext) {

		super(pageContext);

	}

	onReady () {

		new ConfirmAction('.js-confirm-exit', {
			message: 'Are you sure you want to exit checkout?',
			redirectUrl: this.pageContext.Basket_Url
		});

	}

};
