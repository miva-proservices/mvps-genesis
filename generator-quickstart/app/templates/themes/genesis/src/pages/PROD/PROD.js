import './PROD.scss';
import {PageManager} from '@mvps-genesis/bootstrap';
import 'extensions/quantify';
import AjaxAddToCart from 'extensions/ajax-add-to-cart';

export default class PROD extends PageManager {

	constructor (pageContext) {

		super(pageContext);

	}

	onReady () {

		/**
		 * Initialize ajax add to cart functionality
		 */
		const purchaseForm = document.getElementById('js-purchase-form');

		if (purchaseForm && purchaseForm.querySelector('.js-add-to-cart')) {
			new AjaxAddToCart(purchaseForm);
		}

		/**
		 * This removes the default Miva `closeup_backing` container.
		 */
		var closeupBacking = document.querySelector('div.closeup_backing');

		window.addEventListener('load', function () {
			if (closeupBacking) {
				closeupBacking.parentNode.removeChild(document.querySelector('div.closeup_backing'));
			}
		});

	}

}
