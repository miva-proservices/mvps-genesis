// ========================= IMPORT DEPENDENCIES ========================= //

import {bootstrap} from '@mvps-genesis/bootstrap';
import Global from 'pages/Global';

// ========================= RUN MAIN SCRIPT ========================= //

bootstrap({

	// Pass your MVT to JS namespace
	context: window.mivaJS,

	// Global Page class
	globalClass: Global,

	// A directory of page classes or scss files to be loaded by matching the `Screen` context variable
	screenImports: {
		BASK: () => import('pages/BASK/BASK.js'),
		OCST: () => import('pages/OCST/OCST.js'),
		OPAY: () => import('pages/OPAY/OPAY.js'),
		OSEL: () => import('pages/OSEL/OSEL.js'),
		PROD: () => import('pages/PROD/PROD.js'),
		SFNT: () => import('pages/SFNT/SFNT.js')
	},

	// A directory of page classes or scss files to be loaded by matching the `Page_Code` context variable
	pageCodeImports: {},

	// A directory of page classes or scss files to be loaded by matching the `Screen` or `Page_Code` context variable against a regular expression
	regexImports: {}

});
