// Core
const path = require('path');
const webpack = require('webpack');

// Plugins
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

// Common configuration, with extensions in webpack.local.js and webpack.prod.js
// eslint-disable-next-line max-lines-per-function
module.exports = () => ({
	bail: true,
	entry: {
		main: path.resolve(__dirname, './src/theme.js')
	},
	mode: 'development',
	module: {
		rules: [
			{
				loader: 'vue-loader',
				test: /\.vue$/
			},
			{
				exclude: file => (/node_modules/).test(file) &&
					!(/@mvps-genesis[\\/]/).test(file) &&
					!(/\.vue\.js/).test(file),
				test: /\.m?js$/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							plugins: [
								'@babel/plugin-syntax-dynamic-import',
								'@babel/plugin-transform-runtime',
								'@babel/plugin-proposal-class-properties',
								'@babel/plugin-transform-parameters',
								[
									'@babel/plugin-transform-for-of',
									{
										assumeArray: true
									}
								]
							],
							presets: [
								[
									'@babel/preset-env',
									{
										corejs: {
											version: 3
										},
										loose: true,
										modules: false,
										targets: 'last 2 versions, not dead, not ie < 11',
										useBuiltIns: 'usage'

									}
								]
							]
						}
					}
				]
			},
			{
				test: /\.s?css$/i,
				use: [
					'vue-style-loader',
					{
						loader: MiniCssExtractPlugin.loader
					},
					{
						// Translates CSS into CommonJS
						loader: 'css-loader',
						options: {
							sourceMap: true
						}
					},
					// Fix relative url paths to images/fonts
					'resolve-url-loader',
					{
						// Enable PostCSS features (autoprefixer)
						loader: 'postcss-loader',
						options: {
							postcssOptions: {
								config: 'postcss.config.js'
							},
							sourceMap: true
						}
					},
					{
						// Compiles Sass to CSS
						loader: 'sass-loader',
						options: {
							sassOptions: {
								includePaths: [path.resolve(__dirname, 'src')]
							},
							sourceMap: true
						}
					}
				]
			},
			{
				test: /\.(html)$/,
				use: [
					{
						loader: 'html-loader',
						options: {
							attrs: false
						}
					}
				]
			},
			{
				generator: {
					filename: 'images/[hash].[name][ext][query]'
				},
				resourceQuery: /^(?!fonts$).*/,
				test: /\.(svg|gif|png|jpg|jpeg)(\?v=\d+\.\d+\.\d+)?$/,
				type: 'asset'
			},
			{
				generator: {
					filename: 'fonts/[hash].[name][ext][query]'
				},
				test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
				type: 'asset'
			}
		]
	},
	performance: {
		hints: 'warning',
		maxAssetSize: 512000,
		maxEntrypointSize: 512000
	},
	plugins: [
		new CleanWebpackPlugin({
			cleanStaleWebpackAssets: false
		}),
		new webpack.ProgressPlugin(),
		new HtmlWebpackPlugin(),
		new MiniCssExtractPlugin({
			chunkFilename: '[name].[contenthash].chunk.css',
			filename: '[name].css'
		}),
		new VueLoaderPlugin()
	],
	profile: true,
	resolve: {
		extensions: [
			'.js',
			'.json',
			'.mjs',
			'.vue'
		],
		modules: [
			path.resolve('./src'),
			'node_modules'
		]
	},
	stats: {
		cached: true,
		colors: true
	}
});
