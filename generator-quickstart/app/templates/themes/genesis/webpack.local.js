// Core
const {merge} = require('webpack-merge');
const path = require('path');
const commonConfig = require('./webpack.common.js');
const themeSettings = require('./theme-settings.json');

// Plugins
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

// Merge common and local configurations
module.exports = env => merge(commonConfig(env), {
	devtool: 'eval-cheap-source-map',
	mode: 'development',
	output: {
		chunkFilename: '[contenthash].chunk.js',
		filename: '[name].js',
		path: path.resolve(__dirname, 'public/dist'),
		publicPath: `/${themeSettings.environments[env.MIVA_ENV].moduleRoot}/_local/dist/`
	},
	performance: {
		hints: false
	},
	plugins: [
		new BrowserSyncPlugin(
			{
				cors: true,
				files: ['src/**'],
				https: true,
				open: false,
				proxy: {
					target: themeSettings.environments[env.MIVA_ENV].proxyUrl
				},
				rewriteRules: [
					{
						match: new RegExp(`(b[0-9]+/)?${themeSettings.environments[env.MIVA_ENV][env.LAUNCH ? 'launchThemePath' : 'themePath']}(public/)?`, 'g'),
						replace: '_local/'
					}
				],
				serveStatic: [
					{
						dir: 'public',
						route: `/${themeSettings.environments[env.MIVA_ENV].moduleRoot}/_local`
					}
				]
			},
			{
				injectCss: true,
				reload: false,
				stream: true
			}
		)
	],
	watch: true
});
