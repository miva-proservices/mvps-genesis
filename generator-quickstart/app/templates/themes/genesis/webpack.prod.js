// Core
const {merge} = require('webpack-merge');
const path = require('path');
const commonConfig = require('./webpack.common.js');
const themeSettings = require('./theme-settings.json');

// Plugins
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

// Merge common and production configurations
// eslint-disable-next-line max-lines-per-function
module.exports = env => merge(commonConfig(env), {
	devtool: 'source-map',
	mode: 'production',
	optimization: {
		emitOnErrors: false,
		minimizer: [
			new TerserPlugin({
				extractComments: true,
				test: /\.js(\?.*)?$/i
			}),
			new OptimizeCSSAssetsPlugin({
				cssProcessorOptions: {
					discardComments: {
						removeAll: true
					},
					map: {
						annotation: true,
						inline: false
					}
				}
			})
		],
		splitChunks: {
			automaticNameDelimiter: '~',
			cacheGroups: {
				default: {
					minChunks: 2,
					priority: -20,
					reuseExistingChunk: true
				},
				vendors: {
					name (module) {

						/*
						 * Get the package name & version from the module.context
						 * example module.context: "C:\Users\yourName\Documents\Development\mvps-genesis\node_modules\.pnpm\lazysizes@5.2.0\node_modules\lazysizes\plugins\unveilhooks"
						 */
						const packageName = module.context.match(/[\\/]node_modules[\\/].pnpm[\\/](.*?)([\\/]|$)/)[1];

						// Npm package names are URL-safe, but some servers don't like @ symbols
						return `pnpm.${packageName.replace('@', '')}`;

					},
					priority: -10,
					reuseExistingChunk: true,
					test: /[\\/]node_modules[\\/]/
				}
			},
			chunks: 'async',
			maxAsyncRequests: 6,
			maxInitialRequests: 4,
			minChunks: 1,
			minSize: 30000
		}
	},
	output: {
		chunkFilename: '[name].[contenthash].chunk.js',
		filename: '[name].js',
		path: path.resolve(__dirname, 'public/dist'),
		publicPath: `/${themeSettings.environments[env.MIVA_ENV].moduleRoot}/${themeSettings.environments[env.MIVA_ENV][env.LAUNCH ? 'launchThemePath' : 'themePath']}public/dist/`
	},
	plugins: [
		new BundleAnalyzerPlugin({
			analyzerMode: 'static',
			openAnalyzer: false
		})
	]
});
