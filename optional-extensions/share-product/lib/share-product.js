function PopupCenter (url, title, w, h) {

	// Fixes dual-screen position                         Most browsers      Firefox

	let dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	let dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	let width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	let height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	let left = width / 2 - w / 2 + dualScreenLeft;
	let top = height / 2 - h / 2 + dualScreenTop;
	let newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

	// Puts focus on the newWindow
	if (window.focus()) {
		newWindow.focus();
	}
}

export default function shareProduct () {

	const triggers = document.getElementsByClassName('js-share-product-button');

	for (let element of triggers) {

		let social_media_type = element.getAttribute('data-social');
		let url = '';
		let title = '';
		let w = '';
		let h = '';

		switch (social_media_type) {
		case 'facebook':
			url = element.getAttribute('href');
			url += '&u=' + element.getAttribute('data-url');
			title = 'Share';
			w = 750;
			h = 650;
			break;
		case 'twitter':
			url = element.getAttribute('href');
			title = 'Share';
			w = 500;
			h = 300;
			break;
		case 'pinterest':
			url = element.getAttribute('href');
			url += '?url=' + element.getAttribute('data-url');
			url += '&media=' + element.getAttribute('data-media');
			url += '&description=' + element.getAttribute('data-description');
			title = 'Share';
			w = 750;
			h = 650;
			break;
		}

		element.addEventListener('click', function (e) {
			e.preventDefault();
			PopupCenter(url, title, w, h);
		});

	};
}
