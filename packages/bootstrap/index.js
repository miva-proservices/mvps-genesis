import bootstrap from './lib/bootstrap';
import PageManager from './lib/PageManager';

export {
	bootstrap,
	PageManager
};
