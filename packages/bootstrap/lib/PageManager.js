export default class PageManager {
	/**
	 * constructor for PageManager class
	 *
	 * @param {Object} pageContext The Miva to JS namespace object (ex: mivaJS)
	 */
	constructor (pageContext) {
		this.pageContext = pageContext;
	}

	/**
	 * callback function after DOMContentLoaded event
	 */
	onReady () {
	}

	/**
	 * static method used to dynamically load a PageManager instance and listen for onReady state
	 *
	 * @param {Object} pageContext The Miva to JS namespace object (ex: mivaJS)
	 */
	static load (pageContext) {
		const page = new this(pageContext);
		const callback = () => {
			page.onReady.bind(page)();
		};

		if (
			document.readyState === 'complete' ||
			(document.readyState !== 'loading' && !document.documentElement.doScroll)
		) {
			callback();
		} else {
			document.addEventListener('DOMContentLoaded', callback);
		}
	}
};
