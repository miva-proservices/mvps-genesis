import string2regex from '@mvps-genesis/string2regex';

/**
 * Bootstrap a Miva site with Genesis
 *
 * @param {Object} config configuration options used to determine context, routing and global scripts
 */
export default function ({
	context = window.mivaJS,
	globalClass = null,
	screenImports = {},
	pageCodeImports = {},
	regexImports = {}
} = {}) {
	// check for config keys / context variables
	if (context == null) {
		throw new Error('[@mvps-genesis/bootstrap] - Key `context` object was not passed within `config`.');
	}
	if (context.Screen == null) {
		throw new Error('[@mvps-genesis/bootstrap] - Variable `Screen` was not found within `context`.');
	}
	if (context.Page_Code == null) {
		throw new Error('[@mvps-genesis/bootstrap] - Variable `Page_Code` was not found within `context`.');
	}
	if (screenImports == null) {
		throw new Error('[@mvps-genesis/bootstrap] - Key `screenImports` object was not passed within `config`.');
	}
	if (pageCodeImports == null) {
		throw new Error('[@mvps-genesis/bootstrap] - Key `pageCodeImports` object was not passed within `config`.');
	}
	if (regexImports == null) {
		throw new Error('[@mvps-genesis/bootstrap] - Key `regexImports` object was not passed within `config`.');
	}

	const importPromises = [];

	// execute global class
	if (globalClass != null) {
		globalClass.load(context);
	}

	// find screen class and push to importPromises
	const screenClassImporter = screenImports[context.Screen];
	if (typeof screenClassImporter === 'function') {
		importPromises.push(screenClassImporter());
	}

	// find page code class and push to importPromises
	const pageCodeClassImporter = pageCodeImports[context.Page_Code];
	if (typeof pageCodeClassImporter === 'function') {
		// throw error if page code was already registered within the screenImports object
		if (typeof screenClassImporter === 'function') {
			throw new Error(`[@mvps-genesis/bootstrap] - Screen "${context.Page_Code}" was already registered via "screenImports". Remove one of the duplicate registrations.`);
		}

		importPromises.push(pageCodeClassImporter());
	}

	// find regex keys and load if matched
	for (const key in regexImports) {
		const regex = string2regex(key);

		if (regex) {
			if (regex.test(context.Page_Code) || regex.test(context.Screen)) {
				const importer = regexImports[key];

				if (typeof importer === 'function') {
					importPromises.push(importer());
				}
			}
		}
	}

	// wait for imports to resolve then execute the load method
	Promise
		.all(importPromises)
		.then(imports => {
			imports.forEach(imported => {
				// only fire the load function if it exists
				if (typeof imported.default.load === 'function') {
					imported.default.load(context);
				}
			});
		});
};
