// Debounce Function As taken from https://codeburst.io/throttling-and-debouncing-in-javascript-646d076d0a44
export default function debounced (delay, fn) {
	'use strict';

	let timerId = null;

	return function (...args) {
		if (timerId) {
			clearTimeout(timerId);
		}
		timerId = setTimeout(() => {
			fn(...args);
			timerId = null;
		}, delay);
	};
};
