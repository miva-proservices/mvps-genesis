const decoder = document.createElement('div');

export default function decodeEntities (str) {
	decoder.innerHTML = str;
	return decoder.innerText;
};
