import 'custom-event-polyfill';

// create bus element or read from existing window variable (to prevent duplications across chunk files)
const bus = window.MVPS_EventBus || (window.MVPS_EventBus = document.createElement('div'));

export default class EventBus {
	constructor () {}

	static bus = bus;

	/**
	 * Add an event listener
	 */
	static addEventListener (event, callback) {
		bus.addEventListener(event, callback);
	}

	/**
	 * Remove an event listener
	 */
	static removeEventListener (event, callback) {
		bus.removeEventListener(event, callback);
	}

	/**
	 * Dispatch an event
	 */
	static dispatchEvent (event, detail = {}) {
		bus.dispatchEvent(new CustomEvent(event, { detail }));
	}
}
