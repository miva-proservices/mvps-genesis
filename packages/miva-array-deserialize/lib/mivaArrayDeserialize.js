import _merge from 'lodash.merge';

const cast = (value) => {
	if (!isNaN(value)) {
		return parseFloat(value);
	}
	return value;
};

const decode = (value) => {
	return decodeURIComponent(value.replace(/\+/g, ' '));
};

const parsePart = (part) => {
	if (!part) {
		return null;
	}

	const pair = part.split('=');
	const match = pair[0].match(/([^[]+)(\[([0-9]+)\])?/);

	return match
		? {
			arrIndex: match[3],
			key: match[1],
			value: pair.length > 1 ? pair[1] : null
		}
		: null;
};

const wrapArray = (value, index) => {
	if (index) {
		const arr = [];
		arr[index - 1] = value;
		return arr;
	}

	return value;
};

const parsePath = (path) => {
	return path.indexOf(':') > -1
		? path.split(':').slice(1).reverse()
			.reduce((output, part) => {
				const {key, arrIndex, value} = parsePart(part);

				if (value) {
					return key
						? {
							...output,
							[key]: wrapArray(cast(decode(value)), arrIndex)
						}
						: value;
				}

				return {[key]: wrapArray(output, arrIndex)};
			}, {})
		: path.replace('=', '');
};

const mivaArrayDeserialize = (input) => {
	return !input || typeof input !== 'string'
		? null
		: input.trim().split(',').reduce((output, path) => {
			const parsedPath = parsePath(path);
			return typeof parsedPath === 'string'
				? parsedPath
				: _merge(output, parsedPath);
		}, {});
};

export default mivaArrayDeserialize;
