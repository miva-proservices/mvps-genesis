/**
 * A class that includes helper functions for validating values against a certain type
 */
export default class ParameterValidator {
	/**
	 *
	 * @param {Object|String} instanceOrName a class instance (this) or string
	 */
	constructor (instanceOrName) {
		this._types = {
			Any: this.isDefined.bind(this),
			Array: this.isArray.bind(this),
			Boolean: this.isBoolean.bind(this),
			Defined: this.isDefined.bind(this),
			Element: this.isElement.bind(this),
			ElementList: this.isElementList.bind(this),
			Float: this.isFloat.bind(this),
			Function: this.isFunction.bind(this),
			Instance: this.isInstance.bind(this),
			Int: this.isInt.bind(this),
			Number: this.isNumber.bind(this),
			Object: this.isObject.bind(this),
			RegExp: this.isRegExp.bind(this),
			String: this.isString.bind(this),
			URL: this.isUrl.bind(this)
		};
		this._setName(instanceOrName);
	}

	// Private methods

	/**
	 *
	 * @param {Object|String} instanceOrName a class instance (this) or string
	 */
	_setName (instanceOrName) {
		if (instanceOrName === undefined || instanceOrName === null) {
			throw new TypeError('ParameterValidator :: a class instance (usually `this`) or name must be passed');
		}

		if (typeof instanceOrName === 'string') {
			this.name = instanceOrName;
		} else {
			this.name = instanceOrName.constructor.name;

			if (this.name === 'Function') {
				this.name = instanceOrName.name;
			}
		}
	}

	_test ({ key, value, condition, messagePartial, throwError, returnValue, returnMessage }) {
		if (condition) {
			const message = `${this.name} :: \`${key}\` must be ${messagePartial}`;

			if (throwError) {
				throw new TypeError(message);
			}

			return returnMessage ? message : false;
		}

		return returnValue ? value : true;
	}

	_validateRules (key, value, rule) {
		if (Array.isArray(rule)) {
			this.isArray(key, value);

			return value.length === 0
				? value
				: value.map((arrValue, index) => this._validateRules(`${key}.${index}`, arrValue, rule[0]));
		} else if (typeof rule === 'object' && rule !== null) {
			this.isObject(key, value);

			return Object.keys(rule).reduce((obj, objKey) => {
				return {
					...obj,
					...{
						[objKey]: this._validateRules(`${key}.${objKey}`, value[objKey], rule[objKey])
					}
				};
			}, {});
		}

		const required = rule.indexOf('?') === -1;
		const fn = this._types[required ? rule : rule.replace('?', '')];

		return fn(key, value, { required });
	}

	_testUrl (url) {
		try {
			if (typeof document === 'object' && document.baseURI) {
				new URL(url, document.baseURI)
			}
			else {
				new URL(url);
			}
		} catch (e) {
			return false;
		}

		return true;
	}

	// Public methods

	/**
	 *
	 * @param {String} key a string representing the variable name used within the TypeError message
	 * @param {Any} value the variable to be evaluated
	 * @param {Array} set the array to evaluate the value against
	 * @param {Boolean} options.required whether to first check if value is defined
	 * @param {Boolean} options.throwError whether to throw a TypeError or not
	 * @param {Boolean} options.returnValue whether to return the value on success
	 * @param {Boolean} options.returnMessage whether to return the message on success
	 */
	inSet (key, value, set, { throwError = true, returnValue = true, returnMessage = false } = {}) {
		return this._test({
			condition: set.every(_value => _value !== value),
			key,
			messagePartial: `match one of the following values: ${set.join(', ')}`,
			returnMessage,
			returnValue,
			throwError,
			value
		});
	}

	/**
	 *
	 * @param {String} key a string representing the variable name used within the TypeError message
	 * @param {Array} value the variable to be evaluated
	 * @param {Boolean} options.required whether to first check if value is defined
	 * @param {Boolean} options.throwError whether to throw a TypeError or not
	 * @param {Boolean} options.returnValue whether to return the value on success
	 * @param {Boolean} options.returnMessage whether to return the message on success
	 */
	isArray (key, value, { required = true, throwError = true, returnValue = true, returnMessage = false } = {}) {
		return this._test({
			condition: required && !Array.isArray(value),
			key,
			messagePartial: 'an array',
			returnMessage,
			returnValue,
			throwError,
			value
		});
	}

	/**
	 *
	 * @param {String} key a string representing the variable name used within the TypeError message
	 * @param {Boolean} value the variable to be evaluated
	 * @param {Boolean} options.required whether to first check if value is defined
	 * @param {Boolean} options.throwError whether to throw a TypeError or not
	 * @param {Boolean} options.returnValue whether to return the value on success
	 * @param {Boolean} options.returnMessage whether to return the message on success
	 */
	isBoolean (key, value, { required = true, throwError = true, returnValue = true, returnMessage = false } = {}) {
		return this._test({
			condition: required && !(typeof value === 'boolean'),
			key,
			messagePartial: 'a boolean',
			returnMessage,
			returnValue,
			throwError,
			value
		});
	}

	/**
	 *
	 * @param {String} key a string representing the variable name used within the TypeError message
	 * @param {any} value the variable to be evaluated
	 * @param {Boolean} options.throwError whether to throw a TypeError or not
	 * @param {Boolean} options.returnValue whether to return the value on success
	 * @param {Boolean} options.returnMessage whether to return the message on success
	 */
	isDefined (key, value, { throwError = true, returnValue = true, returnMessage = false } = {}) {
		return this._test({
			condition: value === undefined || value === null,
			key,
			messagePartial: 'defined',
			returnMessage,
			returnValue,
			throwError,
			value
		});
	}

	/**
	 *
	 * @param {String} key a string representing the variable name used within the TypeError message
	 * @param {HTMLElement|String} value the variable to be evaluated
	 * @param {Boolean} options.required whether to first check if value is defined
	 * @param {Boolean} options.throwError whether to throw a TypeError or not
	 * @param {Boolean} options.returnValue whether to return the value on success
	 * @param {Boolean} options.returnMessage whether to return the message on success
	 * @param {HTMLDocument|HTMLElement} options.container the container element to run selectors on; defaults to document
	 */
	isElement (key, value, { required = true, throwError = true, returnValue = true, returnMessage = true, container = document } = {}) {
		let valueElement = value;

		if (typeof value === 'string') {
			valueElement = container.querySelector(value);
		}

		return this._test({
			condition: required && !(valueElement instanceof HTMLElement),
			key,
			messagePartial: 'an element or valid selector',
			returnMessage,
			returnValue,
			throwError,
			value: valueElement
		});
	}

	/**
	 *
	 * @param {String} key a string representing the variable name used within the TypeError message
	 * @param {HTMLCollection|NodeList|String} value the variable to be evaluated
	 * @param {Boolean} options.required whether to first check if value is defined
	 * @param {Boolean} options.throwError whether to throw a TypeError or not
	 * @param {Boolean} options.returnValue whether to return the value on success
	 * @param {Boolean} options.returnMessage whether to return the message on success
	 * @param {HTMLDocument|HTMLElement} options.container the container element to run selectors on; defaults to document
	 */
	isElementList (key, value, { required = true, throwError = true, returnValue = true, returnMessage = false, container = document } = {}) {
		let valueElements = value;

		if (typeof value === 'string') {
			valueElements = container.querySelectorAll(value);
		}

		return this._test({
			condition: required && !(valueElements instanceof HTMLCollection) && !(valueElements instanceof NodeList),
			key,
			messagePartial: 'a list of elements or valid selector',
			returnMessage,
			returnValue,
			throwError,
			value: valueElements
		});
	}

	/**
	 *
	 * @param {String} key a string representing the variable name used within the TypeError message
	 * @param {Float} value the variable to be evaluated
	 * @param {Boolean} options.required whether to first check if value is defined
	 * @param {Boolean} options.throwError whether to throw a TypeError or not
	 * @param {Boolean} options.returnValue whether to return the value on success
	 * @param {Boolean} options.returnMessage whether to return the message on success
	 */
	isFloat (key, value, { required = true, throwError = true, returnValue = true, returnMessage = false } = {}) {
		return this._test({
			condition: required && !(Number(value) === value && value % 1 !== 0),
			key,
			messagePartial: 'a float',
			returnMessage,
			returnValue,
			throwError,
			value
		});
	}

	/**
	 *
	 * @param {String} key a string representing the variable name used within the TypeError message
	 * @param {Function} value the variable to be evaluated
	 * @param {Boolean} options.required whether to first check if value is defined
	 * @param {Boolean} options.throwError whether to throw a TypeError or not
	 * @param {Boolean} options.returnValue whether to return the value on success
	 * @param {Boolean} options.returnMessage whether to return the message on success
	 */
	isFunction (key, value, { required = true, throwError = true, returnValue = true, returnMessage = false } = {}) {
		return this._test({
			condition: required && !(typeof value === 'function'),
			key,
			messagePartial: 'a function',
			returnMessage,
			returnValue,
			throwError,
			value
		});
	}

	/**
	 *
	 * @param {String} key a string representing the variable name used within the TypeError message
	 * @param {any} value the variable to be evaluated
	 * @param {any} value a constructor reference (class name) (e.g. Array, HTMLElement)
	 * @param {Boolean} options.required whether to first check if value is defined
	 * @param {Boolean} options.throwError whether to throw a TypeError or not
	 * @param {Boolean} options.returnValue whether to return the value on success
	 * @param {Boolean} options.returnMessage whether to return the message on success
	 */
	isInstance (key, value, constructor, { required = true, throwError = true, returnValue = true, returnMessage = false } = {}) {
		return this._test({
			condition: required && !(value instanceof constructor),
			key,
			messagePartial: `an instance of \`${constructor.name}\``,
			returnMessage,
			returnValue,
			throwError,
			value
		});
	}

	/**
	 *
	 * @param {String} key a string representing the variable name used within the TypeError message
	 * @param {Int} value the variable to be evaluated
	 * @param {Object} options optional parameters
	 */
	isInt (key, value, { required = true, throwError = true, returnValue = true, returnMessage = false } = {}) {
		return this._test({
			condition: required && !(Number(value) === value && value % 1 === 0),
			key,
			messagePartial: 'an integer',
			returnMessage,
			returnValue,
			throwError,
			value
		});
	}

	/**
	 *
	 * @param {String} key a string representing the variable name used within the TypeError message
	 * @param {Number} value the variable to be evaluated
	 * @param {Boolean} options.required whether to first check if value is defined
	 * @param {Boolean} options.throwError whether to throw a TypeError or not
	 * @param {Boolean} options.returnValue whether to return the value on success
	 * @param {Boolean} options.returnMessage whether to return the message on success
	 */
	isNumber (key, value, { required = true, throwError = true, returnValue = true, returnMessage = false } = {}) {
		return this._test({
			condition: required && !(Number(value) === value),
			key,
			messagePartial: 'a number',
			returnMessage,
			returnValue,
			throwError,
			value
		});
	}

	/**
	 *
	 * @param {String} key a string representing the variable name used within the TypeError message
	 * @param {Object} value the variable to be evaluated
	 * @param {Object} options optional parameters
	 */
	isObject (key, value, { required = true, throwError = true, returnValue = true, returnMessage = false } = {}) {
		return this._test({
			condition: required && !(typeof value === 'object' && value !== null),
			key,
			messagePartial: 'an object',
			returnMessage,
			returnValue,
			throwError,
			value
		});
	}

	/**
	 *
	 * @param {String} key a string representing the variable name used within the TypeError message
	 * @param {Object} value the variable to be evaluated
	 * @param {Object} options optional parameters
	 */
	isRegExp (key, value, { required = true, throwError = true, returnValue = true, returnMessage = false } = {}) {
		return this._test({
			condition: required && !(value instanceof RegExp),
			key,
			messagePartial: 'a RegExp',
			returnMessage,
			returnValue,
			throwError,
			value
		});
	}

	/**
	 *
	 * @param {String} key a string representing the variable name used within the TypeError message
	 * @param {String} value the variable to be evaluated
	 * @param {Boolean} options.required whether to first check if value is defined
	 * @param {Boolean} options.throwError whether to throw a TypeError or not
	 * @param {Boolean} options.returnValue whether to return the value on success
	 * @param {Boolean} options.returnMessage whether to return the message on success
	 */
	isString (key, value, { required = true, throwError = true, returnValue = true, returnMessage = false } = {}) {
		return this._test({
			condition: required && !(typeof value === 'string'),
			key,
			messagePartial: 'a string',
			returnMessage,
			returnValue,
			throwError,
			value
		});
	}

	/**
	 *
	 * @param {String} key a string representing the variable name used within the TypeError message
	 * @param {String} value the variable to be evaluated
	 * @param {Boolean} options.required whether to first check if value is defined
	 * @param {Boolean} options.throwError whether to throw a TypeError or not
	 * @param {Boolean} options.returnValue whether to return the value on success
	 * @param {Boolean} options.returnMessage whether to return the message on success
	 */
	isUrl (key, value, { required = true, throwError = true, returnValue = true, returnMessage = false } = {}) {
		return this._test({
			condition: required && !this._testUrl(value),
			key,
			messagePartial: 'a valid url',
			returnMessage,
			returnValue,
			throwError,
			value
		});
	}

	/**
	 * Test an object against a set of type rules called a "ruleset". See documentation on how to format your ruleset object.
	 * @param {String} key a string representing the variable name used within the TypeError message
	 * @param {Object|Array} value the variable to be evaluated
	 * @param {Object|Array} ruleset a "ruleset" object containing a structure of types
	 */
	validate (key, value, ruleset) {
		return this._validateRules(key, value, ruleset);
	}
};
