export default function string2regex (str) {
	let matches = null;

	try {
		matches = str.match(/\/(.+)\/(.*)/);

		if (!matches) {
			throw new SyntaxError('string2regex :: unable parse regex string');
		}
	} catch (e) {
		// eslint-disable-next-line no-console
		console.error(e);

		return;
	}

	// eslint-disable-next-line consistent-return
	return new RegExp(matches[1], matches[2]);
};
