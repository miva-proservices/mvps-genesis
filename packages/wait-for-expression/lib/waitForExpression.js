import ParameterValidator from '@mvps-genesis/parameter-validator';

/**
 * Callback function that is run after the timer ends without success
 */
const onEndDefault = function () {
	// eslint-disable-next-line no-console
	console.warn('waitForExpression :: timer ended without success');
};

/**
 * A helper function that evaluates an expression every X milliseconds until true or until the maximum wait time is reached; then runs a callback function on success or error
 *
 * @param {Function} expression a function that will be evaluated every tick, stops the process if true
 * @param {Function} onSuccess a function that is called on success
 * @param {Int} options.intervalTime how often to check the expression in ms
 * @param {Int} options.maxWaitTime the maximum time to check for before expiring
 * @param {Function} options.onEnd callback function ran if timer expires
 */
export default function waitForExpression (expression, onSuccess, { intervalTime = 200, maxWaitTime = 5000, onEnd = onEndDefault } = {}) {
	// Create validator class
	const validator = new ParameterValidator('waitForExpression');

	// Validate passed parameters
	validator.isFunction('expression', expression);
	validator.isFunction('onSuccess', onSuccess);
	validator.isInt('intervalTime', intervalTime);
	validator.isInt('maxWaitTime', maxWaitTime);
	validator.isFunction('onEnd', onEnd);

	// Define properties
	let interval = null;
	const startTime = Date.now();

	// Define private functions
	const onTick = function () {
		const remainingTime = Date.now() - startTime;
		const result = expression(remainingTime);

		// Check if expression is true or timer ended
		if (result || remainingTime > maxWaitTime) {
			clearInterval(interval);

			return result ? onSuccess() : onEnd();
		}

		return false;
	};

	// Start interval loop
	interval = setInterval(onTick, intervalTime);
};
