# mvps-sitespeed-io-budgets

This repo exemplifies how you can use sitespeed.io to check a [list of urls](urls.txt) and see how they perform against certain [Lighthouse, Web Vitals, and Axe best-practices](budget.json)

# Setup

## One-time Computer Setup

```sh
npm install -g sitespeed.io
```

## One-time Repo Setup

```sh
# cd into your store's repo
git clone --depth=1 --branch=main ssh://git@bitbucket.mivamerchant.net:7999/ps/mvps-sitespeed-io-budgets.git tests/sitespeed.io
rm -rf ./tests/sitespeed.io/.git
cd tests/sitespeed.io
npm install
# modify urls.txt with your store's URLs from https://bitbucket.mivamerchant.net/projects/PS/repos/mvps-sitespeed-io-dashboard/browse/tests
git add .
git commit -m "added sitespeed.io tests"
```

## Run Tests in QA Phase

```sh
cd tests/sitespeed.io
npm run test
# Then view output.txt & the sitespeed-results/YYYY-MM-DD/mobile/budget.html
```

That's it! Re-run this as often as you want & commit output.txt and/or the sitespeed-results/ to track changes over time

---

## Viewing Results

1. After running `$ npm run test`, wait for the process to finish, check the results in `sitespeed-result/`
2. Review the results by opening the latest run's budget file in a browser.
	* For example: `sitespeed-result\www.example.com\YYY-MM-DD\mobile\budget.html`
3. Also, you can review the `output.txt` to see a text-summary of the results (Note the "Failing budget..." for areas of improvement):
	* For example:
	```
	[2020-11-06 11:13:34] INFO: Versions OS: win32 10.0.19041 nodejs: v14.14.0 sitespeed.io: 15.7.2 browsertime: 10.6.4 coach: 5.1.1
	[2020-11-06 11:13:35] INFO: Axe plugin activated
	[2020-11-06 11:13:35] INFO: Will run Lighthouse tests after Browsertime has finished
	[2020-11-06 11:13:35] INFO: Running tests using Chrome - 1 iteration(s)
	[2020-11-06 11:13:40] INFO: Use Chrome trace categories: -*,disabled-by-default-lighthouse,v8,v8.execute,blink.user_timing,devtools.timeline,disabled-by-default-devtools.timeline,disabled-by-default-devtools.timeline.stack
	[2020-11-06 11:13:40] INFO: Testing url https://genesis.mivareadythemes.com/ iteration 1
	[2020-11-06 11:13:53] INFO: https://genesis.mivareadythemes.com/ 40 requests, TTFB: 1.16s, firstPaint: 1.73s, FCP: 1.73s, DOMContentLoaded: 1.86s, LCP: 1.73s, CLS: 0.0136, TBT: 10ms, Load: 3.19s
	[2020-11-06 11:13:53] INFO: Start collecting Lighthouse result for https://genesis.mivareadythemes.com/ iteration 1
	[2020-11-06 11:14:07] INFO: Failing budget Lighthouse Performance Score for https://genesis.mivareadythemes.com/ with value 67 min limit 90
	[2020-11-06 11:14:07] INFO: Failing budget Lighthouse Best Practices Score for https://genesis.mivareadythemes.com/ with value 86 min limit 90
	[2020-11-06 11:14:07] INFO: Failing budget Lighthouse SEO Score for https://genesis.mivareadythemes.com/ with value 83 min limit 90
	[2020-11-06 11:14:07] INFO: Budget: 8 working, 3 failing tests and 0 errors
	[2020-11-06 11:14:09] INFO: HTML stored in C:\Users\ssoule\Documents\Development\mvps-sitespeed-io-budgets\sitespeed-result\2020-11-06\mobile
	```

## More SiteSpeed.io Information

- [sitespeed.io](https://www.sitespeed.io/)
	- [Installation](https://www.sitespeed.io/documentation/sitespeed.io/installation/#npm)
	- [Configuration](https://www.sitespeed.io/documentation/sitespeed.io/configuration/)
	- [Budgets](https://www.sitespeed.io/documentation/sitespeed.io/performance-budget/)
	- [Lighthouse Plugin](https://github.com/sitespeedio/plugin-lighthouse)

---

## Modifications

### Dev Stores

To test dev stores, temporarily update urls.txt, or create a `urls--dev.txt` and `test--dev.sh` to target the dev store

### Basic Authentication

If your dev store is behind basic auth, then you can either:

1. Update the `test.sh` script with a mew argument `--basicAuth="username@password"`
	* ex `sitespeed.io --headless --basicAuth="username@password" --outputFolder="$(date +sitespeed-result/%Y-%m-%d/mobile)" --browsertime.viewPort="414×896" --config="config.json" urls.txt`
2. Add `basicAuth` to the `config.json`'s `browsertime` section:
	* For example:
	```json
	{
		"browsertime": {
			"basicAuth": "username@password",
			// ...
		}
		// ...
	}
	```
